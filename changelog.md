# Change log
## Imported modules.

- Select comboboxes:    http://silviomoreto.github.io/bootstrap-select/examples/  
                        http://harvesthq.github.io/chosen/  
- File uploads:         http://blueimp.github.io/jQuery-File-Upload/jquery-ui.html  
- DateTime picker:      http://www.daterangepicker.com/  
- editable:             http://vitalets.github.io/x-editable/  
- calendar:             http://bootstrap-calendar.eivissapp.com/  
- password complexity:  https://danpalmer.me/jquery-complexify/  
- overlay div:          https://gasparesganga.com/labs/jquery-loading-overlay/  
- to check:             https://startbootstrap.com/bootstrap-resources/#plugins
- inline editor:        https://vitalets.github.io/x-editable/index.html
- Double list d'n'd:    https://www.jqueryscript.net/other/jQuery-Drag-drop-Sorting-Plugin-For-Bootstrap-html5sortable.html
     
 
### 2017.09.01
    - Added scrapper to recover localities from http://www.solosequenosenada.com/misc/postalcodes/
### 2017.10.11
    - Added dates for application, council and communications.
    - Refactor database schema.
### 2017.10.30 
    - Incluír la fecha e caducidad del Carnet de conducir
    - Incluír la fecha de caducidad de la Licencia Municipal
     