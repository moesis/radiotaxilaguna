<!DOCTYPE html>
<html>
    <head>
        @include('intranet.sections.header')
    </head>
    <body class="hold-transition {{ Config::get('radiotaxilaguna.skin') }} sidebar-mini fuelux">
        <div id="wrapper">
            @yield('content')
        </div>
        <section>
            @include('intranet.sections.footer')
        </section>
    </body>
</html>
