@extends('layouts.master')

@section('title', Lang::get('general.admin-home'))

@section ('content')
    @parent
    @include('intranet.partials.header')
    @include('intranet.partials.sidebar')
    <div class="content-wrapper">
        @if (array_key_exists('expirations', $data) && array_key_exists('staffExpirations', $data))
            @include('intranet.dashboard.dashboard', ['expirations' => $data['expirations'], 'staffExpirations' => $data['staffExpirations'], 'days' => $data['days']])
        @endif

        @yield('intranet-content')
        @include('intranet.partials.snippets.content.footer')
    </div>
    @include('intranet.partials.right-sidebar')
    @include('intranet.partials.footer')
@endsection

