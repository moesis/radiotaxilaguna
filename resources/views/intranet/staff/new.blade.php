@extends('intranet')

@section ('intranet-content')

    <div class="container" id="new-staff-container">
        {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'new-staff-form', 'method' => 'POST']) !!}
        <h3>Datos personales del asalariado</h3>
        <div class="form-group">
            <label for="name-id" class="col-sm-2 control-label text-right">Nombre</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="name" id="name-id" placeholder="Nombre"
                       minlength="1" data-msg-minlength="Mín. 2 car."
                       maxlength="40" data-msg-maxlength="Máx. 40 car."
                       required data-msg-required="Requerido"/>
            </div>

            <label for="lastname-id" class="col-sm-1 control-label text-right">Apellidos</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="lastname" id="lastname-id" placeholder="Apellidos"
                       minlength="3" data-msg-minlength="Mín. 3 car."
                       maxlength="100" data-msg-maxlength="Máx. 100 car."
                       required data-msg-required="Requerido" />
            </div>

            <div class="form-group">
                <label for="dob-input" class="col-md-1 control-label">F.Nac</label>
                <div class="input-group date form-date-past col-md-2"
                     data-date=""
                     data-date-format="dd/mm/yyyy"
                     data-link-field="dob-id"
                     data-link-format="yyyy-mm-dd">
                    <input id="dob-input" class="form-control" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                </div>
                <input type="hidden" id="dob-id" value="" name="dob" />
            </div>
        </div>

        <div class="form-group">
            <label for="email-id" class="col-sm-2 control-label text-right">C. Electr.</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="email" id="email-id" placeholder="Correo Electrónico"
                       data-msg-required="Debe introducir un correo electrónico válido."
                       data-msg="Esta dirección de correo electrónico ya se encuentra registrada."
                       maxlength="50"
                       data-rule-email="email"
                       required/>
            </div>

            <label for="personal-id" class="col-sm-1 control-label text-right">DNI</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" name="pid" id="personal-id" placeholder="Núm. DNI"
                       data-msg="Ese DNI ya existe en la base de datos."
                       minlength="9" data-msg-minlength="Mín. 9 car."
                       maxlength="11" data-msg-maxlength="Máx. 11 car."
                       required data-msg-required="Requerido"/>
            </div>

            <label for="pid_expiration_date-input" class="col-sm-1 control-label text-right">F. Caduc.</label>
            <div class="input-group date form-date-future col-sm-2" data-date="" data-date-format="dd/mm/yyyy" data-link-field="pid_expiration_date-id" data-link-format="yyyy-mm-dd">
                <input id="pid_expiration_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
            </div>
            <input type="hidden" id="pid_expiration_date-id" value="" name="pid_expiration_date" />
        </div>

        <div class="form-group">
            <div class="col-sm-6">
                <label for="driver_licence_expiry_date-input" class="col-sm-7 control-label text-right">Fecha de caducidad del Carnet de Conducir</label>
                <div class="input-group date form-date-future col-sm-5" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dlicence_expiry_date-id" data-link-format="yyyy-mm-dd">
                    <input id="driver_licence_expiry_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                </div>
                <input type="hidden" id="dlicence_expiry_date-id" value="" name="driver_licence_expiry_date" />
            </div>

            <div class="col-sm-6">
                <label for="municipal_licence_expiry_date-input" class="col-sm-7 control-label text-right">Fecha de caducidad de la licencia del taxi</label>
                <div class="input-group date form-date-future col-sm-5" data-date="" data-date-format="dd/mm/yyyy" data-link-field="mlicence_expiry_date-id" data-link-format="yyyy-mm-dd">
                    <input id="municipal_licence_expiry_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                </div>
                <input type="hidden" id="mlicence_expiry_date-id" value="" name="municipal_licence_expiry_date" />
            </div>
        </div>

        <div class="form-group">
            <label for="address-1-id" class="col-sm-2 control-label text-right">Dirección</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="address" id="address-1-id" placeholder="dirección - linea 1"
                       minlength="3"
                       maxlength="200"
                       required/>
            </div>
        </div>

        <div class="form-group">
            <label for="address-2-id" class="col-sm-2 control-label text-right"></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="address_1" id="address-2-id" placeholder="dirección - linea 2"
                       minlength="3"
                       maxlength="200"/>
            </div>
        </div>

        <div class="form-group">
            <label for="postal-code-id" class="col-sm-2 control-label text-right">C.P.<i id="addAction" class="rtl-margin-left-10 fa fa-pencil-square-o hidden" data-toggle="modal" data-target="#postalCodeModal"></i></label>
            <div class="col-sm-2">
                <input type="number" class="form-control" name="postalCode" id="postal-code-id" placeholder="Cód. Pos."
                       minlength="5" data-msg-minlength="Mín. 5 car."
                       maxlength="5" data-msg-maxlength="Máx. 5 car."
                       required data-msg-required="Requerido"/>
            </div>
            <input type="hidden" name="postal_code_id" id="postal-code-val" value=""/>

            <label for="locality-id" class="col-sm-2 control-label text-right">Localidad</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="locality" id="locality-id" disabled readonly/>
            </div>
        </div>

        <div class="form-group">
            <label for="phone-id" class="col-sm-2 control-label text-right">Teléfono</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="phone" id="phone-id" placeholder="Teléfono"
                       minlength="9" data-msg-minlength="Mín. 9 car."
                       maxlength="30" data-msg-maxlength="Máx. 30 car."/>
            </div>

            <label for="mobile-id" class="col-sm-2 control-label text-right">Móvil</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="mobile" id="mobile-id" placeholder="Móvil"
                       minlength="9" data-msg-minlength="Mín. 9 car."
                       maxlength="30" data-msg-maxlength="Máx. 30 car."
                       required data-msg-required="Requerido"/>
            </div>
        </div>

        <fieldset>
            <legend>Datos del contrato</legend>
            <div class="form-group">
                <label for="boss-id" class="col-sm-2 control-label text-right">Socio</label>
                <div class="col-sm-4">
                    <select id="boss-id" class="form-control selectpicker" name="boss-name"
                            title="Seleccione el socio que contrata"
                            data-selected-text-format="count"
                            data-live-search="true" required data-msg-required="Requerido">
                        @foreach ($data['members'] as $member)
                            <option value="{{ $member->id }}">{{ $member->name }}&nbsp;{{ $member->lastname }}</option>
                        @endforeach
                    </select>
                </div>
                <label for="conctract-id" class="col-sm-2 control-label text-right">Tipo de contrato</label>
                <div class="col-sm-4">
                    <select id="contract-id" class="form-control selectpicker" name="contract-type"
                            title="Seleccione el tipo de contrato que tiene."
                            data-selected-text-format="count"
                            data-live-search="true">
                        @foreach ($data['contracts'] as $contractId => $contractType)
                            <option value="{{ $contractId }}">{{ $contractType }}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="start_date-input" class="col-sm-7 control-label text-right">Fecha de inicio del contrato</label>
                    <div class="input-group date form-date-past col-sm-5" data-date="" data-date-format="dd/mm/yyyy" data-link-field="start_date-id" data-link-format="yyyy-mm-dd">
                        <input id="start_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="start_date-id" value="" name="start_date" />
                </div>

                <div class="col-sm-6">
                    <label for="end_date-input" class="col-sm-7 control-label text-right">Fecha de finalización del contrato</label>
                    <div class="input-group date form-date-past col-sm-5" data-date="" data-date-format="dd/mm/yyyy" data-link-field="end_date-id" data-link-format="yyyy-mm-dd">
                        <input id="end_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="end_date-id" value="" name="end_date" />
                </div>
            </div>

        </fieldset>

        <fieldset>
            <legend>Preferencias e Idiomas</legend>

            <fieldset class="col-sm-8">
                <legend>Preferencias del Conductor</legend>
                <section>
                    <div class="col-sm-6">
                        <ol id="driver-preferences" class="list-group driver-preferences rtl-bordered"></ol>
                    </div>
                    <div class="col-sm-6">
                        <ol class="list-group driver-preferences rtl-bordered">
                            @foreach ($data['preferences-driver'] as $preference)
                                <li class="list-group-item" draggable="true" data-driver-preferences="{{ $preference->id }}">{{ $preference->name }}</li>
                            @endforeach
                        </ol>
                    </div>
                </section>
                <input type="hidden" name="preferences" id="choosen-driver-preferences" readonly="readonly" />
            </fieldset>

            <fieldset class="col-lg-4">
                <legend>Idiomas</legend>
                <section>
                    <div class="row">
                        <div class="col-sm-6">
                            <ol id="spoken-languages" class="list-group languages rtl-bordered"></ol>
                        </div>
                        <div class="col-sm-6">
                            <ol class="list-group languages rtl-bordered">
                                @foreach ($data['languages'] as $language)
                                    <li class="list-group-item" draggable="true" data-spoken-languages="{{ $language->id }}">{{ $language->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </section>
                <input type="hidden" name="languages" id="choosen-spoken-languages" readonly="readonly" />
            </fieldset>

        </fieldset>

        <div class="col-sm-12 text-right">
            <button class="btn btn-primary" id="savebutton">Grabar</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@include('intranet.partials.snippets.modals.addpostalcode')

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var staffFormValidator = $('#new-staff-form').validate({
            onkeyup   : function (element, event) {
                if ($(element).attr('name') == 'name') {
                    return false // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if (event.which === 9 && this.elementValue(element) === '') {
                        return
                    } else if (element.name in this.submitted || element === this.lastElement) {
                        this.element(element)
                    }
                }
            },
            errorClass: 'text-danger',
            validClass: 'text-success',
            onfocusout: function (element) {
                $(element).valid()
            },
            rules     : {
                email         : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.staff.email') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese correo ya está registrado.'))
                                    break
                                case 'Not Found':
                                    state = true
                                    break
                            }

                            return state
                        },
                        error      : function (response) {
                            showNotification('Error', 'error', 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador')
                        }
                    }
                },
                pid           : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.staff.personalId') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa DNI ya existe.'))
                                    break
                                case 'Not Found':
                                    if (data.letter.length > 0) {
                                        showNotification('Letra DNI incorrecta', 'notice', 'La letra correcta es la <b>' + data.letter + '</b>')
                                    }
                                    $('#personal-id').val(($('#personal-id').val()).toUpperCase())
                                    state = true
                                    break
                            }

                            return state
                        },
                        error      : function (response) {
                            showNotification('Error', 'error', 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador')
                        }
                    }
                },
                postalCode    : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.postalcode') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            $('#locality-id').val(data['name'])
                            $('#postal-code-val').val(data['id'])

                            return true
                        },
                        error      : function (response) {
                            ajaxStandardErrorTreatment(response.status, null, function () {
                                $('#locality-id').val('El código postal no existe...')
                                $('#addAction').removeClass('hidden')
                                $('#postal-code').focus()
                            })
                        }
                    }
                },
                errorElement  : 'em',
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass('help-block')
                    if (element.prop('type') === 'checkbox') {
                        error.insertAfter(element.parent('label'))
                    } else {
                        error.insertAfter(element)
                    }
                },
                highlight     : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-error').removeClass('has-success')
                },
                unhighlight   : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-success').removeClass('has-error')
                }
            }
        })

        $('#postalCodeModal').on('shown.bs.modal', function () {
            var modal = $(this)

            $.LoadingOverlay('show')
            createNewPromise('{{ route('ajax.localities') }}').then(
                function (response) {
                    populateSelect('modal-locality-id', response, 0)
                    $('#modal-locality-id').selectpicker('refresh')
                    $.LoadingOverlay('hide')
                },
                function (Error) {
                    showNotification('Oops! Ha ocurrido un error.', 'error', Error)
                    $.LoadingOverlay('hide')
                }
            )

            $('#postal_code_id').val($('#postal-code-id').val())
            $('#place_id').focus()
        })

        {{-- Capturing the Submit event. --}}
        $('#new-staff-form').on('submit', function (evt) {
            evt.preventDefault()
            if (!staffFormValidator.form()) {
                evt.preventDefault();
                return false;
            }

            $('.content-wrapper').LoadingOverlay('show');

            var disabled = $('#new-staff-form').find(':input:disabled').removeAttr('disabled')
            disabled.attr('disabled', 'disabled')

            $('#choosen-driver-preferences').val(getActiveOptionsFromList('driver-preferences'));
            $('#choosen-spoken-languages').val(getActiveOptionsFromList('spoken-languages'));

            var formData = $('#new-staff-form').serialize();

            $.post('{{ route('ajax.add.staff') }}', formData, function (data) {
            })
                .fail(function (data) {
                    $.each(data.responseJSON, function (index, item) {
                        $('input[name="' + index + '"]').parent().addClass('has-error');
                        $('input[name="' + index + '"]').parent().append('<span class="help-block">' + item[0] + '</span>');
                    })
                    showNotification('Error en la grabación.', 'error', 'Se ha producido un error en la grabación de la ficha del personal asalariado.');
                })
                .done(function (data) {
                    showNotification('Grabación con éxito.', 'success', 'La grabación de la ficha del personal asalariado ha sido safisfactoria.');
                    window.location.href = '{{ route('admin.staff.list') }}';
                })
                .always(function () {
                    $('.content-wrapper').LoadingOverlay('hide');
                })
        })
    </script>

@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $('.form-date-past').datetimepicker(dateTimePickerDefaultOptions);
            $('.form-date-future').datetimepicker(dateTimePickerOptionsForFutureDates);

            $("ol.driver-preferences").sortable({
                group: 'driver-preferences',
                onDragStart: function ($item, container, _super) {
                    // Duplicate items of the no drop area
                    if(!container.options.drop)
                        $item.clone().insertAfter($item);
                    _super($item, container);
                }
             });

            $("ol.languages").sortable({
                group: 'languages',
                onDragStart: function ($item, container, _super) {
                    // Duplicate items of the no drop area
                    if(!container.options.drop)
                        $item.clone().insertAfter($item);
                    _super($item, container);
                }
            });

        })
    </script>
@endsection