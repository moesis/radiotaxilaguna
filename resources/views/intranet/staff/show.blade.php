@extends('intranet')

@section ('intranet-content')

<div class="container rtl-margin-bottom-20" id="staffData">

    {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'staffDataForm', 'method' => 'POST']) !!}
        <input type="hidden" class="not-editable" id="staff-pk" value="{{ $data['staff']['id'] }}" name="staff-pk"/>

        <h3>Datos personales del asalariado</h3>

        <div class="form-group">
            <label for="name-id" class="col-sm-2 control-label text-right">Nombre</label>
            <div class="col-sm-3">
                <input type="text" class="form-control rtl-editable" name="name" id="name-id" placeholder="Nombre"
                       value="{{ $data['staff']['name'] }}"
                       minlength="1" data-msg-minlength="Mín. 2 car."
                       maxlength="40" data-msg-maxlength="Máx. 40 car."
                       required data-msg-required="Requerido"
                       readonly />
            </div>

            <label for="lastname-id" class="col-sm-1 control-label text-right">Apellidos</label>
            <div class="col-sm-3">
                <input type="text" class="form-control rtl-editable" name="lastname" id="lastname-id" placeholder="Apellidos"
                       value="{{ $data['staff']['lastname'] }}"
                       minlength="3" data-msg-minlength="Mín. 3 car."
                       maxlength="100" data-msg-maxlength="Máx. 100 car."
                       required data-msg-required="Requerido"
                       readonly/>
            </div>

            <div class="form-group">
                <label for="dob-input" class="col-md-1 control-label">F.Nac</label>
                <div class="input-group date form-date-past col-md-2"
                     data-date="{{ empty($data['staff']['dob'])  ? '' : Carbon\Carbon::parse($data['staff']['dob'])->format('d/m/Y') }}"
                     data-date-format="dd/mm/yyyy" data-link-field="dob-id" data-link-format="yyyy-mm-dd">
                    <input id="dob-input" class="form-control text-right not-editable" size="16" type="text"
                           value="{{ empty($data['staff']['dob'])  ? '' : Carbon\Carbon::parse($data['staff']['dob'])->format('d/m/Y') }}" readonly>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    <input type="hidden" class="not-editable" id="dob-id"
                           value="{{ empty($data['staff']['dob'])  ? '' : Carbon\Carbon::parse($data['staff']['dob'])->format('Y-m-d') }}" name="dob"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="email-id" class="col-sm-2 control-label text-right">C. Electr.</label>
            <div class="col-sm-4">
                <input type="text" class="form-control rtl-editable" name="email" id="email-id" placeholder="Correo Electrónico"
                       value="{{ $data['staff']['email'] }}"
                       data-msg="Esta dirección de correo electrónico ya se encuentra registrada."
                       maxlength="50" data-msg-maxlength="Max. 50 car."
                       data-rule-email="email"
                       required data-msg-required="Debe introducir un correo electrónico válido."
                       readonly/>
            </div>

            <label for="personal-id" class="col-sm-1 control-label text-right">DNI</label>
            <div class="col-sm-2">
                <input type="text" class="form-control rtl-editable" name="pid" id="personal-id" placeholder="Núm. DNI"
                       value="{{ $data['staff']['pid'] }}"
                       data-msg="Este DNI ya se encuentra registrada."
                       minlength="9" data-msg-minlength="Mín. 9 car."
                       maxlength="11" data-msg-maxlength="Máx. 11 car."
                       required data-msg-required="Requerido"
                       readonly />
            </div>

            <label for="pid_expiration_date-input" class="col-sm-1 control-label text-right">F. Caduc.</label>
            <div class="input-group date form-date-future col-sm-2"
                 data-date="{{ empty($data['staff']['pid_expiration_date'])  ? '' : Carbon\Carbon::parse($data['staff']['pid_expiration_date'])->format('d/m/Y') }}"
                 data-date-format="dd/mm/yyyy" data-link-field="pid_expiration_date-id" data-link-format="yyyy-mm-dd">
                <input id="pid_expiration_date-input" class="form-control text-right not-editable" size="16" type="text"
                       value="{{ empty($data['staff']['pid_expiration_date'])  ? '' : Carbon\Carbon::parse($data['staff']['pid_expiration_date'])->format('d/m/Y') }}"
                       readonly/>
                <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                <input type="hidden" class="not-editable" id="pid_expiration_date-id"
                       value="{{ empty($data['staff']['pid_expiration_date'])  ? '' : Carbon\Carbon::parse($data['staff']['pid_expiration_date'])->format('Y-m-d') }}"
                       name="pid_expiration_date"/>
            </div>

        </div>

        <div class="form-group">
            <div class="form-group col-sm-6">
                <label for="driver_licence_expiry_date-input" class="col-sm-7 control-label text-right">Fecha de caducidad del Carnet de Conducir</label>
                <div class="input-group date form-date-future"
                     data-date="{{ empty($data['staff']['driver_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['driver_licence_expiry_date'])->format('d/m/Y') }}"
                     data-date-format="dd/mm/yyyy" data-link-field="dlicence_expiry_date-id" data-link-format="yyyy-mm-dd">
                    <input id="driver_licence_expiry_date-input" class="form-control col-sm-4 text-right not-editable" size="16" type="text"
                           value="{{ empty($data['staff']['driver_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['driver_licence_expiry_date'])->format('d/m/Y') }}" readonly/>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    <input type="hidden" class="not-editable" id="dlicence_expiry_date-id"
                           value="{{ empty($data['staff']['driver_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['driver_licence_expiry_date'])->format('Y-m-d') }}"
                           name="driver_licence_expiry_date"/>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label for="municipal_licence_expiry_date-input" class="col-sm-7 control-label text-right">Fecha de caducidad de la licencia del taxi</label>
                <div class="input-group date form-date-future"
                     data-date="{{ empty($data['staff']['municipal_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['municipal_licence_expiry_date'])->format('d/m/Y') }}"
                     data-date-format="dd/mm/yyyy" data-link-field="mlicence_expiry_date-id" data-link-format="yyyy-mm-dd">
                    <input id="municipal_licence_expiry_date-input" class="form-control col-sm-4 text-right not-editable" size="16" type="text"
                           value="{{ empty($data['staff']['municipal_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['municipal_licence_expiry_date'])->format('d/m/Y') }}" readonly>
                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    <input type="hidden" class="not-editable" id="mlicence_expiry_date-id"
                           value="{{ empty($data['staff']['municipal_licence_expiry_date'])  ? '' : Carbon\Carbon::parse($data['staff']['municipal_licence_expiry_date'])->format('Y-m-d') }}"
                           name="municipal_licence_expiry_date"/>
                </div>
            </div>

        </div>

        <div class="form-horizontal">
            <div class="form-group">
                <label for="address-1-id" class="col-sm-2 control-label text-right">Dirección</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control rtl-editable" name="address" id="address-1-id" placeholder="dirección - linea 1"
                           value="{{ $data['staff']['address'] }}"
                           minlength="3" data-msg-minlength="Mín. 3 car."
                           maxlength="200" data-msg-maxlength="Mín. 200 car."
                           required data-msg-required="Requerido"
                           readonly />
                </div>
            </div>

            @if (!empty($data['staff']['address_1']))
            <div class="form-group">
                <label for="address-2-id" class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control rtl-editable" name="address_1" id="address-2-id" placeholder="dirección - linea 2"
                           value="{{ $data['staff']['address_1'] }}"
                           minlength="3" data-msg-minlength="Mín. 3 car."
                           maxlength="200" data-msg-maxlength="Mín. 200 car."
                           readonly />
                </div>
            </div>
            @endif

            <div class="form-group">
                <label for="postal-code-id" class="col-sm-2 control-label text-right">C.P.<i id="addAction" class="rtl-margin-left-10 fa fa-pencil-square-o hidden" data-toggle="modal" data-target="#postalCodeModal"></i></label>
                <div class="col-sm-2">
                    <input type="number" class="form-control rtl-editable" name="postalCode" id="postal-code-id" placeholder="Cód. Pos."
                           value="{{ $data['staff']['postal-code']['postal_code'] }}"
                           minlength="5" data-msg-minlength="Mín. 5 car."
                           maxlength="5" data-msg-maxlength="Máx. 5 car."
                           required data-msg-required="Requerido"
                           readonly/>
                    <input type="hidden" class="not-editable" name="postal_code_id" id="postal-code-val" value="{{ $data['staff']['postal-code']['id'] }}"/>
                </div>

                <label for="locality-id" class="col-sm-2 control-label text-right">Localidad</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control not-editable" name="locality" id="locality-id" value="{{ $data['staff']['locality']['name'] }}" disabled readonly/>
                </div>
            </div>

            <div class="form-group">
                <label for="phone-id" class="col-sm-2 control-label text-right">Teléfono</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control rtl-editable" name="phone" id="phone-id" placeholder="Teléfono"
                           value="{{ $data['staff']['phone'] }}"
                           minlength="9" data-msg-minlength="Mín. 9 car."
                           maxlength="30" data-msg-maxlength="Máx. 30 car."
                           readonly />
                </div>

                <label for="mobile-id" class="col-sm-2 control-label text-right">Móvil</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control rtl-editable" name="mobile" id="mobile-id" placeholder="Móvil"
                           value="{{ $data['staff']['mobile'] }}"
                           minlength="9" data-msg-minlength="Mín. 9 car."
                           maxlength="30" data-msg-maxlength="Máx. 30 car."
                           required data-msg-required="Requerido"
                           readonly/>
                </div>
            </div>


            @if (!empty($data['staff']['member']['id']))
                <h3>Datos del contrato <a href="{{route('admin.licence.show', [$data['staff']['member']['id']])}}">(LM-{{ $data['staff']['member']['licence']['municipal_licence'] }})</a></h3>
            @else
                <h3>Datos del contrato</h3>
            @endif
        <div class="form-horizontal">
            <div class="form-group">
                <label for="boss-id" class="col-sm-2 control-label text-right">Socio</label>
                <div class="col-sm-4">
                    <select id="boss-id" class="form-control selectpicker rtl-editable" name="boss-name"
                            title="Seleccione el socio que contrata"
                            data-selected-text-format="count"
                            data-live-search="true" disabled>
                        @foreach ($data['members'] as $memberList)
                            <option value="{{ $memberList['id'] }}"
                            @if (count($data['staff']['member']) && $memberList['id'] === $data['staff']['member']['id'])
                                selected="selected"
                            @endif
                            >{{ $memberList['name']}}&nbsp;{{ $memberList['lastname']}}</option>
                        @endforeach
                    </select>
                </div>
                <label for="conctract-id" class="col-sm-2 control-label text-right">Tipo de contrato</label>
                <div class="col-sm-4">
                    <select id="contract-id" class="form-control selectpicker rtl-editable" name="contract-type"
                            title="Seleccione el tipo de contrato que tiene."
                            data-selected-text-format="count"
                            data-live-search="true" disabled>
                        @foreach ($data['contracts'] as $contract)
                        <option value="{{ $contract['id'] }}"
                                @if (count($data['staff']['contract']) && $contract['id'] === $data['staff']['contract']['id'])
                                        selected="selected"
                                @endif
                        >{{ $contract['name'] }}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <label for="start_date-input" class="col-sm-7 control-label text-right">Fecha de inicio del contrato</label>
                    <div class="input-group date form-date-past col-sm-5"
                         data-date="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['start_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['start_date'])->format('d/m/Y')}}"
                         data-date-format="dd/mm/yyyy"
                         data-link-field="start_date-id"
                         data-link-format="yyyy-mm-dd">
                        <input id="start_date-input" class="form-control text-right not-editable" size="16" type="text"
                               value="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['start_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['start_date'])->format('d/m/Y')}}" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" class="not-editable" id="start_date-id"
                           value="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['start_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['start_date'])->format('Y-m-d') }}"
                           name="start_date"/>
                </div>

                <div class="col-sm-6">
                    <label for="end_date-input" class="col-sm-7 control-label text-right">Fecha de finalización del contrato</label>
                    <div class="input-group date form-date-past col-sm-5"
                         data-date="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['end_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['end_date'])->format('d/m/Y') }}"
                         data-date-format="dd/mm/yyyy"
                         data-link-field="end_date-id"
                         data-link-format="yyyy-mm-dd">
                        <input id="end_date-input" class="form-control text-right not-editable" size="16" type="text"
                               value="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['end_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['end_date'])->format('d/m/Y')}}" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" class="not-editable" id="end_date-id"
                           value="{{ empty($data['staff']['contract']) || empty($data['staff']['contract']['pivot']['end_date'])  ? '' : Carbon\Carbon::parse($data['staff']['contract']['pivot']['end_date'])->format('Y-m-d') }}"
                           name="end_date"/>
                </div>
            </div>
        </div>

        <h3>Preferencias e Idiomas</h3>
        <div class="form-horizontal">
            <fieldset class="col-sm-8">
                <legend>Preferencias del Conductor</legend>
                <section>
                    <div class="col-sm-6">
                        <ol id="driver-preferences" class="list-group vertical  driver-preferences rtl-sortable-list rtl-bordered rtl-editable">
                            @foreach ($data['staff']['preferences'] as $preference)
                                <li class="list-group-item" draggable="true" data-driver-id="{{ $preference->id }}">{{ $preference->name }}</li>
                            @endforeach
                        </ol>
                    </div>
                    <div class="col-sm-6">
                        <ol class="list-group vertical driver-preferences rtl-bordered">
                            @foreach ($data['preferences']['driver']['values'] as $preference)
                                <li class="list-group-item" draggable="true" data-driver-id="{{ $preference->id }}">{{ $preference->name }}</li>
                            @endforeach
                        </ol>
                    </div>
                </section>
                <input type="hidden" name="driverPreferences" id="choosen-driver-preferences" value="{{ empty($data['preferences']['driver']['ids']) ? '' : $data['preferences']['driver']['ids'] }}" />
            </fieldset>

            <fieldset class="col-lg-4">
                <legend>Idiomas</legend>
                <section>
                    <div class="col-sm-6">
                        <ol id="spoken-languages-preferences" class="list-group vertical spoken-languages-preferences rtl-sortable-list rtl-bordered rtl-editable">
                            @foreach ($data['staff']['languages'] as $preference)
                                <li class="list-group-item" draggable="true" data-spoken-languages-id="{{ $preference->id }}">{{ $preference->name }}</li>
                            @endforeach
                        </ol>
                    </div>
                    <div class="col-sm-6">
                        <ol class="list-group vertical spoken-languages-preferences rtl-bordered">
                            @foreach ($data['languages']['values'] as $preference)
                                <li class="list-group-item" draggable="true" data-spoken-languages-id="{{ $preference->id }}">{{ $preference->name }}</li>
                            @endforeach
                        </ol>
                    </div>
                </section>
                <input type="hidden" name="spokenLanguagesPreferences" id="choosen-spoken-languages-preferences" value="{{ $data['languages']['ids'] }}"/>
            </fieldset>
        </div>
    {!! Form::close() !!}

    <div class="row">
        <hr class="rtl-divider"/>
        <div class="col-sm-12 text-right" id="staff-button-bar">
            <button class="btn btn-success" id="staff-btnEdit" type="button">Editar</button>
        </div>
    </div>
</div>
@endsection

@include('intranet.partials.snippets.modals.addpostalcode')
@include ('intranet.sections.js-functions.Licences')

@section('footer-javascript')
@parent
<script type="text/javascript">

    var editInProgress = false;

    var staffFormValidator = $('#edit-staff-form').validate({
        onkeyup   : function (element, event) {
            if ($(element).attr('name') == 'name') {
                return false // disable onkeyup for your element named as "name"
            } else { // else use the default on everything else
                if (event.which === 9 && this.elementValue(element) === '') {
                    return
                } else if (element.name in this.submitted || element === this.lastElement) {
                    this.element(element)
                }
            }
        },
        errorClass: 'text-danger',
        validClass: 'text-success',
        onfocusout: function (element) {
            $(element).valid()
        },
        rules     : {
            email         : {
                required: true,
                remote  : {
                    url        : '{{ route('ajax.check.staff.email') }}',
                    type       : 'GET',
                    dataType   : 'json',
                    contentType: 'application/json; charset=utf-8',
                    cache      : false,
                    onkeyup    : false,
                    dataFilter : function (response) {
                        var data  = JSON.parse(response)
                        var state = false
                        switch (data.status) {
                            case 'Found':
                                if (editInProgress) {
                                    state = true;
                                } else {
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese correo ya está registrado.'))
                                }
                                break
                            case 'Not Found':
                                state = true
                                break
                        }

                        return state
                    },
                    error      : function (response) {
                        showNotification('Error', 'error', 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador')
                    }
                }
            },
            pid           : {
                required: true,
                remote  : {
                    url        : '{{ route('ajax.check.staff.personalId') }}',
                    type       : 'GET',
                    dataType   : 'json',
                    contentType: 'application/json; charset=utf-8',
                    cache      : false,
                    onkeyup    : false,
                    dataFilter : function (response) {
                        var data  = JSON.parse(response)
                        var state = false
                        switch (data.status) {
                            case 'Found':
                                if (editInProgress) {
                                    state = true;
                                } else {
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa DNI ya existe.'));
                                }
                                break
                            case 'Not Found':
                                if (data.letter.length > 0) {
                                    showNotification('Letra DNI incorrecta', 'notice', 'La letra correcta es la <b>' + data.letter + '</b>');
                                }
                                $('#personal-id').val(($('#personal-id').val()).toUpperCase());
                                state = true;
                                break
                        }

                        return state
                    },
                    error      : function (response) {
                        showNotification('Error', 'error', 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador')
                    }
                }
            },
            postalCode    : {
                required: true,
                remote  : {
                    url        : '{{ route('ajax.postalcode') }}',
                    type       : 'GET',
                    dataType   : 'json',
                    contentType: 'application/json; charset=utf-8',
                    cache      : false,
                    onkeyup    : false,
                    dataFilter : function (response) {
                        var data = JSON.parse(response)
                        $('#locality-id').val(data['name'])
                        $('#postal-code-val').val(data['id'])

                        return true
                    },
                    error      : function (response) {
                        ajaxStandardErrorTreatment(response.status, null, function () {
                            $('#locality-id').val('El código postal no existe...')
                            $('#addAction').removeClass('hidden')
                            $('#postal-code').focus()
                        })
                    }
                }
            },
            errorElement  : 'em',
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass('help-block')
                if (element.prop('type') === 'checkbox') {
                    error.insertAfter(element.parent('label'))
                } else {
                    error.insertAfter(element)
                }
            },
            highlight     : function (element, errorClass, validClass) {
                $(element).parent().closest('div').addClass('has-error').removeClass('has-success')
            },
            unhighlight   : function (element, errorClass, validClass) {
                $(element).parent().closest('div').addClass('has-success').removeClass('has-error')
            }
        }
    })

    $('#postalCodeModal').on('shown.bs.modal', function () {
        var modal = $(this)

        $.LoadingOverlay('show')
        createNewPromise('{{ route('ajax.localities') }}').then(
            function (response) {
                populateSelect('modal-locality-id', response, 0)
                $('#modal-locality-id').selectpicker('refresh')
                $.LoadingOverlay('hide')
            },
            function (Error) {
                showNotification('Oops! Ha ocurrido un error.', 'error', Error)
                $.LoadingOverlay('hide')
            }
        )

        $('#postal_code_id').val($('#postal-code-id').val())
        $('#postal-code-id').focus()
    })

</script>
@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            updateControlsState('preferences', true);

            $('#staff-btnEdit').on('click', function (event) {
                event.preventDefault();
                updateControlsState('staff', false);

                sortableField('driver');
                sortableField('spoken-languages');

                createExtraButtons('staff');
            })
        })
    </script>
@endsection