@extends('intranet')
@section('header-css')
    {!! Html::style('assets/plugins/DataTables/datatables.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    <section class="content rtl-margins-15">
        <table class="dt table table-hover table-responsive table-stripped rtl-full-width rtl-dt-head-line" id="staff-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>LM</th>
                <th>DNI</th>
                <th>Teléfono</th>
                <th>Correo Electrónico</th>
                <th>Empleado por</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>

        <section class="rtl-lists-footer">
            <div class="container-fluid">
                <section class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4 text-right">
                        <a href="{{ route('admin.staff.new') }}#">
                            <button id="add-new-member" class="btn btn-primary rtl-btn-primary">Agregar asalariado</button>
                        </a>
                    </div>
                </section>
            </div>
        </section>
    </section>
@endsection

@section('footer-javascript')

    @parent

    {!! Html::script('assets/plugins/DataTables/datatables.js') !!}

    <script type="text/javascript">
        $(function () {
            $('#staff-table').DataTable({
                processing: true,
                serverSide: true,
                language  : {
                    'url': "{{asset('assets/js/datatables-spanish.json')}}"
                },
                ajax      : '{{ route('ajax.staff') }}',
                dom       : 'Bfrtip',
                buttons   : ['csv', 'excel', 'pdf', 'print'],
                columns   : [
                    {
                        data         : 'members.licence.municipal_licence',
                        name         : 'members.licence.municipal_licence',
                        title        : 'LM',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if ((oData.members.licence !== undefined) && (oData.members.licence.municipal_licence !== 'N/D')) {
                                $(nTd).html("<span>"+ oData.members.licence.municipal_licence + "</spana>");
                            } else {
                                $(nTd).html("<span></spana>");
                            }

                        }
                    },
                    {
                        data         : 'name',
                        name         : 'name',
                        title        : 'Nombre',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<span>" + oData.name + "&nbsp;" + oData.lastname + "</spana>");
                        }
                    },
                    {
                        data      : 'pid',
                        name      : 'pid',
                        title     : 'DNI',
                        orderable : true,
                        searchable: true
                    },
                    {
                        data         : 'mobile',
                        name         : 'mobile',
                        title        : 'Telefono',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if (oData.mobile !== 'N/A') {
                                $(nTd).html("<a href='tel:" + oData.mobile + "'>" + oData.mobile + "</a>");
                            }
                        }
                    },
                    {
                        data         : 'email',
                        name         : 'email',
                        title        : 'Correo Electronico',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if (oData.email !== 'N/D') {
                                $(nTd).html("<a href='mailto:" + oData.email + "'>" + oData.email + "</a>");
                            } else {
                                $(nTd).html("<span>- Correo no disponible -</spana>");
                            }
                        }
                    },
                    {
                        data         : 'members.name',
                        name         : 'members.name',
                        title        : 'Contratado por',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if (oData.members.hasOwnProperty('name') &&  oData.members.hasOwnProperty('lastname'))
                            {
                                var dataName = oData.members.name === 'N/D' ? 'Sin ' : oData.members.name;
                                var dataLastname = oData.members.lastname === 'N/D' ? 'contrato' : oData.members.lastname;

                                $(nTd).html("<span>" + dataName + "&nbsp;" + dataLastname + "</spana>");
                            }
                        }
                    },
                    {
                        data      : 'action',
                        name      : 'action',
                        orderable : false,
                        searchable: false,
                        width     : '55px',
                        className : 'text-right'
                    },
                ],
            })
        })
    </script>
@endsection