    <br />
    <div class="row">
        <div class="container-fluid">
            <div class="col-md-3">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-olive">
                        <div class="widget-user-image">
                            <i class="fa fa-car fa-4x" style="float:left;"></i>
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Carnet de Conducir</h3>
                        <h5 class="widget-user-desc">Próximos vencimientos</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <div class="widget-user-header bg-olive no-padding"><h4>Licencias</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['expires' => array_filter($expirations['driverLicence-data']), 'type' => 'expires' ])
                        </ul>
                        <div class="widget-user-header bg-olive no-padding"><h4>Personal Asalariado</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['staffExpires' => array_filter($staffExpirations['driverLicence-data']), 'type' => 'staffExpires'])
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue">
                        <div class="widget-user-image">
                            <i class="fa fa-truck fa-4x" style="float:left;"></i>
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Tarjeta de transporte</h3>
                        <h5 class="widget-user-desc">Próximos vencimientos</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <div class="widget-user-header bg-blue no-padding"><h4>Licencias</h4></div>
                        <ul class="nav nav-stacked">

                            @include('intranet.partials.snippets.content.expires', ['expires' => array_filter($expirations['transportCard-data']), 'type' => 'expires' ])
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-red">
                        <div class="widget-user-image">
                            <i class="fa fa-id-card-o fa-4x" style="float:left;"></i>
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Documento Nacional de Identidad</h3>
                        <h5 class="widget-user-desc">Próximos vencimientos</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <div class="widget-user-header bg-red no-padding"><h4>Licencias</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['expires' => array_filter($expirations['pid-data']), 'type' => 'expires' ])
                        </ul>
                        <div class="widget-user-header bg-red no-padding"><h4>Personal Asalariado</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['staffExpires' => array_filter($staffExpirations['pid-data']), 'type' => 'staffExpires' ])
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-orange">
                        <div class="widget-user-image">
                            <i class="fa fa-id-card-o fa-4x" style="float:left;"></i>
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Licencia Municipal</h3>
                        <h5 class="widget-user-desc">Próximos vencimientos</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <div class="widget-user-header bg-orange no-padding"><h4>Licencias</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['expires' => array_filter($expirations['municipalLicence-data']), 'type' => 'expires'  ])
                        </ul>
                        <div class="widget-user-header bg-orange no-padding"><h4>Personal Asalariado</h4></div>
                        <ul class="nav nav-stacked">
                            @include('intranet.partials.snippets.content.expires', ['staffExpires' => array_filter($staffExpirations['municipalLicence-data']), 'type' => 'staffExpires' ])
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>