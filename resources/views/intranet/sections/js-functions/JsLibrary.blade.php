<script type="text/javascript">
    /**
     * Created by Cayetano H. Osma <chernandez@elestadoweb.com>
     * Date: May, 10th 2017
     */

    /**
     * @author Russ Cam <https://stackoverflow.com/users/1831/russ-cam>
     * @link https://stackoverflow.com/questions/1458724/how-do-i-set-unset-cookie-with-jquery
     *
     * You MUST retain this license header!
     *
     */
    function createCookie (name, value, days) {
        var expires

        if (days) {
            var date = new Date()
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
            expires = '; expires=' + date.toGMTString()
        } else {
            expires = ''
        }
        document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value) + expires + '; path=/'
    }

    /**
     * @author Russ Cam <https://stackoverflow.com/users/1831/russ-cam>
     * @link https://stackoverflow.com/questions/1458724/how-do-i-set-unset-cookie-with-jquery
     *
     * You MUST retain this license header!
     *
     */
    function readCookie (name) {
        var nameEQ = encodeURIComponent(name) + '='
        var ca     = document.cookie.split(';')
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i]
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length)
            }
            if (c.indexOf(nameEQ) === 0) {
                return decodeURIComponent(c.substring(nameEQ.length, c.length))
            }
        }
        return null
    }

    /**
     * @author Russ Cam <https://stackoverflow.com/users/1831/russ-cam>
     * @link https://stackoverflow.com/questions/1458724/how-do-i-set-unset-cookie-with-jquery
     *
     * You MUST retain this license header!
     *
     */
    function eraseCookie (name) {
        createCookie(name, '', -1)
    }

    /**
     * @author ComFreek <http://stackoverflow.com/users/603003/comfreek>
     * @link http://stackoverflow.com/a/16069817/603003
     * @license MIT 2013-2015 ComFreek
     * @license[dual licensed] CC BY-SA 3.0 2013-2015 ComFreek
     * You MUST retain this license header!
     *
     * History:
     * July 2016 - remove the call to setCustomValidity as default in InvalidInputHelper function.
     */
    (function (exports) {
        function valOrFunction (val, ctx, args) {
            if (typeof val == 'function') {
                return val.apply(ctx, args)
            } else {
                return val
            }
        }

        function InvalidInputHelper (input, options) {
            function changeOrInput () {
                if (input.value == '') {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]))
                } else {
                    input.setCustomValidity('')
                }
            }

            function invalid () {
                if (input.value == '') {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]))
                } else {
                    input.setCustomValidity(valOrFunction(options.invalidText, window, [input]))
                }
            }

            input.addEventListener('change', changeOrInput)
            input.addEventListener('input', changeOrInput)
            input.addEventListener('invalid', invalid)
        }

        exports.InvalidInputHelper = InvalidInputHelper
    })(window)

    /**
     * Input HTML5 validation messages functions.
     * July 2016 - Add the form parameter to restrict the elements to a specific form.
     *           - Remove defaultText identifier.s
     */
    function registerControlValidation (form, control, messages) {
        var inputElements = document.forms[form].getElementsByTagName(control)
        for (var i = 0; i < inputElements.length; i++) {
            InvalidInputHelper(inputElements[i], {
                // defaultText: 'default Text',
                emptyText  : messages[0],
                invalidText: messages[1]
            })
        }
    }

    /**
     * Call PNotify in order to show a notification box.
     *
     * @param caption   Notification title
     * @param type      'info', 'success', 'error'
     * @param message   Message to show.
     */
    function showNotification (caption, type, message) {
        return new PNotify({
            title  : caption,
            text   : message,
            type   : type,
            delay  : 5000,
            icon   : type === 'error' ? 'fa fa-times' : 'fa fa-check',
            buttons: {
                sticker: false
            }
        });
    }

    function showAlertSesionExpired (url) {
        new PNotify({
            title   : 'Ha caducado la sesión',
            text    : 'Debes identificarte de nuevo para continuar.',
            type    : 'error',
            addclass: 'rtl-laguna-background-important',
            icon    : 'glyphicon glyphicon-info-sign',
            hide    : false,
            confirm : {
                confirm: true,
                buttons: [{
                    text    : 'Login',
                    addClass: 'btn-primary rtl-btn-primary',
                    click   : function (notice) {
                        notice.remove()
                        window.location = url
                    }
                },
                          null]
            },
            buttons : {
                closer : false,
                sticker: false
            },
            history : {
                history: false
            },
            nonblock: {
                nonblock: false
            }
        })
    }

    function dataTablesTranslations (language) {

        var strings = {}
        switch (language) {
            case 'ES':
                strings = {
                    'decimal'       : '',
                    'emptyTable'    : 'No hay datos disponibles para mostrar',
                    'info'          : 'Mostrando _START_ hasta _END_ de _TOTAL_ entradas',
                    'infoEmpty'     : 'Mostrando 0 hasta 0 de 0 elementos',
                    'infoFiltered'  : '(filtrados de _MAX_ total de entradas)',
                    'infoPostFix'   : '',
                    'thousands'     : ',',
                    'lengthMenu'    : 'Mostrar _MENU_ entradas',
                    'loadingRecords': 'Cargando...',
                    'processing'    : 'Procesando...',
                    'search'        : 'Buscar:',
                    'zeroRecords'   : 'No hay registros coindidentes',
                    'paginate'      : {
                        'first'   : 'Primero',
                        'last'    : 'Último',
                        'next'    : 'Siguiente',
                        'previous': 'Anterior'
                    },
                    'aria'          : {
                        'sortAscending' : ': activar para orderar ascendentemente',
                        'sortDescending': ': activar para orderar descendentemente'
                    }
                }
                break
        }

        return strings
    }

    /**
     * Capture the click on row on each single datatable. It redirect to Show view
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param dataTable
     */
    function setupRowClick (dataTable) {
        $(dataTable).on('click', 'tr', function (event) {
            if ($(event.target.parentNode).attr('onClick') == undefined && $(dataTable).DataTable().row(this).data() !== undefined) {
                window.location.href = $('#show-row-' + $(dataTable).DataTable().row(this).data()['id']).attr('href')
            }
        })
    }

    /**
     * Init and configure the spin controls
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param control   control string name
     * @param options   options to add to the control.
     *
     * @see https://www.virtuosoft.eu/code/bootstrap-touchspin/
     */
    function setupTouchSpin (control, options) {

        var defaultOptions = {}
        $.extend(defaultOptions, {
            min            : 1,
            max            : 7,
            step           : 1,
            decimals       : 0,
            boostat        : 5,
            maxboostedstep : 10,
            verticalbuttons: true
        }, options)
        $('input[name=' + control + ']').TouchSpin(defaultOptions)
    }

    /**
     * Populate the selectControl with the data provided.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param control           Control over you want to act
     * @param data              Data to load into control
     * @param selectedOption    Option to be selected, if undefined won't be used
     *
     * @see https://www.virtuosoft.eu/code/bootstrap-touchspin/
     */
    function populateSelect (control, data, selectedOption) {

        $.each(data, function (key, value) {
            var option = $('<option></option>').attr('value', key).text(value)
            if (selectedOption !== undefined) {
                if (key === selectedOption) {
                    option.attr('selected', 'selected')
                }
            }
            $('#' + control).append(option)
        })

    }

    /**
     * Standard error response for ajax connections.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param status            Response status
     * @param message           (Optional) Text message to return to UI
     * @param callbackFunction  (Optional) Callback to do some tasks
     *
     * @see https://www.virtuosoft.eu/code/bootstrap-touchspin/
     */
    function ajaxStandardErrorTreatment (status, message, callbackFunction) {

        switch (status) {
            case 401:
                        {{-- Set cookie to be redirected to the current url --}}
                var route = '{{ route('admin.dashboard') }}'
                if ('{{ Route::getCurrentRoute()->getName() }}' !== '') {
                    route = '{{ Route::getCurrentRoute()->getName() }}'
                }
                createCookie('rtl-state', route, 1)
                showAlertSesionExpired('{{ route('login') }}')
                break
            case 404:
                if (callbackFunction === undefined) {
                    if (message !== undefined) {
                        return message
                    } else {
                        showNotification('Error', 'error', 'Ha ocurrido un error')
                    }
                } else {
                    callbackFunction()
                }
                break
            default:
                callbackFunction
                break
        }
    }

    /**
     * Add an event to selectPicker components
     * enable it when is fully loaded.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @see https://www.virtuosoft.eu/code/bootstrap-touchspin/
     */
    $('.selectpicker').on('loaded.bs.select', function (evt) {
        $(this).attr('disabled', false)
    })

    /**
     * Fill with zeroes by left until fill size chars
     *
     * Taken from https://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
     * @param size
     *
     * @returns {string}
     */
    Number.prototype.pad = function (size) {
        var s = String(this)
        while (s.length < (size || 2)) {
            s = '0' + s
        }

        return s
    }

    /**
     * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
     *
     * @param obj1
     * @param obj2
     * @returns obj3 a new object based on obj1 and obj2
     */
    function mergeObjects (obj1, obj2) {
        var obj3 = {}
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname] }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname] }

        return obj3
    }

    /**
     * Return the today data as string formatted as DD/MM/YYYY
     *
     * @returns {string}
     */
    function today () {
        var today = new Date()
        var dd    = today.getDate()
        var mm    = today.getMonth() + 1 // January is 0!
        var yyyy  = today.getFullYear()

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        return dd + '/' + mm + '/' + yyyy
    }

    /**
     * Promise to get Localitites.
     *
     * @param url
     *
     * @returns {Promise}
     */
    function createNewPromise (url) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest()
            request.open('GET', url)
            request.responseType = 'json'
            request.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
            request.onload  = function () {
                if (request.status === 200) {
                    resolve(request.response)
                } else {
                    reject(Error('No han podido cargarse las localidades; error code:' + request.statusText))
                }
            }
            request.onerror = function () {
                reject(Error('There was a network error.'))
            }
            request.send()
        })
    }

    // DateTimePicker configurations.
    var dateTimePickerDefaultOptions = {
        language      : 'es',
        todayBtn      : 1,
        autoclose     : 1,
        todayHighlight: 1,
        minView       : 2,
        forceParse    : 0,
        format        : 'dd/mm/yyyy',
        pickerPosition: 'bottom-left'
    }
    var dateTimePickerOptionsForFutureDates = mergeObjects(dateTimePickerDefaultOptions, {startDate: moment().format('Y-MM-DD')})
    var dateTimePickerOptionsForNotFutureDates = mergeObjects(dateTimePickerDefaultOptions, { endDate: moment().format('Y-MM-DD')})

    /**
     * Return a camelCase version of string
     *
     * @param string str
     *
     * return {string}
     */
    function toCamelCase (str) {
        return str.toLowerCase()              // Lower cases the string
                  .replace(/[-_]+/g, ' ')     // Replaces any - or _ characters with a space
                  .replace(/[^\w\s]/g, '')    // Removes any non alphanumeric characters
                  // Uppercases the first character in each group immediately following a space (delimited by spaces)
                  .replace(/ (.)/g, function ($1) {
                      return $1.toUpperCase()
                  })
                  .replace(/ /g, '')        // Removes spaces
    }

    /**
     * Return the options selected from list.
     *
     * @param   string listId
     *
     * @returns {string}
     */
    function getActiveOptionsFromList (listId) {
        var list = ''

        $('#' + listId + ' li').each(function () {
            list += $(this).data(toCamelCase(listId)) + ','
        })

        return list.substring(0, list.length - 1)
    }

    /**
     * Return true if the field has a value different than the original.
     * false otherwise.
     * Notice that the comparision is with '==' or '!=' in order to avoid
     * type comparision.
     *
     * @param field
     *
     * @returns {boolean}
     */
    function isDirty (field) {
        var fieldname = $('#' + field);
        var fieldValue = fieldname.val();
        var fieldOriginalValue = fieldname.data('originalvalue');

        if (fieldValue !== undefined && fieldValue !== null) {
            if (typeof fieldOriginalValue !== 'undefined') {
                return (fieldValue != fieldOriginalValue);
            }
        }

        return true;
    }

    /**
     * Return true if browser support drag and drop, FormData and FileReader, False otherwise
     *
     * https://css-tricks.com/drag-and-drop-file-uploading/
     */
    var isAdvancedUpload = function() {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();

</script>

@yield('footer-javascript')

@include('intranet.sections.footer.DocumentReady')
