<script type="text/javascript">
    /**
     * Function related to Licences
     *
     * Created by Cayetano H. Osma <chernandez@elestadoweb.com>
     * Date: Jan 2018
     */

    // Set to match with DNI last number in order to get the month of expiry date.
    // The match rule is:
    // 1 - January   2 - Februry    3 - March       4 - April   5 - May
    // 6 - June      7 - July       8 - September   9 - October 0 - November
    var transportCardMonthMatching = [11, 1, 2, 3, 4, 5, 6, 7, 9, 10]
    const TRANSPORT_CARD_PERIOD    = 5        // Years

    /**
     * Return the appropriate route for each area.
     *
     * @param section
     *
     * @returns {boolean}
     */
    function getRouteForArea(section) {

        var sectionRoute = '';
        switch (section) {
            case 'personal':
                sectionRoute = '{{ route('ajax.licences.update.personaldata') }}';
                break;
            case 'vehicle':
                sectionRoute = '{{ route('ajax.licences.update.vehicledata') }}';
                break;
            case 'document':
                sectionRoute = '{{ route('ajax.licences.update.documentdata') }}';
                break;
            case 'economic':
                sectionRoute = '{{ route('ajax.licences.update.economicdata') }}';
                break;
            case 'staff':
                sectionRoute = '{{ route('ajax.update.staff') }}';
                break;
            case 'preferences':
                sectionRoute = '{{ route('ajax.licences.update.preferences') }}';
                break;
        }

        return sectionRoute;
    }

    /**
     * Enable or disable the controls of a specified area.
     *
     * @param section
     * @param state
     *
     */
    function updateControlsState (section, state) {

        // Remove all disabled and readonly attributes
        $.each($('#' + section + 'Data .rtl-editable'), function (index, value) {
            $('#' + value.id).prop('disabled', state).attr('readonly', state)
        })

        $.each($('#' + section + 'Data .spinbox-buttons'), function (index, value) {
            if (state) {
                $(value).addClass('hidden')
            } else {
                $(value).removeClass('hidden')
            }
        })

        $('.rtl-sortable-list ').sortable(!state ? 'enable' : 'disable');

        $('#' + section + 'Data .selectpicker').selectpicker('refresh');

        if (!state) {
            // Let use the datetimepickers.
            $('#' + section + 'Data .form-date-past').datetimepicker(dateTimePickerDefaultOptions);
            $('#' + section + 'Data .form-date-future').datetimepicker(dateTimePickerOptionsForFutureDates);
            $('#' + section + 'Data .form-date-no-future').datetimepicker(dateTimePickerOptionsForNotFutureDates);

        } else {
            $('#' + section + 'Data .form-date-past').datetimepicker('remove');
            $('#' + section + 'Data .form-date-future').datetimepicker('remove');
            $('#' + section + 'Data .form-date-no-future').datetimepicker('remove');
        }
    }

    /**
     * Remove the extra buttons, those are save and cancel.
     *
     * @param section
     */
    function removeExtraButtons(section) {
        $('#' + section + '-btnCancel').remove();
        $('#' + section + '-btnSave').remove();
        $('#' + section + '-btnEdit').show();
    }

    /**
     * Create the extra buttons, those are, save and cnacel
     * and bind the events to them
     *
     * @param section
     */
    function createExtraButtons(section) {
        var buttonBar = $('#' + section + '-button-bar');
        buttonBar.prepend('<button class="btn btn-danger" id="' + section +'-btnCancel">Cancelar</button>');
        buttonBar.append('<button class="btn btn-primary" id="' + section +'-btnSave">Grabar</button>');
        $('#' + section + '-btnEdit').hide();

        $('#' + section + '-btnSave').on('click', function (event) {
            event.preventDefault();
            var sectionForm = $('#'+ section +'DataForm');

            sectionForm.validate();
            if (sectionForm.valid()) {
                sendSubmit(section, event);
            }
        });

        $('#' + section + '-btnCancel').on('click', function (event) {
            event.preventDefault()
            removeExtraButtons(section);
            updateControlsState(section, true);
        });
    }

    /**
     * Do a submit of form.
     *
     * @param section string   name of involved area
     * @param evt           events argument
     */
    function sendSubmit (section, evt) {
        evt.preventDefault();
        var sectionForm = $('#'+ section +'DataForm');
        var sectionDivContainer = $('#'+ section +'Data').parents('div[class^="nav-tabs-custom"]')
        var disabled = sectionForm.find(':input:disabled').removeAttr('disabled');
        var formData = sectionForm.serialize();
        var route = getRouteForArea(section);

        disabled.attr('disabled', 'disabled');
        sectionDivContainer.LoadingOverlay('show');
        $.ajax({
            type   : 'POST',
            url    : route,
            data   : formData,
            success: function (data) {
                sectionDivContainer.LoadingOverlay('hide');
                showNotification('Registro actualizado', 'success', 'Los datos del Socio han sido grabados con éxito.');
                removeExtraButtons(section);
                updateControlsState(section, true);
            },
            error  : function (data) {
                sectionDivContainer.LoadingOverlay('hide');
                if (data.responseJSON['code'] === undefined && data.responseJSON['message'] === undefined) {
                    var errorInfo = JSON.parse(data.responseJSON.response);
                    $.each(errorInfo, function (key, valueObj) {
                        showNotification('ERROR: ' + key, 'error', valueObj);
                    })
                } else {
                    console.log(data.responseJSON['code'], data.responseJSON['message']);
                    showNotification('ERROR', 'error', 'Ha ocurrido un error durante la operación. Consulte la consola.');
                }
                return false
            },
            complete: function() {

            }
        })
    }

    /**
     * Load the selected items into hidden control
     *
     * @param section   name of the control without suffix '-preferences'
     */
    function sortableField(section) {

        if (section !== undefined && section.length > 0 && section != null) {
            var group = $('ol.' + section + '-preferences').sortable({
                group      : section + '-preferences',
                onDragStart: function ($item, container, _super) {
                    if (!container.options.drop) {
                        $item.clone().insertAfter($item)
                    }
                    _super($item, container)
                },
                onDrop: function ($item, container, _super) {
                    var items = [];
                    $.each($('#' + section + '-preferences li'), function(key, item) {
                        items.push($(item).data(section + '-id'));
                    });
                    $('#choosen-' + section + '-preferences' ).val(items.join(','))
                    _super($item, container);
                }
            })
        }
    }

</script>

