<!--[if lt IE 9]>
{!! Html::script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') !!}
{!! Html::script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') !!}

<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
<![endif]-->
@yield ('header-js')