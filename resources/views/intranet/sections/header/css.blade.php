{!! Html::style('assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/ionicons-2.0.1/css/ionicons.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/Animate-v3.5.2/animate.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/PNotify-3.2.0/pnotify.custom.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/Bootstrap-toggle-v2.2.0/css/bootstrap-toggle.min.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/FuelUX-3.16.1/css/fuelux.min.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/bootstrap-select-1.12.4/dist/css/bootstrap-select.min.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/bootstrap-datetimepicker-2.4.4/css/bootstrap-datetimepicker.min.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/plugins/jQuery-sortable/jquerySortable.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}

{!! Html::style('assets/css/AdminLTE.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/css/' . Config::get('radiotaxilaguna.skin') . '.min.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
{!! Html::style('assets/css/rtl.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
@yield('header-css')

