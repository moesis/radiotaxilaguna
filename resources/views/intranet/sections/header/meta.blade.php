<title>{{ Config::get('app.name') }} - @yield('title')</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Intranet Radio Taxi Laguna"/>
<meta name="keywords" content="admin,restricted"/>

@yield ('header-meta')


