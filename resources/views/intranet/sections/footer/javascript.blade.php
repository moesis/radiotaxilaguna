{{-- Mainly scripts --}}
{!! Html::script('assets/plugins/jQuery/jquery-2.2.3.min.js') !!}
{!! Html::script('assets/plugins/jQueryUI/jquery-ui.min.js') !!}

<script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
</script>

{!! Html::script('assets/plugins/bootstrap-3.3.7/js/bootstrap.js') !!}
{!! Html::script('assets/plugins/PNotify-3.2.0/pnotify.custom.js') !!}
{!! Html::script('assets/plugins/Bootstrap-toggle-v2.2.0/js/bootstrap-toggle.min.js') !!}
{!! Html::script('assets/plugins/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.js') !!}
{!! Html::script('assets/plugins/FuelUX-3.16.1/js/fuelux.min.js') !!}
{!! Html::script('assets/plugins/JQuery-validator-1.17.0/jquery.validate.min.js') !!}
{!! Html::script('assets/plugins/JQuery-validator-1.17.0/additional-methods.min.js') !!}
{!! Html::script('assets/plugins/JQuery-validator-1.17.0/localization/messages_es.min.js') !!}
{!! Html::script('assets/plugins/loading-overlay-1.5.3/src/loadingoverlay.min.js') !!}
{!! Html::script('assets/plugins/bootstrap-select-1.12.4/dist/js/bootstrap-select.min.js') !!}
{!! Html::script('assets/plugins/Moment-2.10.0/moment-with-locales.min.js') !!}
{!! Html::script('assets/plugins/bootstrap-datetimepicker-2.4.4/js/bootstrap-datetimepicker.js') !!}
{!! Html::script('assets/plugins/bootstrap-datetimepicker-2.4.4/js/locales/bootstrap-datetimepicker.es.js') !!}
{!! Html::script('assets/plugins/jQuery-sortable/jquerySortable-0.9.13.js') !!}

{!! Html::script('assets/js/app.min.js') !!}

@include ('intranet.sections.js-functions.JsLibrary')
