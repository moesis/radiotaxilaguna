<script type="text/javascript">
    {{-- Main JQuery function when DOM is fully rendered --}}
    $(document).ready(function () {

        {{-- Activate the Bootstrap tooltips --}}
        $().tooltip();

        {{-- Init Pnotify - Bootstrap Style. --}}
        PNotify.prototype.options.styling = 'bootstrap3'

        {{-- Setup onRowClick event for Datatables. --}}
        if ($('.dt') !== undefined) {
            setupRowClick('.dt')
        }

        {{-- Setup the overlay layer. --}}
        $.LoadingOverlaySetup({
            color: 'rgba(0, 0, 0, 0.4)',
            image: '{{ asset('assets/img/blocks.gif') }}'
        })

    })
</script>

@yield('document-ready')


