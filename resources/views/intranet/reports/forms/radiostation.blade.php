@extends('intranet')
@section('header-css')
    {!! Html::style('assets/plugins/DataTables/datatables.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')
    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    <div class="fluid-content">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="panel" id="report-panel">
                    <div class="panel-header">
                        {!! Form::open(['url' => '#', 'id' => 'stationgpsReportFormId', 'name' => 'stationgpsReportForm', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                        <div class="panel">
                            <div class="panel-body">
                                <div class="panel-header">
                                    <h4>Seleccione los modelo de radio y de GPS para el informe.</h4>
                                    <div class="row">
                                        <div class="col-sm-6"><h4>Emisoras</h4></div>
                                        <div class="col-sm-6"><h4>GPS</h4></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="well" id="stationWell">
                                            <div class="checkbox">
                                                <input type="checkbox" id="all-stations-id" name="allStations" value="all-station"/>
                                                <label for="all-stations-id">Todas las marcas de emisoras</label>
                                            </div>
                                            @foreach ($data['station'] as $stationKey => $stationValue)

                                                <div class="checkbox">
                                                    <input type="checkbox" id="{{ $stationKey . 'Id' }}" name="station[]" value="{{ $stationKey }}"/>
                                                    <label for="{{ $stationKey . 'Id' }}"> {{ $stationValue }}</label>
                                                </div>

                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="well" id="gpsWell">
                                            <div class="checkbox">
                                                <input type="checkbox" id="all-gps-id" name="allGps" value="all-gps"/>
                                                <label for="all-gps-id">Todas las marcas de GPS</label>
                                            </div>
                                            @foreach ($data['gps'] as $gpsKey => $gpsValue)

                                                <div class="checkbox">
                                                    <input type="checkbox" id="{{ $gpsKey . 'Id' }}" name="gps[]" value="{{ $gpsKey }}"/>
                                                    <label for="{{ $gpsKey . 'Id' }}"> {{ $gpsValue }}</label>
                                                </div>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <button type="button" class="btn rtl-btn-primary" id="reportBtn">Generar informe</button>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="well hidden" id="reportZoneId">
                        <section class="content rtl-margins-15">
                            <table class="dt table table-hover table-responsive table-stripped rtl-full-width rtl-dt-head-line" id="report-table">
                                <thead>
                                <tr>
                                    <th class="col-md-1">Lic. Mun.</th>
                                    <th>Nombre</th>
                                    <th class="col-md-2">Emisora</th>
                                    <th class="col-md-2">GPS</th>
                                    <th class="col-md-2">Móvil</th>
                                    <th>Correo Electrónico</th>
                                </tr>
                                </thead>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-javascript')
    @parent

    {!! Html::script('assets/plugins/DataTables/datatables.js') !!}

    <script type="text/javascript">
        function doReport (event) {
            $.ajax({
                type      : 'GET',
                url       : "{{ route('ajax.reports.stationandgps') }}",
                data      : $('#stationgpsReportFormId').serializeArray(),
                beforeSend: function () {
                    $('#report-panel').LoadingOverlay('show')
                },
                success   : function (response) {
                    $('#reportZoneId').removeClass('hidden')
                    $('#report-table').DataTable({
                        processing: true,
                        data      : response.data,
                        dom       : 'Bfrtip',
                        language  : {
                            'url': "{{asset('assets/js/datatables-spanish.json')}}"
                        },
                        buttons   : ['csv', 'excel', 'pdf', 'print'],
                        columns   : [
                            {
                                data      : 'municipal_licence',
                                name      : 'municipal_licence',
                                title     : 'Lic. Mun.',
                                orderable : true,
                                searchable: true
                            },
                            {
                                data         : 'member.name',
                                name         : 'member.name',
                                title        : 'Nombre',
                                orderable    : true,
                                searchable   : true,
                                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                                    $(nTd).html('<span>' + oData.member.name + '&nbsp;' + oData.member.lastname + '</spana>')
                                }
                            },
                            {
                                data      : 'station_brand',
                                name      : 'station_brand',
                                title     : 'Emisora',
                                orderable : true,
                                searchable: false
                            },
                            {
                                data      : 'gps_model',
                                name      : 'gps_model',
                                title     : 'Emisora',
                                orderable : true,
                                searchable: false
                            },
                            {
                                data         : 'member.mobile',
                                name         : 'member.mobile',
                                title        : 'Móvil',
                                orderable    : true,
                                searchable   : true,
                                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                                    if (oData.member.mobile !== 'N/A') {
                                        $(nTd).html('<a href=\'tel:' + oData.member.mobile + '\'>' + oData.member.mobile + '</a>')
                                    }
                                }
                            },
                            {
                                data         : 'member.email',
                                name         : 'member.email',
                                title        : 'Correo Electronico',
                                orderable    : true,
                                searchable   : true,
                                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                                    if (oData.member.email !== 'N/D') {
                                        $(nTd).html('<a href=\'mailto:' + oData.member.email + '\'>' + oData.member.email + '</a>')
                                    } else {
                                        $(nTd).html('<span>- Correo no disponible -</spana>')
                                    }
                                }
                            }
                        ]
                    })
                },
                error     : function (data) {
                    return false
                },
                complete  : function () {
                    $('#report-panel').LoadingOverlay('hide')
                }
            })
        }
    </script>
@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            var stationHeight = $('#stationWell').height()
            var gpsHeight     = $('#gpsWell').height()

            if (stationHeight > gpsHeight) {
                $('#gpsWell').height(stationHeight)
            } else {
                $('#stationWell').height(gpsHeight)
            }

            $('#reportBtn').on('click', function (event) {
                event.preventDefault()
                var stationCount = $('input:checkbox[name=\'station[]\']:checked').length + $('input:checkbox[name=\'allStations\']:checked').length
                var gpsCount     = $('input:checkbox[name=\'gps[]\']:checked').length + $('input:checkbox[name=\'allStations\']:checked').length

                if (stationCount === 0 || gpsCount === 0) {
                    showNotification('ERROR', 'error', 'Debe seleccionar, al menos, un modelo de emisora y un modelo de GPS')
                    return false
                }

                doReport(event)
            })

            $('#all-stations-id').on('click', function (event) {
                $('input:checkbox[name=\'station[]\']').each(function () {
                    if ($(this).is(':disabled')) {
                        $(this).removeAttr('disabled')
                    } else {
                        $(this).attr('disabled', true)
                    }
                })
            })

            $('#all-gps-id').on('click', function (event) {
                $('input:checkbox[name=\'gps[]\']').each(function () {
                    if ($(this).is(':disabled')) {
                        $(this).removeAttr('disabled')
                    } else {
                        $(this).attr('disabled', true)
                    }
                })
            })

        })
    </script>
@endsection