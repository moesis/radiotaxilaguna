@extends('intranet')
@section('header-css')
    {!! Html::style('assets/plugins/DataTables/datatables.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    {{--<section class="content">--}}
        {{--<table class="dt table table-hover table-responsive table-stripped rtl-full-width rtl-dt-head-line" id="members-table">--}}
            {{--<thead>--}}
            {{--<tr >--}}
                {{--<th>LM</th>--}}
                {{--<th>Asociación</th>--}}
                {{--<th>Nombre</th>--}}
                {{--<th>Teléfono</th>--}}
                {{--<th>Correo Electrónico</th>--}}
                {{--<th>Acciones</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
        {{--</table>--}}
    {{--</section>--}}

@endsection

@section('footer-javascript')
    @parent
    {{--{!! Html::script('assets/plugins/DataTables/datatables.js') !!}--}}
    <script type="text/javascript">
        {{--$(function () {--}}
            {{--$('#members-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax      : '{{ route('ajax.members') }}',--}}
                {{--columns   : [--}}
                    {{--{--}}
                        {{--data      : 'municipal_licence',--}}
                        {{--name      : 'municipal_licence',--}}
                        {{--title     : 'L.M.',--}}
                        {{--orderable : true,--}}
                        {{--searchable: true--}}
                    {{--},--}}
                    {{--{--}}
                        {{--data      : 'associationName',--}}
                        {{--name      : 'associationName',--}}
                        {{--title     : 'Asociación',--}}
                        {{--orderable : true,--}}
                        {{--searchable: true--}}
                    {{--},--}}
                    {{--{--}}
                        {{--data      : 'name',--}}
                        {{--name      : 'name',--}}
                        {{--title     : 'Nombre',--}}
                        {{--orderable : true,--}}
                        {{--searchable: true--}}
                    {{--},--}}
                    {{--{--}}
                        {{--data      : 'mobile',--}}
                        {{--name      : 'mobile',--}}
                        {{--title     : 'Telefono',--}}
                        {{--orderable : true,--}}
                        {{--searchable: true,--}}
                        {{--fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {--}}
                            {{--if (oData.phone !== 'N/A') {--}}
                                {{--$(nTd).html("<a href='tel:" + oData.phone + "'>" + oData.phone + "</a>");--}}
                            {{--}--}}
                        {{--},--}}
                    {{--},--}}
                    {{--{--}}
                        {{--data      : 'email',--}}
                        {{--name      : 'email',--}}
                        {{--title     : 'Correo Electronico',--}}
                        {{--orderable : true,--}}
                        {{--searchable: true,--}}
                        {{--fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {--}}
                            {{--if (oData.email != null) {--}}
                                {{--$(nTd).html("<a href='mailto:" + oData.email + "'>" + oData.email + "</a>");--}}
                            {{--}--}}
                        {{--}--}}
                    {{--},--}}
                    {{--{--}}
                        {{--data      : 'action',--}}
                        {{--name      : 'action',--}}
                        {{--orderable : false,--}}
                        {{--searchable: false,--}}
                        {{--width     : '55px',--}}
                        {{--className : 'text-right'--}}
                    {{--},--}}
                {{--],--}}
            {{--})--}}
        {{--})--}}
    </script>
@endsection