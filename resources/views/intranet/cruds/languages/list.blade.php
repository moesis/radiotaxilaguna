@extends('intranet')
@section('header-css')
    {!! Html::style('assets/plugins/DataTables/datatables.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    <section class="content" id="crud-section">

        <div class="row">
            <div class="col-sm-12 col-sm-push-1">
                <div class="col-sm-4">
                    <div class="panel">
                        <!-- Default panel contents -->
                        <div class="panel-heading rtl-default-background rtl-broken-white"><h4>Registros</h4></div>
                        <div class="panel-body">
                            <!-- Table -->
                            <table class="table table-hover">
                                <thead>
                                <th>Idioma</th>
                                <th class="col-sm-1">Acciones</th>
                                </thead>
                                <tbody>
                                @foreach($data['records'] as $recordId => $recordData)
                                    <tr>
                                        <td>{{ $recordData }}</td>
                                        <td class="text-right">
                                            <a id="edit-record" data-record="{{ $recordId }}" href="#" data-toggle="tooltip" data-placement="bottom" class="rtl-edit-icon" title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                                            <a id="delete-record" data-record="{{ $recordId }}" href="#" data-toggle="tooltip" data-placement="bottom" class="rtl-delete-icon" title="Eliminar"><i class="fa fa-close"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group"><div class="text-right col-sm-12"><button type="button" class="btn btn-primary rtl-btn-primary">Nuevo</button></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel hidden" id="edition-panel">
                        <!-- Default panel contents -->
                        <div class="panel-heading rtl-default-background rtl-broken-white"><h4>Edición</h4></div>
                        <div class="panel-body">
                            <form class="form-horizontal" id="crud_form">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

@section('footer-javascript')
    @parent
    <script type="text/javascript">

        function mountForm (resource, fieldDefinitions) {
            $.each(fieldDefinitions, function (key, value) {
                var formElement = ''
                switch (value['controlType']) {
                    case 'INPUT':
                        formElement = '<div class="form-group"><label for="field_' + key + '" class="col-sm-2 control-label">' + value['label'] + '</label><div class="col-sm-10"><input type="' + value['type'] + '" value="' + value['value'] + '" class="form-control" id="field_' + key + '" name="' + key + '"/></div></div>';
                        break
                    case
                    'SELECT':
                        break
                }
                $('#crud_form').append(formElement);
            })
            $('#crud_form').append('<div class="form-group"><div class="text-right col-sm-12"><button type="button" class="btn btn-primary rtl-btn-primary">Grabar</button>&nbsp;<button type="button" class="btn btn-primary rtl-btn-danger">Cancelar</button></div></div>');
        }

        function editRecord () {
            var recordId = $(this).data('record')
            event.preventDefault()
            $.ajax({
                type      : 'GET',
                url       : '{{ route('admin.crud.get.record') }}',
                data      : {'resource': 'language', 'record': recordId},
                beforeSend: function () {
                    $('#crud-section').LoadingOverlay('show')
                },
                success   : function (data) {
                    mountForm('language', data['record'])
                    $('#edition-panel').removeClass('hidden');
                },
                error     : function (jqXHR, exception) {
                    console.log(jqXHR)
                    console.log(exception)
                    showNotification('Oops! Ha ocurrido un error', 'error', 'Ha ocurrido un error durante la grabación de los datos.')
                },
                complete  : function () {
                    $('#crud-section').LoadingOverlay('hide')
                }
            })
        }

        function deleteRecord () {
            return false
        }

        $(document).ready(function () {

            $('#edit-record').on('click', editRecord)
            $('#delete-record').on('click', deleteRecord)

        })
    </script>
@endsection