<header class="main-header">
@include('intranet.partials.logo')
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @include('intranet.partials.header.icons-menu')
                @include('intranet.partials.header.notifications')
                @include('intranet.partials.header.tasks')
                @include('intranet.partials.header.user-account')
                <li><a href="#" data-toggle="control-sidebar" tabindex="-1"><i class="fa fa-gears"></i></a></li>
            </ul>
        </div>
    </nav>
</header>

{{-- Modals Section --}}
@include('intranet.partials.snippets.modals.suggestions')
{{-- End Modal section --}}