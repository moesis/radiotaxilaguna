<div class="modal fade" id="vehicle-notes" tabindex="-1" role="dialog" aria-labelledby="vehicleNotesLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Anotaciones sobre el vehículo</h4>
            </div>
            {!! Form::open(['url' => '#', 'id' => 'annotationsForm', 'name' => 'AnnotationsForm', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <input type="hidden" id="modal-vehicle-id" value="0" name="vehicle_id">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-horizontal">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <label for="annotations-id" class="control-label">Anotaciones</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea class="col-sm-12 resizable-vertical" rows="10" id="annotations-id" name="notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnSave">Guardar Cambios</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>


@section('footer-javascript')
    @parent
    <script type="text/javascript">

        $('#vehicle-notes').on('show.bs.modal', function (event) {
            var vehicleId = $(event.relatedTarget).closest('form').find('input#vehicle-id:hidden').not('script').val();
            $(this).find('#modal-vehicle-id').val(vehicleId);
            $(this).find('#annotations-id').empty();
        })

        $('#btnSave').on('click', function (event) {
            event.preventDefault()
            $.ajax({
                type      : 'POST',
                url       : '{{ route('ajax.add.notes') }}',
                data      : $('#annotationsForm').serialize(),
                beforeSend: function () {
                    $('#vehicle-notes').LoadingOverlay('show')
                },
                success   : function (data) {
                    showNotification('Operación realizada', 'success', 'Se han grabado los datos correctamente.')
                },
                error     : function (jqXHR, exception) {
                    console.log(jqXHR)
                    console.log(exception)
                    showNotification('Oops! Ha ocurrido un error', 'error', 'Ha ocurrido un error durante la grabación de los datos.')
                },
                complete  : function () {
                    $('#annotationsForm').modal('hide')
                    $('#vehicle-notes').LoadingOverlay('hide')
                }
            })
        })

    </script>
@endsection