<div class="modal fade" id="suggestions" tabindex="-1" role="dialog" aria-labelledby="suggestionsLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div id="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="caption"></h4>
            </div>
            {!! Form::open(['url' => '#', 'id' => 'feedbackForm', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('type', null, ['id' => 'type_id']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 hidden" id="messages-id"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-justify">A través de este formulario puede hacer llegar
                            <span id="message"></span> la aplicación a los desarrolladores y directivos de
                            <strong>{{ Config::get('radiotaxilaguna.company.name') }} {{ Config::get('radiotaxilaguna.company.surname') }}{{ Config::get('radiotaxilaguna.company.suffix') }}</strong>
                        </p>
                        <p class="text-justify">Cuanto más preciso sea en su texto, más sencillo será su evaluación y corrección si procede.</p>
                        <p class="text-justify">Los datos solicitados en el presente formulario serán tratados única y exclusivamente con el fin de contacto para aclarar las dudas que puedan surgir.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('username_id', 'Nombre', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('username', $data['login']['user']->name, ['id' => 'username_id', 'class' => 'form-control readonly', 'placeholder' => 'Introduzca su nombre', 'readonly' => 'readonly']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email_id', 'C. Electrónico', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('email', $data['login']['user']->email, ['id' => 'email_id', 'class' => 'form-control readonly', 'placeholder' => 'Introduzca su correo electrónico', 'readonly' => 'readonly']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('text_id', 'Texto', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {{ Form::textarea('texto', null, ['id' => 'text_id', 'col' => 5, 'row' => 5, 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-2 text-left rtl-margin-top-20">
                        <div id="ajax-panel" class="overlay hidden"><i class="fa fa-refresh fa-spin"></i></div>
                    </div>
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="form-submit">Enviar</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>