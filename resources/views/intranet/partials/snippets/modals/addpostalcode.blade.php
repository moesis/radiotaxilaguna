<div class="modal fade" id="postalCodeModal" tabindex="-1" role="dialog" aria-labelledby="postalCodeLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header rtl-laguna-bright-background rtl-broken-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="caption">Agreagar un código postal.</h4>
            </div>

            {!! Form::open(['url' => '#', 'id' => 'postalCodeForm', 'name' => 'postalCodeModalForm', 'method' => 'post', 'class' => 'form-horizontal']) !!}

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 hidden" id="messages-id"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="postal-code" class="col-sm-2 control-label text-right">C.P.</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="postal_code" id="postal_code_id" placeholder="Códido Postal" value="{{ old('postal_code') }}"
                                       minlength="5" data-msg-minlength="Mín. 5 car."
                                       maxlength="5" data-msg-maxlength="Máx. 5 car." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="place_id" class="col-sm-2 control-label text-right">Lugar</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="place" id="place_id" placeholder="Escriba el lugar correspondiente"
                                       minlength="3" data-msg-minlength="Mín. 3 car."
                                       maxlength="30" data-msg-maxlength="Máx. 30 car."/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="locality_id" class="col-sm-2 control-label text-right">Localidad</label>
                            <div class="col-sm-9">
                                <select id="modal-locality-id" class="form-control selectpicker" name="locality_id"
                                        title="Seleccione una localidad de la lista"
                                        data-selected-text-format="count"
                                        data-live-search="true"
                                        required data-msg-required="Requerido"></select>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn rtl-btn-default" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn rtl-btn-primary" id="post-code-form-submit" value="Grabar" />
                {{--<button type="button" class="btn rtl-btn-primary" id="postalCode-submit">Grabar</button>--}}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>


@section('footer-javascript')
    @parent
    <script type="text/javascript">

        $('#postalCodeForm').on('submit', function(e){
            e.preventDefault();
            var validPostalCodeForm = $("#postalCodeForm").validate();
            if (validPostalCodeForm.form()) {
                $.ajax({
                    type      : 'POST',
                    url       : '{{ route('ajax.postalcode.save') }}',
                    data      : $("#postalCodeForm").serialize(),
                    beforeSend: function () {
                        $('#postalCodeModal').LoadingOverlay("show");
                    },
                    success   : function (data) {
                        $('#postalCodeModal').LoadingOverlay("hide");
                        $('#postalCodeModal').modal('hide');
                        showNotification('Operación realizada', 'success', 'Se han grabado los datos correctamente.');
                    },
                    error     : function (jqXHR, exception) {
                        console.log(jqXHR);
                        console.log(exception);
                        $('#postalCodeModal').LoadingOverlay("hide");
                        showNotification('Oops! Ha ocurrido un error', 'error', 'Ha ocurrido un error durante la grabación de los datos.');
                    }
                });
            }
        });

    </script>
@endsection