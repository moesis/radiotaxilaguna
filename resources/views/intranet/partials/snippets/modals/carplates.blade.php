<div class="modal fade" id="car-plate-modal" tabindex="-1" role="dialog" aria-labelledby="carPlateModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Matrícula encontrada</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-horizontal">
                        <div class="row">

                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Matrícula</label>
                                </div>
                                <div class="col-sm-2">
                                    <input class="form-control text-right "
                                           type="text"
                                           name="carPlate" id="plateModal-car-plate-id"
                                           value=""
                                           readonly disabled/>
                                </div>

                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Vehiculo</label>
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control"
                                           type="text"
                                           name="vehicle" id="plateModal-vehicle-id"
                                           value=""
                                           readonly disabled/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Socio</label>
                                </div>
                                <div class="col-sm-5">
                                    <input class="form-control"
                                           type="text"
                                           name="member" id="plateModal-member-id"
                                           value=""
                                           readonly disabled/>
                                </div>
                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Lic. Municipal</label>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control"
                                           type="text"
                                           name="municipalLicence" id="plateModal-municipal-licence-id"
                                           value=""
                                           readonly disabled/>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Correo electrónico</label>
                                </div>
                                <div class="col-sm-10">
                                    <input class="form-control"
                                           type="text"
                                           name="email" id="plateModal-email-id"
                                           value=""
                                           readonly disabled/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Móvil</label>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control text-right "
                                           type="text"
                                           name="mobile" id="plateModal-mobile-id"
                                           value="+34 687,922,546"
                                           readonly disabled/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label class="control-label">Teléfono</label>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control"
                                           type="text"
                                           name="phone" id="plateModal-phone-id"
                                           value=""
                                           readonly disabled/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="telegram-id" class="control-label" style="margin-left:-10px !important; margin-right: 10px;">Telegram</label>
                                </div>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <input class="form-control text-right" type="text"
                                               name="telegram" id="plateModal-telegram-id"
                                               value=""
                                               readonly disabled/>
                                        <span class="input-group-addon">
                                            <a href="" target="_blank" id="plateModal-telegram-link-id">
                                                <span class="fa fa-paper-plane-o input-link" aria-hidden="true"></span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <a href="" id="go-to-licence-id"><button type="button" class="btn btn-primary">Ver Licencia</button></a>
            </div>
        </div>
    </div>
</div>