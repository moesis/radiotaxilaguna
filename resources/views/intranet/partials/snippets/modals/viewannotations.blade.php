<div class="modal fade" id="view-vehicle-notes" tabindex="-1" role="dialog" aria-labelledby="viewvehicleNotesLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Anotaciones del vehículo</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <table id="tblNotes" class="table table-striped table-responsive table-hover">
                        <thead>
                        <tr>
                            <th class="col-sm-2">Fecha</th>
                            <th>notas</th>
                        </tr>
                        </thead>
                        <tbody id="tblNotesBody">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">

        function addRows (data) {
            var htmlCode = ''

            for (var i = 0; i < data['notes'].length; i++) {
                htmlCode += '<tr><td>' + moment(data['notes'][i]['date']).format('DD/MM/YYYY') + '</td><td>' + data['notes'][i]['text'] + '</td></tr>'
            }

            return htmlCode
        }

        $('#view-vehicle-notes').on('show.bs.modal', function (event) {
            var vehicleId = $(event.relatedTarget).closest('form').find('input#vehicle-id:hidden').not('script').val()
            $.ajax({
                type      : 'GET',
                url       : '{{ route('ajax.get.notes') }}',
                data      : {'vehicle_id': vehicleId},
                beforeSend: function () {
                    $('#vehicle-notes').LoadingOverlay('show')
                },
                success   : function (data) {
                    $('#tblNotes > tbody').html('')
                    $('#tblNotes > tbody').append(addRows(data))
                },
                error     : function (jqXHR, exception) {
                    console.log(jqXHR)
                    console.log(exception)
                    showNotification('Oops! Ha ocurrido un error', 'error', 'Ha ocurrido un error durante la grabación de los datos.')
                },
                complete  : function () {
                    $('#vehicle-notes').LoadingOverlay('hide')
                }
            })
        })

    </script>
@endsection
