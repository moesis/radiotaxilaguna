
@if($type == 'expires')
@forelse($expires as $key => $record)
    <li>
        <a href="{{ route('admin.licence.show', ['id' => $record['id']]) }}"><i class="fa fa-user"></i> {{ $record['lm'] }} - {{ $record['name'] }} - {{  $record['date'] }}
            @if ($record['status'] === 200)
                <span class="pull-right badge bg-blue" data-toggle="tooltip" data-placement="left" title="Último aviso enviado el {{ $record['last-date'] }}">
                    <i class="fa fa-check"></i>
                </span>
            @else
                @if ($record['status'] === 404)
                    <span class="pull-right badge bg-purple" data-toggle="tooltip" data-placement="left" title="Cuenta Teleegram no disponible">
                        <i class="fa fa-exclamation-triangle"></i>
                    </span>
                @else
                    <span class="pull-right badge bg-red" data-toggle="tooltip" data-placement="left" title="Fallo en el envío.">
                        <i class="fa fa-times"></i>
                    </span>
                @endif
            @endif
        </a>
    </li>
@empty
        <li>
            <a href="#">No hay vencimientos para personal de licencias para los próximos {{ $days['driver_licence_expiration_date'] }} días
                <span class="pull-right badge bg-blue">
                    <i class="fa fa-check"></i>
                </span>
            </a>
        </li>
@endforelse
@endif

@if($type == 'staffExpires')
@forelse($staffExpires as $key => $record)
    <li>
        <a href="{{ route('admin.staff.show', ['id' => $record['id']]) }}"><i class="fa fa-user-o"></i> @if($record['members']['licence']['municipal_licence'] != 'LM-'){{ $record['members']['licence']['municipal_licence'] }} - @endif{{ $record['name'] }} - {{  $record['date'] }}
            @if ($record['status'] === 200)
                <span class="pull-right badge bg-blue" data-toggle="tooltip" data-placement="left" title="Último aviso enviado el {{ $record['last-date'] }}">
                    <i class="fa fa-check"></i>
                </span>
            @else
                @if ($record['status'] === 404)
                    <span class="pull-right badge bg-purple" data-toggle="tooltip" data-placement="left" title="Cuenta Teleegram no disponible">
                        <i class="fa fa-exclamation-triangle"></i>
                    </span>
                @else
                    <span class="pull-right badge bg-red" data-toggle="tooltip" data-placement="left" title="Fallo en el envío.">
                        <i class="fa fa-times"></i>
                    </span>
                @endif
            @endif
        </a>
    </li>
@empty
    <li>
        <a href="#">No hay vencimientos para personal asalariado para los próximos {{ $days['driver_licence_expiration_date'] }} días
            <span class="pull-right badge bg-blue">
                    <i class="fa fa-check"></i>
                </span>
        </a>
    </li>
@endforelse
@endif