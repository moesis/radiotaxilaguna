<div class="step-pane form-horizontal" data-step="2">
    <h3>Datos de Licencia</h3>
    <div class="container-fluid">
        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="association" class="control-label">Asociación</label>
                </div>
                <div class="col-sm-3">
                    <select id="association-id" class="form-control selectpicker" name="association_id"
                            title="Seleccione una asociación de la lista"
                            data-selected-text-format="count"
                            data-live-search="true"
                            required data-msg-required="Requerido"></select>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="rest-day-id" class="control-label">Día Libre</label>
                </div>
                <div class="col-sm-2">
                    <select id="rest-day" class="form-control selectpicker" name="rest_day" data-live-search="true" required data-msg-required="Requerido">
                        <option value="">Seleccione una opción</option>
                        @foreach (Config::get('radiotaxilaguna.restDays') as $keyDay => $valueDay)
                            <option value="{{ $keyDay }}">{{ $valueDay }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="list-id" class="control-label">Lista</label>
                </div>
                <div class="col-sm-2">
                    <select id="list-id" class="form-control selectpicker" name="list" data-live-search="true" required data-msg-required="Requerido">
                        <option value="">Seleccione una lista</option>
                        @foreach (Config::get('radiotaxilaguna.taxi-lists') as $keyDay => $valueDay)
                            <option value="{{ $keyDay }}">{{ $valueDay }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="telegram-alias" class="control-label">Telegram</label>
                </div>
                <div class="col-sm-1">
                    <input type="text" class="form-control" name="telegram_alias" id="telegram-alias" placeholder="" disabled readonly/>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="auriga-pin-id" class="control-label">Auriga Pin</label>
                </div>
                <div class="col-sm-1">
                    <input type="number" class="form-control" name="auriga_id" id="auriga-pin-id" placeholder="pin"
                           max="9999"
                           minlength="4" data-msg-minlength="Mín. 4 car."
                           maxlength="4" data-msg-maxlength="Máx. 4 car."
                           required />
                </div>

                <div class="col-sm-1 text-right">
                    <label for="alt-auriga-pin-id" class="control-label">Pin Asalariado</label>
                </div>
                <div class="col-sm-1">
                    <input type="number" class="form-control" name="alternative_auriga_id" id="alt-auriga-pin-id" placeholder="Pin"
                           max="9999"
                           minlength="4" data-msg-minlength="Mín. 4 car."
                           maxlength="4" data-msg-maxlength="Máx. 4 car."/>
                </div>

                <div class="col-sm-2 text-right">
                    <label for="dlicence_expiry_date_input_id" class="control-label">F. Caducidad carnet de conducir</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-future"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="dlicence_expiry_date_id" data-link-format="yyyy-mm-dd">
                        <input id="dlicence_expiry_date_input_id" class="form-control text-right" size="16" type="text" value="" required readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="dlicence_expiry_date_id" value="" name="driver_licence_expiry_date"/>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="transport-card-id" class="control-label">Tarjeta de Transporte</label>
                </div>
                <div class="col-sm-2">
                    <input type="number" class="form-control" name="transport_card" id="transport-card-id" placeholder="Tarj. de Transporte"
                           minlength="8" data-msg-minlength="Mín. 8 car."
                           maxlength="8" data-msg-maxlength="Máx. 8 car."
                           data-msg-remote="Este número de tarjeta de transporte ya está en uso!"
                           required data-msg-required="Requerido"/>
                </div>

                <div class="col-sm-2 text-right">
                    <label for="tc_expiration_date_input_id" class="control-label">F. caducidad tarj. de transporte</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-future"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="tc_expiration_date_id" data-link-format="yyyy-mm-dd">
                        <input id="tc_expiration_date_input_id" class="form-control text-right" size="16" type="text" value="" required readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="tc_expiration_date_id" value="" name="tc_expiration_date"/>
                </div>

                <div class="col-sm-2 text-right">
                    <label for="municipal_licence_expiry_date_input" class="control-label">F. Caducidad licencia del taxi</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-future"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="municipal_licence_expiry_date_id" data-link-format="yyyy-mm-dd">
                        <input id="municipal_licence_expiry_date_input" class="form-control text-right" size="16" type="text" value="" required readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="municipal_licence_expiry_date_id" value="" name="municipal_licence_expiry_date"/>
                </div>
            </div>
        </div>

        <div class="row">
            <fieldset>
                <div class="container-fluid">
                    <legend class="rtl-legend"><h3>Datos de la Emisora</h3></legend>
                    <div class="form-group">
                        <div class="col-sm-1 text-right">
                            <label for="station-brand-id" class="control-label">Marca</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="station_brand" id="station-brand-id" placeholder="Marca"
                                   minlength="3" data-msg-minlength="Mín. 3 car."
                                   maxlength="20" data-msg-maxlength="Máx. 20 car."
                                   required data-msg-required="Requerido"/>
                        </div>

                        <div class="col-sm-1 text-right">
                            <label for="station-model-id" class="control-label">Modelo</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="station_model" id="station-model-id" placeholder="Modelo"
                                   minlength="4" data-msg-minlength="Mín. 4 car."
                                   maxlength="20" data-msg-maxlength="Máx. 20 car."/>
                        </div>

                        <div class="col-sm-1 text-right">
                            <label for="station-serial-id" class="control-label">Serie:</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="station_serial" id="station-serial-id" placeholder="Nº Serie"
                                   minlength="4" data-msg-minlength="Mín. 4 car."
                                   data-msg-remote="Este número de serie ya está asignado a otro dispositivo."
                                   maxlength="20" data-msg-maxlength="Máx. 20 car." />
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="row">
            <fieldset>
                <div class="container-fluid">
                    <legend class="rtl-legend"><h3>Datos del GPS.</h3></legend>
                    <div class="form-group">

                        <div class="col-sm-1 text-right">
                            <label for="gps-brand-id" class="control-label">Marca</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="gps_brand" id="gps-brand-id" placeholder="Marca"
                                   value="Auriga"
                                   minlength="4" data-msg-minlength="Mín. 4 car."
                                   maxlength="20" data-msg-maxlength="Máx. 20 car."
                                   required data-msg-required="Requerido"/>
                        </div>

                        <div class="col-sm-1 text-right">
                            <label for="gps-model-id" class="control-label">Modelo</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="gps_model" id="gps-model-id" placeholder="Modelo"
                                   minlength="4" data-msg-minlength="Mín. 4 car."
                                   maxlength="20" data-msg-maxlength="Máx. 20 car."/>
                        </div>

                        <div class="col-sm-1 text-right">
                            <label for="gps-serial-id" class="control-label">Serie:</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control text-right" name="gps_serial" id="gps-serial-id" placeholder="0082-9999"
                                   minlength="9" data-msg-minlength="Mín. 9 car."
                                   maxlength="9" data-msg-maxlength="Máx. 9 car."
                                   data-msg-remote="Ya existe un GPS con este número de serie"
                                   required data-msg-required="Requerido"/>
                        </div>
                        <div class="col-sm-1 text-right">
                            <label for="gps-mac-id" class="control-label">MAC</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control text-right rtl-padding-left-5" name="gps_mac" id="gps-mac-id"
                                   minlength="12" data-msg-minlength="Mín. 12 car."
                                   maxlength="17" data-msg-maxlength="Máx. 17 car."/>
                        </div>
                        <div class="col-sm-1 text-right">
                            <label for="gps-mac-id" class="col-sm-1 control-label col-sm-push-9">ICCID</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control text-right" name="gps_iccid" id="gps-iccid-id"
                                   minlength="19" data-msg-minlength="Mín. 19 car."
                                   maxlength="19" data-msg-maxlength="Máx. 19 car."/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $('#gps-mac-id').on('blur', function () {
                var macAddr = $('#gps-mac-id').val()
                if ((macAddr.length > 0) && (macAddr.indexOf(':') == -1)) {
                    $('#gps-mac-id').val(macAddr.toString(16).match(/.{1,2}/g).join(':'))
                }
            })

            $('#gps-model-id').on('blur', function () {
                $('#gps-model-id').val($('#gps-model-id').val().toUpperCase())
            })

            $.ajax({
                type   : 'GET',
                url    : "{{ route('ajax.associations') }}",
                data   : {},
                success: function (data) {
                    populateSelect('association-id', data);
                    $('#association-id').selectpicker('refresh');
                },
                error  : function (response) {
                    ajaxStandardErrorTreatment(response.status)
                }
            });


        })
    </script>

@endsection