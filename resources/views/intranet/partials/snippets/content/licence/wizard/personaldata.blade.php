<div class="step-pane active form-horizontal" data-step="1">
    <h4>Datos personales</h4>

    <div class="container-fluid">
        <div class="row">
             <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="municipal-licence-id" class=control-label">Licencia Municipal</label>
                </div>
                <div class="col-sm-1">
                    <input type="number" class="form-control" name="municipal_licence" id="municipal-licence-id" placeholder="L. Munic."
                           data-msg-required="Debe introducir un número de licencia municipal."
                           data-msg="Esta Licencia Municipal ya existe en la base de datos."
                           max="365"
                           data-msg-remote="Esta licencia municipal ya está en uso!"
                           minlength="1" data-msg-minlength="Mín. 1 car."
                           maxlength="3" data-msg-maxlength="Máx. 3 car."
                           required />
                </div>

                <div class="col-sm-1 text-right">
                    <label for="email-id" class="control-label text-right">Correo Electrónico</label>
                </div>

                <div class="col-sm-3">
                    <input type="text" class="form-control" name="email" id="email-id" placeholder="Correo Electrónico"
                           data-msg-required="Debe introducir un correo electrónico válido."
                           data-msg="Esta dirección de correo electrónico ya se encuentra registrada."
                           data-msg-remote="Este correo electrónico ya está en uso!"
                           maxlength="50"
                           data-rule-email="email"
                           required/>
                </div>

                 <div class="col-sm-1 text-right">
                     <label for="personal-id" class="control-label ">DNI</label>
                 </div>

                 <div class="col-sm-2">
                     <input type="text" class="form-control" name="pid" id="personal-id" placeholder="Núm. DNI"
                            data-msg="Ese DNI ya existe en la base de datos."
                            minlength="9" data-msg-minlength="Mín. 9 car."
                            maxlength="11" data-msg-maxlength="Máx. 11 car."
                            data-msg-remote="Este DNI ya está en uso!"
                            required data-msg-required="Requerido"/>
                 </div>

                <div class="col-sm-1">
                    <label for="pid_expiration_date-input" class="control-label text-right">Fecha de Caducidad</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-future"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="pid_expiration_date_id" data-link-format="yyyy-mm-dd">
                        <input id="pid_expiration_date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="pid_expiration_date_id" value="" name="pid_expiration_date" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="name-id" class="control-label">Nombre</label>
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="name" id="name-id" placeholder="Nombre"
                           minlength="1" data-msg-minlength="Mín. 2 car."
                           maxlength="40" data-msg-maxlength="Máx. 40 car."
                           required data-msg-required="Requerido"/>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="lastname-id" class="control-label">Apellidos</label>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="lastname" id="lastname-id" placeholder="Apellidos"
                           minlength="3"
                           maxlength="100"
                           required/>
                </div>

                <div class="col-sm-2 text-right">
                    <label for="dob-input" class="control-label">F. Nacimiento</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-past"
                         data-date=""
                         data-date-format="dd/mm/yyyy"
                         data-link-field="dob-id"
                         data-link-format="yyyy-mm-dd">
                        <input id="dob-input" class="form-control" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="dob-id" value="" name="dob" />
                </div>

            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="iban-id" class="control-label">IBAN</label>
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control text-right" name="iban" id="iban-id" placeholder="Codigo Cuenta Cliente"
                           data-msg="Código Cuenta inválido."
                           minlength="24" data-msg-minlength="Mín. 24 car."
                           maxlength="34" data-msg-maxlength="Máx. 34 car."
                           data-msg-remote="Esta cuenta corriente ya está siendo utilizada por otra licencia."
                           required data-msg-required="Requerido"/>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="application-date-input" class="control-label">Fecha Solicitud</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-past"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="doa-id" data-link-format="yyyy-mm-dd">
                        <input id="application-date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="doa-id" value="" name="application_date" />
                </div>

                <div class="col-sm-1 text-right">
                    <label for="council-date-input" class="control-label">F. Consejo Rector</label>
                </div>
                <div class="col-sm-2">

                    <div class="input-group date form-date-past"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="doc-id" data-link-format="yyyy-mm-dd">
                        <input id="council-date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="doc-id" value="" name="council_date" />
                </div>

                <div class="col-sm-1 text-right">
                    <label for="communication-date-input" class="control-label">Fecha Comunicación</label>
                </div>
                <div class="col-sm-2">
                    <div class="input-group date form-date-past"
                         data-date="" data-date-format="dd/mm/yyyy" data-link-field="doco-id" data-link-format="yyyy-mm-dd">
                        <input id="communication-date-input" class="form-control text-right" size="16" type="text" value="" readonly>
                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                    </div>
                    <input type="hidden" id="doco-id" value="" name="communication_date" />
                </div>

            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="address-1-id" class="control-label">Dirección</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="address" id="address-1-id" placeholder="dirección - linea 1"
                           minlength="3"
                           maxlength="200"
                           required/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="address-2-id" class="control-label"></label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="address_1" id="address-2-id" placeholder="dirección - linea 2"
                           minlength="3"
                           maxlength="200"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-1 text-right">
                    <label for="postal-code-id" class="control-label">C.P.<i id="addAction" class="rtl-margin-left-10 fa fa-pencil-square-o hidden" data-toggle="modal" data-target="#postalCodeModal"></i></label>
                </div>
                <div class="col-sm-1">
                    <input type="number" class="form-control" name="postalCode" id="postal-code-id" placeholder="Cód. Pos."
                           minlength="5" data-msg-minlength="Mín. 5 car."
                           maxlength="5" data-msg-maxlength="Máx. 5 car."
                           required data-msg-required="Requerido"/>
                    <input type="hidden" name="postal_code_id" id="postal-code-val" value=""/>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="locality-id" class="control-label">Localidad</label>
                </div>

                <div class="col-sm-2">
                    <input type="text" class="form-control" name="locality" id="locality-id" disabled readonly />
                </div>

                <div class="col-sm-1 text-right">
                    <label for="mobile-id" class="control-label">Teléfono(s)</label>
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="phone" id="phone-id" placeholder="Teléfono(s)"
                           minlength="9" data-msg-minlength="Mín. 9 car."
                           maxlength="30" data-msg-maxlength="Máx. 30 car."/>
                </div>

                <div class="col-sm-1 text-right">
                    <label for="mobile-id" class="control-label">Móvil(es)</label>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="mobile" id="mobile-id" placeholder="Móvil(es)"
                           minlength="9" data-msg-minlength="Mín. 9 car."
                           maxlength="30" data-msg-maxlength="Máx. 30 car."
                           required data-msg-required="Requerido"/>
                </div>
            </div>
        </div>
    </div>

</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">

        $('#personal-id').on('blur', function (evt) {
            const matches = {1: '01', 2: '02', 3: '03', 4: '04', 5: '05', 6: '06', 7: '07', 8: '09', 9: '10', 0: '11'};
            var edtPersonalId = $(this).val();
            if (edtPersonalId.length > 7) {
                var personalId = edtPersonalId.replace(/\D/g, '');
                var month = matches[personalId.charAt(personalId.length - 1)];
                var year = (new Date()).getFullYear() + 4;
                $('#transport-card-expiration-date').val('01/' + month + '/' + year);
            } else {
                $(this).focus();
            }
        });

        $('#postalCodeModal').on('shown.bs.modal', function () {
            var modal = $(this);

            $.LoadingOverlay("show");
            createNewPromise('{{ route('ajax.localities') }}').then(
                function (response) {
                    populateSelect('modal-locality-id', response, 0);
                    $('#modal-locality-id').selectpicker('refresh');
                    $.LoadingOverlay("hide");
                },
                function (Error) {
                    showNotification('Oops! Ha ocurrido un error.', 'error', Error);
                    $.LoadingOverlay("hide");
                }
            );

            $('#postal_code_id').val($('#postal-code-id').val());
            $('#place_id').focus();
        })
    </script>

@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $('.olddatetimepicker').val('');
            $('.noupdatedatetimepicker').val('');
        });
    </script>
@endsection