<div class="step-pane form-horizontal" data-step="3">
    <h3>Datos del vehículo</h3>

    <div class="container-fluid">

        <div class="form-group">
            <div class="col-sm-1 text-right">
                <label for="car-brand-id" class="control-label">Marca</label>
            </div>
            <div class="col-sm-1">
                <select id="car-brand-id" class="form-control selectpicker rtl-padding-left-5" name="car_brand_id"
                        title="Seleccione una marca para el vehículo"
                        data-selected-text-format="count"
                        data-live-search="true"
                        required data-msg-required="Requerido" disabled></select>
            </div>

            <div class="col-sm-1 text-right">
                <label for="car-model-id" class="control-label">Modelo</label>
            </div>
            <div class="col-sm-2">
                <select id="car-model-id" class="form-control selectpicker" name="car_model_id"
                        title="Seleccione el modelo del vehículo"
                        data-selected-text-format="count"
                        data-live-search="true"
                        required data-msg-required="Requerido" disabled></select>
            </div>

            <div class="col-sm-1 text-right">
                <label for="car-plate-id" class="control-label">Matrícula</label>
            </div>
            <div class="col-sm-1">
                <input type="text" class="form-control" id="car-plate-id" name="plate" placeholder=""
                       minlength="7" data-msg-minlength="Mín. 4 car."
                       maxlength="15" data-msg-maxlength="Máx. 25 car."
                       data-msg-remote="Ya existe esta matrícula en la base de datos."
                       required data-msg-required="Requerido"/>
            </div>

            <div class="col-sm-1 text-right">
                <label for="car-places-id" class="control-label">Pl. Totales.</label>
            </div>
            <div class="col-sm-1">
                <div class="spinbox text-right" data-initialize="spinbox" id="car-places"
                     data-min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                     data-max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}">
                    <input type="number" class="form-control input-mini spinbox-input" name="places" id="car-places"
                           value="0"
                           min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                           max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}"
                           data-min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                           data-max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}"/>
                    <div class="spinbox-buttons btn-group btn-group-vertical">
                        <button type="button" class="btn btn-default spinbox-up btn-xs rtl-spin-btn-xs">
                            <span class="glyphicon glyphicon-chevron-up"></span><span class="sr-only">Incrementar</span>
                        </button>
                        <button type="button" class="btn btn-default spinbox-down btn-xs rtl-spin-btn-xs">
                            <span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only">Decrementar</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-sm-1 text-right">
                <label for="car-disabled-places-id" class="control-label">Pl. Minusv.</label>
            </div>
            <div class="col-sm-1">
                <div class="spinbox text-right" data-initialize="spinbox" id="car-disabled-places"
                     data-min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                     data-max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}">
                    <input type="number" class="form-control input-mini spinbox-input" name="disabled_places" id="car-disabled-places"
                           value="0"
                           min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                           max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}"
                           data-min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                           data-max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}"/>
                    <div class="spinbox-buttons btn-group btn-group-vertical">
                        <button type="button" class="btn btn-default spinbox-up btn-xs rtl-spin-btn-xs">
                            <span class="glyphicon glyphicon-chevron-up"></span><span class="sr-only">Incrementar</span>
                        </button>
                        <button type="button" class="btn btn-default spinbox-down btn-xs rtl-spin-btn-xs">
                            <span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only">Decrementar</span>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">

        $('#car-plate-id').on('blur', function(){
            var plate = $(this).val();
            if (/-|\s/g.test(plate)) {
                $(this).val(plate.replace(/-|\s/g,""));
            }
        });

        $('#car-brand-id').on('changed.bs.select', function (evt) {
            $('#car-model-id').find('option').remove().end()
            $.ajax({
                type   : 'GET',
                url    : "{{ route('ajax.carmodels') }}",
                data   : {
                    'brand_id': $('#car-brand-id').find(':selected').val()
                },
                success: function (data) {
                    populateSelect('car-model-id', data)
                    $('#car-model-id').selectpicker('refresh')
                    $('#car-model-id').focus()
                },
                error  : function (response) {
                    ajaxStandardErrorTreatment(response.status, function () {
                        showNotification('Error', 'error', 'Ha ocurrido un error')
                    })
                }
            })
        })

        $(document).ready(function () {

            setupTouchSpin('carPlaces')
            setupTouchSpin('carDisabledPlaces')

            $.ajax({
                type   : 'GET',
                url    : "{{ route('ajax.carbrands') }}",
                data   : {},
                success: function (data) {
                    populateSelect('car-brand-id', data)
                },
                error  : function (response) {
                    ajaxStandardErrorTreatment(response.status, function () {
                        ajaxStandardErrorTreatment(response.status)
                    })
                }
            })
        })
    </script>

@endsection