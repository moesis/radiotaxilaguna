<div role="economicData" class="tab-pane" id="economicData">
    <div class="container-fluid">
        <div class="form-horizontal">
            {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'economicDataForm', 'method' => 'POST']) !!}
                <input type="hidden" id="licence-id" value="{{ $data['id'] }}" name="licence_id">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="col-sm-2 text-right">
                                <label class="control-label">Cuenta IBAN</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control text-right rtl-editable" name="iban" id="iban-id"
                                       value="{{ $data['member']['iban'] }}"
                                       data-msg="Código Cuenta inválido."
                                       minlength="24" data-msg-minlength="Mín. 24 car."
                                       maxlength="34" data-msg-maxlength="Máx. 34 car."
                                       required data-msg-required="Requerido"
                                       disabled readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr class="rtl-divider"/>
                    <div class="col-sm-12 text-right" id="economic-button-bar">
                        <button class="btn btn-success" id="economic-btnEdit">Editar</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var economicDataForm = $('#economicDataForm').validate({
            onkeyup   : function (element, event) {
                if ($(element).attr('name') == 'name') {
                    return false // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if (event.which === 9 && this.elementValue(element) === '') {
                        return;
                    } else if (element.name in this.submitted || element === this.lastElement) {
                        this.element(element)
                    }
                }
            },
            errorClass: 'text-danger',
            validClass: 'text-success',
            rules     : {
                iban             : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.iban') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('iban-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa cuenta IBAN se encuentra utilizada.'));
                                    } else {
                                        state = true;
                                    }
                                    break
                                case 'Not Found':
                                    state = true
                                    break
                            }

                            return state
                        },
                        error      : function (response) {
                            message = JSON.parse(response.responseText)
                            var msg = ''
                            if (message['status'] !== undefined && message['status'].length > 0) {
                                msg = message['status']
                            } else {
                                msg = 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador'
                            }
                            showNotification('Error', 'error', msg)
                        }
                    }
                }
            }
        })
    </script>
@endsection

@section('document-ready')
    @parent
    <script type='text/javascript'>
        $(document).ready(function () {
            $('#economic-btnEdit').on('click', function (event) {
                event.preventDefault()
                updateControlsState('economic', false)         // Enable the controls.
                createExtraButtons('economic')
            })
        })
    </script>
@endsection