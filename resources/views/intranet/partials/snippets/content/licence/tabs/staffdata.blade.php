<div role="staffData" class="tab-pane" id="staffData">
    <div class="container-fluid">
        <div class="form-horizontal">
            <div class="row">
                <div class="col-sm-8">
                    <table class="table table-striped" style="width:75% !important; margin: 0px auto;">
                        <thead>
                        <th class="text-center">DNI</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Móvil</th>
                        <th>C. Electrónico</th>
                        <th class="text-center">F. Contratación</th>
                        <th class="text-center">F. Despido</th>
                        <th class="text-center col-sm-1">&nbsp;</th>
                        </thead>
                        <tbody>
                        @forelse($data as $staff)
                            <tr>
                                <td>{{ $staff['pid'] }}</td>
                                <td>{{ $staff['name'] }}</td>
                                <td>{{ $staff['lastname'] }}</td>
                                <td><a href="tel:{{ $staff['mobile'] }}" title="click para llamar al teléfono" data-toggle="tooltip" data-placement="top">{{ $staff['mobile'] }}</a></td>
                                <td><a href="mailto:{{ $staff['email'] }}" title="click para enviar un correo electrónico" data-toggle="tooltip" data-placement="top">{{ $staff['email'] }}</a></td>
                                <td class="text-right">{{ empty($staff['pivot']['start_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $staff['pivot']['start_date'])->format('d-m-Y')  }}</td>
                                <td class="text-right">{{ empty($staff['pivot']['end_date']) ? '- en activo -' : Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $staff['pivot']['end_date'])->format('d-m-Y')  }}</td>
                                <td class="text-center">
                                    <span><a href="{{ route('admin.staff.show', ['id' => $staff['id']]) }}" title="Ver detalles del personal" data-toggle="tooltip" data-placement="right"><i class="fa fa-eye"></i></a></span>
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="7" class="text-center"> No existen contrataciones </td></tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>