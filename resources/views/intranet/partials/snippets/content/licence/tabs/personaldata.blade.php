<div role="personalData" class="tab-pane active" id="personalData">
    <div class="form-horizontal">
        {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'personalDataForm', 'method' => 'POST']) !!}
        <input type="hidden" id="licence-id" value="{{ $data['id'] }}" name="licence_id">
        <div class="container-fluid">
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-1 text-right">
                        <label class="control-label">Lic. Municipal</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="form-control text-right rtl-editable"
                               type="text"
                               name="municipal_licence" id="municipal-licence-id"
                               value="{{ $data['municipal_licence'] }}"
                               data-originalValue="{{ $data['municipal_licence'] }}"
                               required readonly disabled/>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label class="control-label">DNI</label>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control text-right rtl-editable"
                               type="text"
                               name="pid" id="personal-id"
                               value="{{ $data['member']['pid'] }}"
                               data-originalValue="{{ $data['member']['pid'] }}"
                               readonly disabled required/>
                    </div>

                    <div class="col-sm-1">
                        <label for="pid_expiration_date-input" class="control-label text-right">Fecha de Caducidad</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group date form-date-past"
                             data-date="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['pid_expiration_date'])->format('d-m-Y') }}"
                             data-date-format="dd-mm-yyyy" data-link-field="pid_expiration_date_id"
                             data-link-format="yyyy-mm-dd">
                            <input id="pid_expiration_date-input" class="form-control text-right" size="16" type="text" value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['pid_expiration_date'])->format('d-m-Y') }}" readonly>
                            <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                            <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                        </div>
                        <input type="hidden" id="pid_expiration_date_id" name="pid_expiration_date"
                               value="{{ empty($data['member']['pid_expiration_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['pid_expiration_date'])->format('Y-m-d') }}" />
                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="dob-input" class="control-label">Fecha de Nacimiento</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group date form-date-past"
                             data-date="{{ Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['dob'])->format('d-m-Y') }}"
                             data-date-format="dd-mm-yyyy"
                             data-link-field="dob-id"
                             data-link-format="yyyy-mm-dd">
                            <input id="dob-input" class="form-control text-right" size="16" type="text" name=""
                                   value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['dob'])->format('d-m-Y') }}" readonly/>
                            <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                            <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                        </div>

                        <input type="hidden" id="dob-id" name="dob"
                               value="{{ empty($data['member']['dob']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['dob'])->format('Y-m-d') }}" />
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-1 text-right">
                        <label class="control-label">Nombre</label>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control rtl-editable"
                               type="text"
                               name="name" id="name-id"
                               value="{{ $data['member']['name'] }}"
                               data-originalValue="{{ $data['member']['name'] }}"
                               required readonly disabled/>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label class="col-sm-5 col-md-6 col-lg-1 control-label">Apellidos</label>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control rtl-editable"
                               type="text"
                               name="lastname" id="lastname-id"
                               value="{{ $data['member']['lastname'] }}"
                               data-originalValue="{{ $data['member']['lastname'] }}"
                               required readonly disabled/>

                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="dob-input" class="control-label">Usuario Telegram</label>
                    </div>
                    <div class="col-sm-1">
                        <div class="input-group">
                            <input class="form-control text-right" type="text"
                                   name="telegram_alias" id="telegram-alias"
                                   value="{{ !empty($data['member']['telegram_alias']) ? $data['member']['telegram_alias'] : '' }}"
                                   data-originalValue="{{ !empty($data['member']['telegram_alias']) ? $data['member']['telegram_alias'] : '' }}"
                                   readonly disabled/>
                            <span class="input-group-addon">
                                @if (!empty($data['member']['telegram_alias']))
                                    <a href="https://t.me/{{ Config::get('radiotaxilaguna.prefixes.telegram') . str_replace('LM-', '', $data['member']['telegram_alias']) }}" target="_blank"><span class="fa fa-paper-plane-o input-link" aria-hidden="true"></span></a>
                                @else
                                    <span class="fa fa-paper-plane-o input-link" aria-hidden="true"></span>
                                @endif
                            </span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-1 text-right">
                        <label class="control-label">Dirección</label>
                    </div>
                    <div class="col-sm-4">
                        <input class="form-control rtl-editable"
                               type="text"
                               name="address" id="address"
                               value="{{ $data['member']['address'] }}"
                               required readonly disabled/>
                        @if(!empty($data['member']['address_1']))
                            <br />
                            <input class="form-control rtl-editable"
                                   type="text"
                                   name="address_1" id="address-1"
                                   value="{{ $data['member']['address_1'] }}"
                                   readonly disabled/>
                        @endif
                    </div>

                    <div class="col-sm-1 text-right">
                        <label class="control-label">Código Postal</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="form-control text-right rtl-editable" type="text"
                               name="postalCode" id="postal-code-id"
                               value="{{ $data['member']['postal_code']['postal_code'] }}"
                               data-originalValue="{{ $data['member']['postal_code']['postal_code'] }}"
                               required readonly disabled/>
                        <input type="hidden" name="postal_code_id" id="postal-code-val" value="{{ $data['member']['postal_code']['id'] }}"/>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label class="control-label">Localidad</label>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control" type="text" value="{{ $data['member']['postal_code']['locality']['name'] }}" readonly disabled/>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-1 text-right">
                        <label for="dob-input" class="control-label">Teléfonos</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input name="phone" id="phone-id" class="form-control text-right rtl-editable"
                                   type="text"
                                   value="{{ !empty($data['member']['phone']) ? $data['member']['phone'] : '' }}"
                                   data-originalValue="{{ !empty($data['member']['phone']) ? $data['member']['phone'] : '' }}"
                                   readonly disabled/>
                            <span class="input-group-addon">
                            @if (!empty($data['member']['phone']))
                                    <a class="rtl-callable" href="tel:{{ $data['member']['phone']}}"><span class="fa fa-phone input-link" aria-hidden="true"></span></a>
                                @else
                                    <span class="fa fa-phone input-link" aria-hidden="true"></span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="dob-input" class="control-label">Móviles</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input name="mobile" id="mobile-id" class="form-control text-right rtl-editable"
                                   type="text"
                                   value="{{ !empty($data['member']['mobile']) ? $data['member']['mobile'] : '' }}"
                                   data-originalValue="{{ !empty($data['member']['mobile']) ? $data['member']['mobile'] : '' }}"
                                   readonly disabled/>
                            <span class="input-group-addon">
                            @if (!empty($data['member']['phone']))
                                    <a href="tel:{{ $data['member']['mobile'] }}"><span class="fa fa-mobile input-link" aria-hidden="true"></span></a>
                                @else
                                    <span class="fa fa-phone input-link" aria-hidden="true"></span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="dob-input" class="control-label">Correo Electrónico</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input name="email" id="email-id" class="form-control text-right rtl-editable"
                                   type="text"
                                   value="{{ !empty( $data['member']['email']) ?  $data['member']['email'] : '' }}"
                                   data-originalValue="{{ !empty( $data['member']['email']) ?  $data['member']['email'] : '' }}"
                                   readonly disabled required/>
                            <span class="input-group-addon">
                            @if (!empty($data['member']['phone']))
                                    <a href="mailto:{{  $data['member']['email'] }}"><span class="fa fa-mobile input-link" aria-hidden="true"></span></a>
                                @else
                                    <span class="fa fa-envelope-o input-link" aria-hidden="true"></span>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-2 text-right">
                        <label for="tc_expiration_date_input_id" class="control-label">Fecha caducidad Licencia Municipal</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group date form-date-past"
                             data-date="{{ empty($data['member']['municipal_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['municipal_licence_expiry_date'])->format('d-m-Y') }}"
                             data-date-format="dd-mm-yyyy"
                             data-link-field="tc_expiration_date_id"
                             data-link-format="yyyy-mm-dd">
                            <input id="tc_expiration_date_input_id" class="form-control text-right" size="16" type="text"
                                   value="{{ empty($data['member']['municipal_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['municipal_licence_expiry_date'])->format('d-m-Y') }}"
                                   required readonly/>
                            <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                            <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                        </div>
                        <input type="hidden" id="tc_expiration_date_id" name="tc_expiration_date"
                               value="{{ empty($data['member']['municipal_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['municipal_licence_expiry_date'])->format('Y-m-d') }}"/>
                    </div>

                    <div class="col-sm-2 text-right">
                        <label for="dlicence_expiry_date_input_id" class="control-label">F. Caducidad carnet de conducir</label>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group date form-date-past"
                             data-date="{{ empty($data['member']['driver_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['driver_licence_expiry_date'])->format('d-m-Y') }}"
                             data-date-format="dd-mm-yyyy"
                             data-link-field="dlicence_expiry_date_id"
                             data-link-format="yyyy-mm-dd">
                            <input id="dlicence_expiry_date_input_id" class="form-control text-right" size="16" type="text" required readonly
                                   value="{{ empty($data['member']['driver_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['driver_licence_expiry_date'])->format('d-m-Y') }}">
                            <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                            <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                        </div>
                        <input type="hidden" id="dlicence_expiry_date_id" name="driver_licence_expiry_date"
                               value="{{ empty($data['member']['driver_licence_expiry_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['driver_licence_expiry_date'])->format('Y-m-d') }}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">

                    <div class="col-sm-1 text-right">
                        <label for="association" class="control-label">Asociación</label>
                    </div>
                    <div class="col-sm-2">
                        <select id="association-id" class="form-control selectpicker rtl-editable" name="association_id"
                                title="Seleccione una asociación de la lista"
                                data-selected-text-format="count"
                                data-live-search="true"
                                required data-msg-required="Requerido"></select>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="rest-day-id" class="control-label">Día Libre</label>
                    </div>
                    <div class="col-sm-1">
                        <select id="rest-day" class="form-control selectpicker rtl-editable" name="rest_day" data-live-search="true" required data-msg-required="Requerido" readonly disabled>
                            <option value="">Seleccione una opción</option>
                            @foreach(Config::get('radiotaxilaguna.restDays') as $keyDay => $valueDay)
                                <option value="{{ $keyDay }}"
                                        @if ($keyDay === $data['member']['rest_day'])
                                        selected="selected"
                                        @endif
                                >{{ $valueDay }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-1 text-right">
                        <label for="list-id" class="control-label">Lista</label>
                    </div>
                    <div class="col-sm-1">
                        <select id="list-id" class="form-control selectpicker rtl-editable"
                                name="list" data-live-search="true" required
                                data-msg-required="Requerido"
                                readonly disabled>
                            <option value="">Seleccione una lista</option>
                            @foreach (Config::get('radiotaxilaguna.taxi-lists') as $keyList => $valueList)
                                <option value="{{ $keyList }}"
                                        @if ($keyList === $data['member']['list'])
                                        selected="selected"
                                        @endif
                                >{{ $valueList }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
            <div class="row">
                <hr class="rtl-divider"/>
                <div class="col-sm-12 text-right" id="personal-button-bar">
                    <button class="btn btn-success" id="personal-btnEdit" type="button">Editar</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var memberFormValidator = $('#personalDataForm').validate({
            onkeyup   : function (element, event) {
                if ($(element).attr('name') == 'name') {
                    return false // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if (event.which === 9 && this.elementValue(element) === '') {
                        return
                    } else if (element.name in this.submitted || element === this.lastElement) {
                        this.element(element)
                    }
                }
            },
            errorClass: 'text-danger',
            validClass: 'text-success',
            rules     : {
                municipal_licence: {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.lm') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data  = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('municipal-licence-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa licencia municipal ya existe'))
                                    } else {
                                        state = true
                                    }
                                    break
                                case 'Not Found':
                                    $('#municipal-licence-id').val(parseInt($('#municipal-licence-id').val()).pad(3))
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                email            : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.email') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data  = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('email-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese correo ya está registrado.'))
                                    } else {
                                        state = true
                                    }
                                    break
                                case'Not Found':
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                pid              : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.personalId') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        dataFilter : function (response) {
                            var data  = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('personal-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese DNI ya existe.'))
                                    } else {
                                        state = true;
                                    }
                                    break
                                case 'Not Found':
                                    if (data.letter.length > 0) {
                                        showNotification('Letra DNI incorrecta', 'notice', 'La letra correcta es la <b>' + data.letter + '</b>')
                                    }
                                    $('#personal-id').val(($('#personal-id').val()).toUpperCase())
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                postalCode       : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.postalcode') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            $('#locality-id').val(data['name'])
                            $('#postal-code-val').val(data['id'])

                            return true
                        },
                        error      : function (response) {
                            ajaxStandardErrorTreatment(response.status, null, function () {
                                $('#locality-id').val('El código postal no existe...')
                                $('#addAction').removeClass('hidden')
                                $('#postal-code').focus()
                            })
                        }
                    }
                },
                errorElement     : 'em',
                errorPlacement   : function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass('help-block')
                    if (element.prop('type') === 'checkbox') {
                        error.insertAfter(element.parent('label'))
                    } else {
                        error.insertAfter(element)
                    }
                },
                highlight        : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-error').removeClass('has-success')
                },
                unhighlight      : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-success').removeClass('has-error')
                }
            }
        })
    </script>
@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

            $('.list-group-sortable-connected').sortable({
                placeholderClass: 'list-group-item',
                connectWith     : '.connected'
            })

            $.ajax({
                type   : 'GET',
                url    : "{{ route('ajax.associations') }}",
                data   : {},
                success: function (data) {
                    populateSelect('association-id', data)
                    $('#association-id').selectpicker('val', '{{ $data['association']['id'] }}')
                    $('#association-id').prop('disabled', true)
                    $('#association-id').selectpicker('refresh')
                },
                error  : function (response) {
                    ajaxStandardErrorTreatment(response.status)
                }
            })

            $('#municipal-licence-id').on('input propertychange paste', function () {
                $('#telegram-alias').val('LM-' + parseInt($('#municipal-licence-id').val()).pad(3))
            })

            $('#personal-btnEdit').on('click', function (event) {
                event.preventDefault();
                updateControlsState('personal', false);         // Enable the controls.
                createExtraButtons('personal');
            })
        })
    </script>
@endsection