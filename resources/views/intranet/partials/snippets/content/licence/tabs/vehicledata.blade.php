<div role="vehicleData" class="tab-pane" id="vehicleData">
    <div class="container-fluid">
        <div class="form-horizontal">
            {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'vehicleDataForm', 'method' => 'POST']) !!}
                <input type="hidden" id="licence-id" value="{{ $data['id'] }}" name="licence_id">
                <input type="hidden" id="vehicle-id" value="{{ $data['member']['vehicle']['id'] }}" name="vehicle_id">
                <div class="row">
                    <fieldset>
                        <legend class="rtl-legend"><h4>Datos del Vehículo</h4></legend>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1 text-right">
                                    <label class="control-label">Matrícula</label>
                                </div>
                                <div class="col-sm-1">
                                    <input id="car-plate-id" name="plate" class="form-control text-center rtl-editable"
                                           type="text" value="{{ $data['member']['vehicle']['plate']}}" readonly disabled />
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="car-brand-id" class="control-label">Marca</label>
                                </div>
                                <div class="col-sm-1">
                                    <select id="car-brand-id" class="form-control selectpicker rtl-padding-left-5" name="car_brand_id"
                                            title="Seleccione una marca para el vehículo"
                                            data-selected-text-format="count"
                                            data-live-search="true"
                                            data-msg-required="Requerido"
                                            readonly disabled>
                                            <option value="">Seleccione una marca</option>
                                            @foreach ($brands as $keyList => $valueList)
                                                <option value="{{ $keyList }}"
                                                    @if ($keyList === $data['member']['vehicle']['brand']['id'])
                                                        selected="selected"
                                                    @endif
                                                >{{ $valueList }}</option>
                                            @endforeach

                                    </select>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="car-model-id" class="control-label">Modelo</label>
                                </div>
                                <div class="col-sm-2">
                                    <select id="car-model-id" class="form-control selectpicker rtl-editable" name="car_model_id"
                                            title="Seleccione el modelo del vehículo"
                                            data-selected-text-format="count"
                                            data-live-search="true"
                                            data-msg-required="Requerido"
                                            readonly disabled>
                                            <option value="">Seleccione una modelo</option>
                                            @foreach ($models as $keyList => $valueList)
                                                <option value="{{ $keyList }}"
                                                        @if ($keyList === $data['member']['vehicle']['model']['id'])
                                                        selected="selected"
                                                        @endif
                                                >{{ $valueList }}</option>
                                            @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="car-places-id" class="control-label">Nº de plazas</label>
                                </div>
                                <div class="col-sm-1">
                                    <div class="spinbox text-right" data-initialize="spinbox" id="car-places"
                                         data-min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                                         data-max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}">
                                        <input type="number" class="form-control input-mini spinbox-input rtl-editable" name="places" id="car-places"
                                               value="{{  $data['member']['vehicle']['places'] }}"
                                               min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                                               max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}"
                                               data-min="{{ Config::get('radiotaxilaguna.company.vehicles.places.min') }}"
                                               data-max="{{ Config::get('radiotaxilaguna.company.vehicles.places.max') }}" disabled readonly/>
                                        <div class="spinbox-buttons btn-group btn-group-vertical hidden">
                                            <button type="button" class="btn btn-default spinbox-up btn-xs rtl-spin-btn-xs">
                                                <span class="glyphicon glyphicon-chevron-up"></span><span class="sr-only">Incrementar</span>
                                            </button>
                                            <button type="button" class="btn btn-default spinbox-down btn-xs rtl-spin-btn-xs">
                                                <span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only">Decrementar</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="car-disabled-places-id" class="control-label">Pl. Minusválidos</label>
                                </div>
                                <div class="col-sm-1">
                                    <div class="spinbox text-right" data-initialize="spinbox" id="car-disabled-places"
                                         data-min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                                         data-max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}">
                                        <input type="number" class="form-control input-mini spinbox-input rtl-editable" name="disabled_places" id="car-disabled-places"
                                               value="{{ $data['member']['vehicle']['disabled_places'] }}"
                                               min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                                               max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}"
                                               data-min="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.min') }}"
                                               data-max="{{ Config::get('radiotaxilaguna.company.vehicles.disabled-places.max') }}"
                                               disabled readonly
                                        />
                                        <div class="spinbox-buttons btn-group btn-group-vertical hidden">
                                            <button type="button" class="btn btn-default spinbox-up btn-xs rtl-spin-btn-xs disabled">
                                                <span class="glyphicon glyphicon-chevron-up"></span><span class="sr-only">Incrementar</span>
                                            </button>
                                            <button type="button" class="btn btn-default spinbox-down btn-xs rtl-spin-btn-xs disabled">
                                                <span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only">Decrementar</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1 text-right">
                                    <label for="enrollment_date-input" class="control-label text-right">Fecha de Alta</label>
                                </div>
                                <div class="col-sm-2">
                                    <div class="input-group date form-date-past"
                                         data-date="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['enrollment_date'])->format('d-m-Y') }}"
                                         data-date-format="dd/mm/yyyy"
                                         data-link-field="enrollment_date-id"
                                         data-link-format="yyyy-mm-dd">
                                        <input id="enrollment_date-input" class="form-control text-right" size="16" type="text"
                                               value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['enrollment_date'])->format('d-m-Y') }}" readonly>
                                        <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                        <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                    </div>
                                    <input type="hidden" id="enrollment_date-id" name="enrollment_date"
                                           value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['enrollment_date'])->format('Y-m-d') }}" />
                                </div>

                                @if (!is_null($data['member']['vehicle']['cancellation_date']))
                                    <div class="col-sm-1 text-right">
                                        <label for="cancellation_date-input" class="control-label text-right">Fecha de Baja</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group date form-date-future"
                                             data-date="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['cancellation_date'])->format('d-m-Y') }}"
                                             data-date-format="dd/mm/yyyy"
                                             data-link-field="cancellation_date-id"
                                             data-link-format="yyyy-mm-dd">
                                            <input id="cancellation_date-input" class="form-control text-right" size="16" type="text"
                                                   value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['cancellation_date'])->format('d-m-Y') }}" readonly>
                                            <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                            <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                        </div>
                                        <input type="hidden" id="cancellation_date-id" name="cancellation_date"
                                               value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['vehicle']['cancellation_date'])->format('Y-m-d') }}" />
                                    </div>
                                @else
                                    <div class="col-sm-1 text-right">
                                        <button class="btn rtl-btn-danger" id="unsuscribe_car_btn">Dar de baja el vehículo</button>
                                    </div>
                                @endif

                                <div class="col-sm-2 text-right">
                                    <label class="control-label">Pin Auriga</label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="number" class="form-control rtl-editable" name="auriga_id" id="auriga-pin-id" placeholder="pin"
                                           max="9999"
                                           minlength="4" data-msg-minlength="Mín. 4 car."
                                           maxlength="4" data-msg-maxlength="Máx. 4 car."
                                           value="{{ $data['auriga_id'] }}"
                                           data-originalValue="{{ $data['auriga_id'] }}"
                                           readonly disabled required data-msg-required="Requerido"/>
                                </div>

                                <div class="col-sm-2 text-right">
                                    <label for="alt-auriga-pin-id" class="control-label">Pin Asalariado</label>
                                </div>
                                <div class="col-sm-1">
                                    <input  type="number" class="form-control rtl-editable" name="alternative_auriga_id" id="alt-auriga-pin-id"
                                            max="9999"
                                            minlength="4" data-msg-minlength="Mín. 4 car."
                                            maxlength="4" data-msg-maxlength="Máx. 4 car."
                                            value="{{ $data['alternative_auriga_id'] }}"
                                            disabled readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-2 text-right">
                                    <button class="btn rtl-btn-primary" id="car-notes-query-btn" type="button"
                                            data-backdrop="static"
                                            data-keyboard="false"
                                            data-toggle="modal"
                                            data-target="#view-vehicle-notes"
                                            {{ $data['member']['vehicle']['notes'] ? 'disabled' : '' }}>
                                        Consultar notas del coche
                                    </button>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <button class="btn rtl-btn-default" id="car-notes-add-btn" type="button"
                                            data-backdrop="static"
                                            data-keyboard="false"
                                            data-toggle="modal"
                                            data-target="#vehicle-notes">
                                        Agregar notas al vehículo
                                    </button>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="row">
                    <fieldset>
                        <legend class="rtl-legend"><h4>Datos de la Emisora</h4></legend>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1 text-right">
                                    <label for="station-brand-id" class="control-label">Marca</label>
                                </div>
                                <div class="col-sm-2">
                                    <input  type="text" class="form-control rtl-editable" name="station_brand" id="station-brand-id" placeholder="Marca"
                                            minlength="3" data-msg-minlength="Mín. 3 car."
                                            maxlength="20" data-msg-maxlength="Máx. 20 car."
                                            value="{{ $data['station_brand'] }}"
                                            data-originalValue="{{ $data['station_brand'] }}"
                                            disabled readonly required data-msg-required="Requerido"/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="station-model-id" class="control-label">Modelo</label>
                                </div>
                                <div class="col-sm-2">
                                    <input  type="text" class="form-control rtl-editable" name="station_model" id="station-model-id" placeholder="Modelo"
                                            minlength="4" data-msg-minlength="Mín. 4 car."
                                            maxlength="20" data-msg-maxlength="Máx. 20 car."
                                            value="{{ $data['station_model'] }}"
                                            disabled readonly/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="station-serial-id" class="control-label">Serie:</label>
                                </div>
                                <div class="col-sm-1">
                                    <input  type="text" class="form-control rtl-editable" name="station_serial" id="station-serial-id" placeholder="Nº Serie"
                                            minlength="4" data-msg-minlength="Mín. 4 car."
                                            maxlength="20" data-msg-maxlength="Máx. 20 car."
                                            value="{{ $data['station_serial'] }}"
                                            disabled readonly/>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="row">
                    <fieldset>
                        <legend class="rtl-legend"><h4>Datos del GPS</h4></legend>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1 text-right">
                                    <label for="gps-brand-id" class="control-label">Marca</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control rtl-editable" name="gps_brand" id="gps-brand-id" placeholder="Marca"
                                           minlength="4" data-msg-minlength="Mín. 4 car."
                                           maxlength="20" data-msg-maxlength="Máx. 20 car."
                                           data-originalValue="{{ $data['gps_brand'] }}"
                                           value="{{ $data['gps_brand'] }}"
                                           readonly disabled required data-msg-required="Requerido"/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="gps-model-id" class="control-label">Modelo</label>
                                </div>
                                <div class="col-sm-2">
                                    <input  type="text" class="form-control rtl-editable" name="gps_model" id="gps-model-id" placeholder="Modelo"
                                            minlength="4" data-msg-minlength="Mín. 4 car."
                                            maxlength="20" data-msg-maxlength="Máx. 20 car."
                                            value="{{ $data['gps_model'] }}"
                                            disabled readonly/>
                                </div>

                                <div class="col-sm-1 text-right">
                                    <label for="gps-serial-id" class="control-label">Serie:</label>
                                </div>
                                <div class="col-sm-1">
                                    <input  type="text" class="form-control text-right rtl-editable" name="gps_serial" id="gps-serial-id" placeholder="0082-9999"
                                            minlength="9" data-msg-minlength="Mín. 9 car."
                                            maxlength="9" data-msg-maxlength="Máx. 9 car."
                                            data-originalValue="{{ $data['gps_serial'] }}"
                                            value="{{ $data['gps_serial'] }}"
                                            disabled readonly required />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1 text-right">
                                    <label for="gps-mac-id" class="control-label">MAC</label>
                                </div>
                                <div class="col-sm-1">
                                    <input  type="text" class="form-control text-right rtl-padding-left-5 rtl-editable" name="gps_mac" id="gps-mac-id"
                                            minlength="16" data-msg-minlength="Mín. 16 car."
                                            maxlength="24" data-msg-maxlength="Máx. 24 car."
                                            value="{{ $data['gps_mac'] }}"
                                            disabled readonly/>
                                </div>
                                <div class="col-sm-1 text-right">
                                    <label for="gps-mac-id" class="col-sm-1 control-label col-sm-push-9">ICCID</label>
                                </div>
                                <div class="col-sm-2">
                                    <input  type="text" class="form-control text-right rtl-editable" name="gps_iccid" id="gps-iccid-id"
                                            minlength="19" data-msg-minlength="Mín. 19 car."
                                            maxlength="19" data-msg-maxlength="Máx. 19 car."
                                            value="{{ $data['gps_iccid'] }}"
                                            disabled readonly />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="row">
                    <hr class="rtl-divider"/>
                    <div class="col-sm-12 text-right" id="vehicle-button-bar">
                        <button class="btn btn-success" id="vehicle-btnEdit">Editar</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var memberFormValidator = $('#vehicleDataForm').validate({
            onkeyup   : function (element, event) {
                if ($(element).attr('name') == 'name') {
                    return false // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if (event.which === 9 && this.elementValue(element) === '') {
                        return
                    } else if (element.name in this.submitted || element === this.lastElement) {
                        this.element(element)
                    }
                }
            },
            errorClass: 'text-danger',
            validClass: 'text-success',
            rules     : {
                gps_serial       : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.gps.serial') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('gps-serial-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ya existe un GPS con esa numeración'))
                                    } else {
                                        state = true
                                    }
                                    break
                                case 'Not Found':
                                    state = true;
                                    break
                            }
                            return state
                        }
                    }
                },
                errorElement     : 'em',
                errorPlacement   : function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass('help-block')
                    if (element.prop('type') === 'checkbox') {
                        error.insertAfter(element.parent('label'))
                    } else {
                        error.insertAfter(element)
                    }
                },
                highlight        : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-error').removeClass('has-success')
                },
                unhighlight      : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-success').removeClass('has-error')
                }
            }
        });

        $('#car-brand-id').on('changed.bs.select', function (evt) {
            $('#car-model-id').find('option').remove().end()
            $.ajax({
                type   : 'GET',
                url    : "{{ route('ajax.carmodels') }}",
                data   : {
                    'brand_id': $('#car-brand-id').find(':selected').val()
                },
                success: function (data) {
                    populateSelect('car-model-id', data)
                    $('#car-model-id').selectpicker('refresh')
                    $('#car-model-id').focus()
                },
                error  : function (response) {
                    ajaxStandardErrorTreatment(response.status, function () {
                        showNotification('Error', 'error', 'Ha ocurrido un error')
                    })
                }
            })
        });

        $('#unsuscribe_car_btn').on('click', function(event) {
            event.preventDefault();
            console.log('Ajax Call to unsuscribe the car.');
            alert ('Procedimiento por implementar.');

            return false;
        });
    </script>
@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            $('#vehicle-btnEdit').on('click', function (event) {
                event.preventDefault();
                updateControlsState('vehicle', false)         // Enable the controls.
                createExtraButtons('vehicle');
            });

            $('#gps-mac-id').on('blur', function () {
                var macAddr = $('#gps-mac-id').val()
                if ((macAddr.length > 0) && (macAddr.indexOf(':') == -1)) {
                    $('#gps-mac-id').val(macAddr.toString(16).match(/.{1,2}/g).join(':'))
                }
            });
        });
    </script>
@endsection