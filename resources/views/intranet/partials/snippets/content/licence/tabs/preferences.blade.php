<div role="preferencesData" class="tab-pane" id="preferencesData">
    <div class="container-fluid">
        <div class="form-horizontal">
            {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'preferencesDataForm', 'method' => 'POST']) !!}
            <input type="hidden" id="licence-id" value="{{ $data['id'] }}" name="licence_id">
            <div class="row">
                <fieldset class="col-sm-4">
                    <legend>Preferencias del conductor</legend>
                    <section>
                        <div class="col-sm-6">
                            <ol id="driver-preferences" class="list-group vertical driver-preferences rtl-sortable-list rtl-bordered rtl-editable">
                                @foreach ($data['member']['preferences'] as $preference)
                                <li class="list-group-item" draggable="true" data-driver-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="col-sm-6">
                            <ol class="list-group vertical driver-preferences rtl-bordered">
                                @foreach ($preferences['driver']['values'] as $preference)
                                <li class="list-group-item" draggable="true" data-driver-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </section>
                    <input type="hidden" name="driverPreferences" id="choosen-driver-preferences" value="{{ $preferences['driver']['ids'] }}"/>
                </fieldset>

                <fieldset class="col-sm-4">
                    <legend>Preferencias del vehículo</legend>
                    <section>
                        <div class="col-sm-6">
                            <ol id="vehicle-preferences" class="list-group vertical vehicle-preferences rtl-sortable-list rtl-bordered rtl-editable">
                                @foreach ($data['member']['vehicle']['preferences'] as $preference)
                                <li class="list-group-item" draggable="true" data-vehicle-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="col-sm-6">
                            <ol class="list-group vertical vehicle-preferences rtl-bordered">
                                @foreach ($preferences['vehicle']['values'] as $preference)
                                <li class="list-group-item" draggable="true" data-vehicle-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </section>
                    <input type="hidden" name="vehiclePreferences" id="choosen-vehicle-preferences" value="{{ $preferences['vehicle']['ids'] }}"/>
                </fieldset>

                <fieldset class="col-sm-4">
                    <legend>Idiomas</legend>
                    <section>
                        <div class="col-sm-6">
                            <ol id="spoken-languages-preferences" class="list-group vertical spoken-languages-preferences rtl-sortable-list rtl-bordered rtl-editable">
                                @foreach ($data['member']['languages'] as $preference)
                                    <li class="list-group-item" draggable="true" data-spoken-languages-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="col-sm-6">
                            <ol class="list-group vertical spoken-languages-preferences rtl-bordered">
                                @foreach ($languages['values'] as $preference)
                                    <li class="list-group-item" draggable="true" data-spoken-languages-id="{{ $preference->id }}">{{ $preference->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </section>

                    <input type="hidden" name="spokenLanguagesPreferences" id="choosen-spoken-languages-preferences" value="{{ $languages['ids'] }}"/>
                </fieldset>
            </div>
            <div class="row">
                <hr class="rtl-divider"/>
                <div class="col-sm-12 text-right" id="preferences-button-bar">
                    <button class="btn btn-success" id="preferences-btnEdit">Editar</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('document-ready')
@parent
    <script type="text/javascript">
        $(document).ready(function () {
            updateControlsState('preferences', true);

            $('#preferences-btnEdit').on('click', function (event) {
                event.preventDefault()

                updateControlsState('preferences', false);

                sortableField('driver');
                sortableField('vehicle');
                sortableField('spoken-languages');

                createExtraButtons('preferences')
            })

        })
    </script>
@endsection
