<div role="documentData" class="tab-pane" id="documentData">
    <div class="container-fluid">
        <div class="form-horizontal">
            {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'documentDataForm', 'method' => 'POST']) !!}
            <input type="hidden" id="licence-id" value="{{ $data['id'] }}" name="licence_id">
            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-2 text-right">
                                <label class="control-label">Tarjeta de Transporte</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="number" class="form-control text-right rtl-editable" name="transport_card" id="transport-card-id"
                                       value="{{ $data['card-info']['enabled']['transport_card'] }}"
                                       data-originalValue="{{ $data['card-info']['enabled']['transport_card'] }}"
                                       minlength="8" data-msg-minlength="Mín. 8 car."
                                       maxlength="8" data-msg-maxlength="Máx. 8 car."
                                       required data-msg-required="Requerido" readonly disabled/>
                            </div>

                            <div class="col-sm-2 text-right">
                                <label for="tc_expiration_date_input_id" class="control-label">Fecha de caducidad</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group date form-date-past"
                                     data-date="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['card-info']['enabled']['tc_expiration_date'])->format('d-m-Y') }}"
                                     data-date-format="dd/mm/yyyy"
                                     data-link-field="tc_expiration_date_id"
                                     data-link-format="yyyy-mm-dd">
                                    <input id="tc_expiration_date_input_id" class="form-control text-right" size="16" type="text"
                                           value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['card-info']['enabled']['tc_expiration_date'])->format('d-m-Y') }}" required readonly>
                                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                </div>
                                <input type="hidden" id="tc_expiration_date_id" value="{{  Carbon\Carbon::createFromFormat('Y-m-d', $data['card-info']['enabled']['tc_expiration_date'])->format('Y-m-d') }}" name="tc_expiration_date"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-1 text-right">
                                <label for="application-date-input" class="control-label" data-toggle="tooltip" data-placement="bottom" title="Fecha de Solicitud">F. Solic.</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group date form-date-no-future"
                                     data-date="{{ empty($data['member']['application_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['application_date'])->format('d-m-Y') }}"
                                     data-date-format="dd/mm/yyyy"
                                     data-link-field="doa-id"
                                     data-link-format="yyyy-mm-dd">
                                    <input id="application-date-input" class="form-control text-right" size="16" type="text"
                                           value="{{ empty($data['member']['application_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['application_date'])->format('d-m-Y') }}" readonly>
                                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                </div>
                                <input type="hidden" id="doa-id" value="{{ empty($data['member']['application_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['application_date'])->format('Y-m-d') }}" name="application_date"/>
                            </div>

                            <div class="col-sm-1 text-right">
                                <label for="council-date-input" class="control-label" data-toggle="tooltip" data-placement="bottom" title="Fecha del Consejo Rector">F. Cons Rector</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group date form-date-no-future"
                                     data-date="{{ empty($data['member']['council_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['council_date'])->format('d-m-Y') }}"
                                     data-date-format="dd/mm/yyyy" data-link-field="doc-id" data-link-format="yyyy-mm-dd">
                                    <input id="council-date-input" class="form-control text-right" size="16" type="text"
                                           value="{{ empty($data['member']['council_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['council_date'])->format('d-m-Y') }}"
                                           readonly>
                                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                </div>
                                <input type="hidden" id="doc-id" value="{{ empty($data['member']['council_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['council_date'])->format('Y-m-d') }}" name="council_date"/>
                            </div>

                            <div class="col-sm-1 text-right">
                                <label for="communication-date-input" class="control-label" data-toggle="tooltip" data-placement="bottom" title="Fecha de Comunicación">F. Comun.</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group date form-date-no-future"
                                     data-date="{{ empty($data['member']['communication_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['communication_date'])->format('d-m-Y') }}"
                                     data-date-format="dd/mm/yyyy" data-link-field="doco-id" data-link-format="yyyy-mm-dd">
                                    <input id="communication-date-input" class="form-control text-right" size="16" type="text"
                                           value="{{ empty($data['member']['communication_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['communication_date'])->format('d-m-Y') }}" readonly>
                                    <span class="input-group-addon"><span class="fa fa-times" aria-hidden="true"></span></span>
                                    <span class="input-group-addon"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                </div>
                                <input type="hidden" id="doco-id" value="{{ empty($data['member']['communication_date']) ? '' : Carbon\Carbon::createFromFormat('Y-m-d', $data['member']['communication_date'])->format('Y-m-d') }}" name="communication_date"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-sm-3">
                    <form method="post" action="{{ route('ajax-upload-post') }}" enctype="multipart/form-data" novalidate class="box">

                        <div class="box__input text-center">
                            <svg class="box__icon text-center" xmlns="http://www.w3.org/2000/svg" width="50" height="43" viewBox="0 0 50 43">
                                <i class="fa fa-cloud-upload fa-5x rtl-default"></i><br />
                                {{--<path d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z"/>--}}
                            </svg>
                            <input type="file" name="files[]" id="file" class="box__file" style="display:none" data-multiple-caption="{count} files selected" multiple />
                            <label for="file" class="rtl-default">
                                <span class="rtl-btn-primary">Pulse aquí para subir ficheros</span>
                                <strong></strong><br />
                                <span class="box__dragndrop"> o arrástrelos aquí.</span>.
                            </label>
                            <button type="submit" class="box__button">Enviar los archivos seleccionados</button>
                        </div>
                        <div id="box-listing hidden" class="box__success"></div>
                        <div class="box__uploading"></div>
                        <div class="box__error">Error! <span></span>. <a href="https://css-tricks.com/examples/DragAndDropFileUploading//?submit-on-demand" class="box__restart" role="button">Try again!</a></div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body rtl-collapsable collapsed" data-toggle="collapse" data-target="#licenceDocumentsList" aria-expanded="false" aria-controls="licenceDocumentsList">
                            <div class="col-sm-11"><h4>Documentación</h4></div>
                            <div class="col-sm-1 text-right"><i class="fa fa-chevron-down rtl-panel-indicator"></i></div>
                        </div>
                        <div class="collapse" id="licenceDocumentsList">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">&nbsp;</th>
                                    <th class="col-sm-8">Nombre del documento</th>
                                    <th class="col-sm-1">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody id="licence-documents"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body rtl-collapsable collapsed" data-toggle="collapse" data-target="#transportCardHistory" aria-expanded="false" aria-controls="transportCardHistory">
                            <div class="col-sm-11"><h4>Histórico de tarjetas de transporte</h4></div>
                            <div class="col-sm-1 text-right"><i class="fa fa-chevron-down rtl-panel-indicator"></i>
                            </div>
                        </div>
                        <div class="collapse" id="transportCardHistory">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-sm-2">Tarjeta de Transporte</th>
                                    <th class="col-sm-2">Fecha desactivación</th>
                                    <th class="col-sm-8">Razón</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($data['card-info']['disabled'] as $transportCard)
                                    <tr>
                                        <td>LM-{{ str_pad($transportCard['transport_card'], 3, '0', STR_PAD_LEFT) }}</td>
                                        <td>{{ empty($transportCard['disable_date']) ? 'No disponible' : Carbon\Carbon::createFromFormat('Y-m-d', $transportCard['disable_date'])->format('d-m-Y') }}</td>
                                        <td><span>{{ $transportCard['reason'] }}</span></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">No hay histórico de tarjetas de transporte</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <hr class="rtl-divider"/>
                <div class="col-sm-12 text-right" id="document-button-bar">
                    <button class="btn btn-success" id="document-btnEdit" type="button">Editar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var documentDataForm = $('#documentDataForm').validate({
            onkeyup   : function (element, event) {
                if ($(element).attr('name') == 'name') {
                    return false // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if (event.which === 9 && this.elementValue(element) === '') {
                        return
                    } else if (element.name in this.submitted || element === this.lastElement) {
                        this.element(element)
                    }
                }
            },
            errorClass: 'text-danger',
            validClass: 'text-success',
            rules     : {
                transport_card: {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.transport.card') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data  = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    if (isDirty('transport-card-id')) {
                                        ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ya existe esa tarjeta de transporte en la base de datos.'))
                                    } else {
                                        state = true
                                    }
                                    break
                                case 'Not Found':
                                    // We don't need to do anything
                                    state = true
                                    break
                            }
                            return state
                        }
                    }
                }
            }
        })

        function updateDocumentList(){

            // Clear licence-documents
            $('#licence-documents').empty();
            $.getJSON('{{ route('ajax-get-documents', ['licenceId' => $data['id']]) }}', function (data) {

                if (data.response.length > 0) {
                    for (var doc = 0; doc < data.response.length; doc++) {
                        $('#licence-documents').append('<tr><td><i class="fa ' + data.response[doc].icon +'"></i></td><td>' + data.response[doc].filename + '</td><td><a href="' + data.response[doc].link + '" target="_blank"><span><i class="fa fa-external-link"></i></span></a></td></tr>');
                    }
                } else {
                    $('#licence-documents').append("<tr><td colspan='3'>No hay documentos asociados</td></tr>");
                }
            });
        }

    </script>
@endsection

@include('intranet.sections.js-functions.upload-files')

@section('document-ready')
    @parent
    <script type='text/javascript'>
        $(document).ready(function () {
            // Update the document list...
            updateDocumentList();

            $('#document-btnEdit').on('click', function (event) {
                event.preventDefault()
                updateControlsState('document', false)         // Enable the controls.
                createExtraButtons('document')
            })

            $('.rtl-collapsable').on('click', function (event) {
                var indicatorToAdd    = 'fa-chevron-up'
                var indicatorToRemove = 'fa-chevron-down'
                if ($(this).hasClass('collapsed')) {
                    indicatorToAdd    = 'fa-chevron-down'
                    indicatorToRemove = 'fa-chevron-up'
                }
                $(this).find('.rtl-panel-indicator').removeClass(indicatorToRemove).addClass(indicatorToAdd)
            })
        })
    </script>
@endsection

