<section class="content-header">

    <h1>
    @if (!isset($data['title']))
        <span class="{{ $data['member']['rest_day'] }}">[LM-{{ str_pad($data['municipal_licence'], 3, '0', STR_PAD_LEFT) }}]</span> - {{ $data['member']['name'] }}&nbsp;{{ $data['member']['lastname']}} <small> Ficha del socio </small>
        <p><span><small>Lista {{ $data['member']['list'] }} </small></span></p>
    @else
        <span>{{ $title }}</span>
    @endif
    </h1>
    <p>&nbsp;</p>
    <ol class="breadcrumb">
        @foreach ($path as $pathItem)
            @if (!$loop->last)
                <li><a href="#"><i class="fa fa-dashboard"></i> {{ $pathItem }}</a></li>
            @else
                <li class='active'>{{ $pathItem }}</li>
            @endif

        @endforeach
    </ol>
</section>
