<aside class="main-sidebar">
    {{--sidebar: style can be found in sidebar.less--}}
    <section class="sidebar">
        {{--@include('intranet.partials.sidebar.user-panel')--}}
        @include('intranet.partials.sidebar.search')
        @include('intranet.partials.sidebar.menu', ['menuOptions' => $data['menu']])
    </section>
    {{--sidebar--}}
</aside>

@include('intranet.partials.snippets.modals.carplates')