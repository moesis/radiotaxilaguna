<a href="{{ route('admin.dashboard') }}" class="logo">
    <span class="logo-mini"><b>{{ Config::get('radiotaxilaguna.company.abbreviated') }}</b></span>
    <img src="{{ asset('assets/img/rtlogo-apaisado2.jpg') }}" class="logo-lg rtl-logo-admin" alt="{{ Config::get('radiotaxilaguna.company.name') }} {{ Config::get('radiotaxilaguna.company.surname') }} Logo">
</a>