<!-- User Account Menu -->
<li class="dropdown user user-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <!-- The user image in the navbar-->
        <img src="{{ asset($data['login']['user-thumb']) }}" class="user-image" alt="User Image">
        <!-- hidden-xs hides the username on small devices so only the image appears. -->
        <span class="hidden-xs">{{ $data['login']['user']->name }}</span>
    </a>
    <ul class="dropdown-menu">
        <!-- The user image in the menu -->
        <li class="user-header">
            <img src="{{ asset($data['login']['user-photo']) }}" class="img-circle" alt="User Image">
            <p>{{ $data['login']['user']->name }}
                <small>{{ ucfirst($data['login']['roles']->first()->name) }}</small>
            </p>
        </li>
        <!-- Menu Body -->
        <li class="user-body">
            <div class="row">
                <div class="col-xs-4 text-left">
                    <a href="#">
                        <button class="btn bg-red-active" type="button"
                                data-toggle="modal"
                                data-target="#suggestions"
                                data-dismiss="modal"
                                data-backdrop="false"
                                data-type="{{ \App\Http\Controllers\Ajax\FeedbackController::TICKET }}"
                                data-text="un error en"
                                data-caption="Enviar una notificación de error"
                                data-color="bg-red-active"
                                title="Nuevo ticket">
                            <i class="fa fa-ticket"></i>
                        </button>
                    </a>
                </div>
                <div class="col-xs-4 text-center">
                    @if ($data['login']['user']->hasAnyRole(['administrador', 'gestor']))
                        <a href="#">
                            <button class="btn bg-blue" type="button" data-backdrop="static" data-toggle="modal" data-target="#allSugestions" title="Listado de Tickets y Sugerencias">
                                <i class="fa fa-list"></i>
                            </button>
                        </a>
                    @endif
                </div>
                <div class="col-xs-4 text-right">
                    <a href="#">
                        <button class="btn bg-olive" type="button"
                                data-toggle="modal"
                                data-target="#suggestions"
                                data-dismiss="modal"
                                data-backdrop="false"
                                data-type="{{ \App\Http\Controllers\Ajax\FeedbackController::SUGGESTION }}"
                                data-text="una sugerencia de mejora de"
                                data-caption="Enviar una sugerencia"
                                data-color="bg-olive"
                                title="Nueva Sugerencia">
                            <i class="fa fa-comment-o"></i>
                        </button>
                    </a>
                </div>
            </div>
            <!-- /.row -->
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Perfil</a>
            </div>
            <div class="pull-right">
                <a href="{{ route('logout') }}#" class="btn btn-default btn-flat">Salir</a>
            </div>
        </li>
    </ul>
</li>

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        $('#suggestions').on('show.bs.modal', function (event) {
            var modal = $(this)
            var sender = $(event.relatedTarget)

            modal.find('#form-submit').prop('onclick', null).off('click')
            modal.find('#modal-header').removeClass().addClass('modal-header').addClass(sender.data('color'))
            modal.find('#caption').text(sender.data('caption'))
            modal.find('#message').text(sender.data('text'))
            modal.find('#type_id').val(sender.data('type'))
            modal.find('#text_id').val('').focus()

            $('#messages-id').empty()
            modal.find('#form-submit').click(function (e) {
                e.preventDefault()
                $.ajax({
                    type      : 'POST',
                    url       : '{{ route('ajax.feedback') }}',
                    data      : modal.find('#feedbackForm').serialize(),
                    beforeSend: function () {
                        $('#ajax-panel').removeClass('hidden')
                    },
                    success   : function (data) {
                        if (data) {
                            $('#ajax-panel').addClass('hidden')
                            showNotification('Hecho!', 'success', data)
                            modal.modal('hide')
                            $('body').removeClass('modal-open')
                            $('.modal-backdrop').remove()
                        }
                        modal.find('#form-submit').prop('onclick', null).off('click')
                    },
                    error     : function (data) {
                        $('#messages-id').empty()
                        $('#ajax-panel').addClass('hidden')
                        if (data) {
                            var message = 'Vuelva a intentarlo pasados unos minutos.'
                            switch (data.status) {
                                case 422:
                                    message = '<ul>'
                                    $.each(data.responseJSON, function (key, value) {
                                        if (Array.isArray(value)) {
                                            $.each(value, function (key, text) {
                                                message = message + '<li>' + text + '</li>'
                                            })
                                        }
                                    })
                                    message = message + '</ul>'
                                    break
                                default:
                            }
                            $('#messages-id').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i>Error</h4>' + message + '</div>').removeClass('hidden')
                        }
                        modal.find('#form-submit').prop('onclick', null).off('click')
                    }
                })
            })
        })
    </script>
@endsection