<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Propiedad de <b class="rtl-laguna-bright">{{ config('radiotaxilaguna.company.name').' '.config('radiotaxilaguna.company.surname').config('radiotaxilaguna.company.suffix')}}</b>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="http://www.elestadoweb.com" target="_blank">El Estado Web</a>.</strong> All rights reserved.
</footer>