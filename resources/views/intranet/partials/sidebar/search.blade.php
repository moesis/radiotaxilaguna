<!-- search form (Optional) -->
{!! Form::open(['url' => '#', 'id' => 'search-form', 'name' => 'searchForm', 'method' => 'get', 'class' => 'sidebar-form']) !!}

<div class="input-group">
    <input type="text" name="q" class="form-control" placeholder="Buscar...">
    <span class="input-group-btn">
            <button type="button" name="search" id="search-btn" class="btn btn-flat"
                    data-toggle="tooltip" data-placement="right" data-html="true"
                    title='<table><tr><th colspan="2">Búsquedas</th></tr><tr><td>MAT</td><td>Busca por la matrícula o parcial</td></tr><tr><td>LIC</td><td>Busca por el número de licencia o parcial</td></tr><tr><td>NOM</td><td>Busca por el nombre, apellidos o parcial</td></tr><tr><td></td><td>Busca por varios campos, pero resulta mucho más lento</td></tr></table>'>
                <i class="fa fa-search"></i>
            </button>
        </span>
</div>

{!! Form::close() !!}
<!-- /.search form -->

@section('footer-javascript')
@parent
    <script type="text/javascript">
        function doSearch(form) {
            var telegramPrefix = "{{ Config::get('radiotaxilaguna.prefixes.telegram') }}";
            var licenceShow = "{{ route('admin.licence.show', ['id' => 99999]) }}";

            $.ajax({
                type      : 'GET',
                url       : '{{ route('ajax.search') }}',
                data      : form.serialize(),
                beforeSend: function () {
                    $('#car-plate-modal').LoadingOverlay('show')
                },
                success   : function (response) {
                    var plateModal = $('#car-plate-modal');

                    plateModal.find('#plateModal-car-plate-id').val(response['data']['plate']);
                    plateModal.find('#plateModal-vehicle-id').val(response['data']['car']);
                    plateModal.find('#plateModal-member-id').val(response['data']['member']);
                    plateModal.find('#plateModal-municipal-licence-id').val('LM-' + response['data']['lm']);
                    plateModal.find('#plateModal-email-id').val(response['data']['email']);
                    plateModal.find('#plateModal-mobile-id').val(response['data']['mobile']);
                    plateModal.find('#plateModal-phone-id').val(response['data']['phone']);
                    plateModal.find('#plateModal-telegram-id').val(response['data']['telegram_alias']);
                    plateModal.find('#plateModal-telegram-link-id').attr("href", 'http://t.me/' + telegramPrefix + response['data']['lm']);
                    plateModal.find('#plateModal-go-to-licence-id').attr("href", licenceShow.replace('99999', response['data']['licence_id']));

                    plateModal.modal('show');
                },
                error     : function (jqXHR, exception) {
                    console.log(jqXHR)
                    console.log(exception)
                    showNotification('Oops! Ha ocurrido un error', 'error', 'Ha ocurrido un error durante la grabación de los datos.')
                },
                complete  : function () {
                    $('#car-plate-modal').LoadingOverlay('hide')
                }
            })
        }

        $('#search-btn').on('click', function (event) {
            event.preventDefault();
            doSearch($('#search-form'));
        })

        //$('#car-plate-model').on('show.bs.modal', function (event) {
        //    var button = $(event.relatedTarget);            // Button that triggered the modal
        //    var recipient = button.data('whatever');        // Extract info from data-* attributes
        //    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //    var modal = $(this);
        //
        //    modal.find('#car-plate-id').text('New message to ' + recipient)
        //    modal.find('.modal-body input').val(recipient)
        //})

    </script>
@endsection