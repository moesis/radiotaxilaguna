<!-- Sidebar user panel (optional) -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ asset($data['login']['user-thumb']) }}" class="user-image" alt="User Image">
    </div>
    <div class="pull-left info">
        <p>{{ $data['login']['user']->name }}</p>
        <!-- Status -->
        <a href="#" id="prueba"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
