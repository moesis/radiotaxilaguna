<ul class="sidebar-menu">
    <li class="header">Menu principal</li>

    @foreach (Config::get('radiotaxilaguna.menu') as $menuItem => $menuData)
    <li class="treeview {{ $data['menu']['activeGroupTitle'] === $menuData['label'] ? ' active' : ''}}">
        <a href="{{ $menuData['link'] }}">
            <i class="fa {{ $menuData['icon'] }}"></i> <span>{{ $menuData['label'] }}</span>
            @if (isset($menuData['options']) && count($menuData['options']) > 0)
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            @endif
        </a>
        <ul class="treeview-menu">
            @foreach ($menuData['options'] as $option)
                <li class="{{ $data['menu']['activeOption'] == $option['label'] ? 'rtl-option-active active' : ''}}">
                    <a href="{{ $option['link'] == '#' ? $option['link'] : route($option['link']) }}"><i class="fa {{ $option['icon'] }}"></i> {{ $option['label'] }}
                        @if (isset($option['options']) && count($option['options']) > 0)
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        @endif
                    </a>
                    @if (isset($option['options']) && count($option['options']) > 0)
                        <ul class="treeview-menu">
                            @foreach ($option['options'] as $subOption)
                                <li class="{{ $data['menu']['activeSubOption'] == $subOption['label'] ? 'rtl-option-active active': ''}}">
                                    <a href="{{ $subOption['link'] == '#' ? $subOption['link'] : route($subOption['link']) }}"><i class="fa {{ $subOption['icon'] }}"></i> {{ $subOption['label'] }}
                                        @if (isset($subOption['options']) && count($subOption['options']) > 0)
                                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </li>
    @endforeach

</ul>

