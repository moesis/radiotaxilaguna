@extends('intranet')

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-push-1 rtl-margin-top-5-percent">
                <div class="wizard" data-initialize="wizard" id="new-member-wizard">
                    {{-- steps --}}
                    <div class="steps-container">
                        <ul class="steps">
                            <li data-step="1" data-name="personal-data" class="active">
                                <span class="badge">1</span>Datos Personales
                                <span class="chevron"></span>
                            </li>
                            <li data-step="2" data-name="licence-data">
                                <span class="badge">2</span>Datos de la Licencia
                                <span class="chevron"></span>
                            </li>
                            <li data-step="3" data-name="vehicle-data">
                                <span class="badge">3</span>Datos del vehículo
                                <span class="chevron"></span>
                            </li>
                        </ul>
                    </div>

                    {{-- Buttons --}}
                    <div class="actions">
                        <button type="button" class="btn btn-default btn-prev">
                            <span class="glyphicon glyphicon-arrow-left"></span>Atrás
                        </button>
                        <button type="button" class="btn rtl-btn-primary btn-next" data-last="Complete">Siguiente
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </button>
                    </div>

                    {{-- Content --}}
                    {!! Form::open(['url' => '#', 'class'=>'form-horizontal', 'id' => 'new-member-form']) !!}
                    <div class="step-content">
                        @include('intranet.partials.snippets.content.licence.wizard.personaldata')
                        @include('intranet.partials.snippets.content.licence.wizard.licencedata')
                        @include('intranet.partials.snippets.content.licence.wizard.vehicledata')
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('intranet.partials.snippets.modals.addpostalcode')
@include('intranet.sections.js-functions.Licences')

@section('footer-javascript')
    @parent
    <script type="text/javascript">
        var memberFormValidator = $('#new-member-form').validate({
            onkeyup: function(element, event) {
                if ($(element).attr('name') == "name") {
                    return false; // disable onkeyup for your element named as "name"
                } else { // else use the default on everything else
                    if ( event.which === 9 && this.elementValue( element ) === "" ) {
                        return;
                    } else if ( element.name in this.submitted || element === this.lastElement ) {
                        this.element( element );
                    }
                }
            },
            errorClass: "text-danger",
            validClass: "text-success",
            rules: {
                municipal_licence: {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.lm') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa licencia municipal ya existe'));
                                    break;
                                case 'Not Found':
                                    $('#municipal-licence-id').val(parseInt($('#municipal-licence-id').val()).pad(3));
                                    state = true;
                                    break;
                            }

                            return state;
                        }
                    }
                },
                email            : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.email') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese correo ya está registrado.'));
                                    break;
                                case 'Not Found':
                                    state = true;
                                    break
                            }

                            return state
                        }
                    }
                },
                pid              : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.personalId') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ese DNI ya existe.'));
                                    break;
                                case 'Not Found':
                                    if (data.letter.length > 0) {
                                        showNotification('Letra DNI incorrecta', 'notice', 'La letra correcta es la <b>' + data.letter + '</b>')
                                    }
                                    $('#personal-id').val(($('#personal-id').val()).toUpperCase());
                                    state = true;
                                    break;
                            }

                            return state;
                        }
                    }
                },
                iban             : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.iban') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : true,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa cuenta IBAN se encuentra utilizada.'));
                                    break
                                case 'Not Found':
                                    state = true
                                    break
                            }

                            return state
                        },
                        error      : function (response) {
                            message = JSON.parse(response.responseText)
                            var msg = ''
                            if (message['status'] !== undefined && message['status'].length > 0) {
                                msg = message['status']
                            } else {
                                msg = 'Oops! ha ocurrido un error. Si persiste pongase en contacto con el administrador'
                            }
                            showNotification('Error', 'error', msg)
                        }
                    }
                },
                postalCode   : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.postalcode') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            $('#locality-id').val(data['name'])
                            $('#postal-code-val').val(data['id'])

                            return true
                        },
                        error      : function (response) {
                            ajaxStandardErrorTreatment(response.status, null, function () {
                                $('#locality-id').val('El código postal no existe...')
                                $('#addAction').removeClass('hidden')
                                $('#postal-code').focus()
                            })
                        }
                    }
                },
                transport_card   : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.transport.card') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Esa tarjeta de transporte ya existe'));
                                    break
                                case 'Not Found':
                                    // We don't need to do anything
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                plate            : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.vehicle.plate') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ya existe un coche con esa matrícula'));
                                    break
                                case 'Not Found':
                                    var plate = $('#car-plate-id').val();
                                    if (/-|\s/g.test(plate)) {
                                        $('#car-plate-id').val(plate.replace(/-|\s/g,""));
                                    }
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                gps_serial       : {
                    required: true,
                    remote  : {
                        url        : '{{ route('ajax.check.gps.serial') }}',
                        type       : 'GET',
                        dataType   : 'json',
                        contentType: 'application/json; charset=utf-8',
                        cache      : false,
                        onkeyup    : false,
                        dataFilter : function (response) {
                            var data = JSON.parse(response)
                            var state = false
                            switch (data.status) {
                                case 'Found':
                                    ajaxStandardErrorTreatment(response.status, showNotification('Error', 'error', 'Ya existe un GPS con esa numeración'));
                                    break
                                case 'Not Found':
                                    state = true
                                    break
                            }

                            return state
                        }
                    }
                },
                errorElement     : 'em',
                errorPlacement   : function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass('help-block')
                    if (element.prop('type') === 'checkbox') {
                        error.insertAfter(element.parent('label'))
                    } else {
                        error.insertAfter(element)
                    }
                },
                highlight        : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-error').removeClass('has-success')
                },
                unhighlight      : function (element, errorClass, validClass) {
                    $(element).parent().closest('div').addClass('has-success').removeClass('has-error')
                }
            }
        });

        {{-- Capturing the Submit event. --}}
        function sendSubmit(evt) {
            evt.preventDefault();
            $('#new-member-wizard').LoadingOverlay('show')
            var disabled = $('#new-member-form').find(':input:disabled').removeAttr('disabled')
            var formData = $('#new-member-form').serialize()
            disabled.attr('disabled', 'disabled')

            $.ajax({
                type   : 'POST',
                url    : '{{ route('ajax.add.member') }}',
                data   : formData,
                success: function (data) {
                    $('#new-member-wizard').LoadingOverlay('hide');
                    showNotification('Grabación correcta', 'success', 'Los datos del Socio han sido grabados con éxito.');
                },
                error  : function (data) {
                    $('#new-member-wizard').LoadingOverlay('hide');
                    if (data.responseJSON['code'] === undefined && data.responseJSON['message'] === undefined) {
                        console.log(data.responseJSON['code'], data.responseJSON['message']);
                        showNotification('ERROR', 'error', 'Ha ocurrido un error durante la operación. Consulte la consola.');
                    } else {
                        var errorInfo = JSON.parse(data.responseJSON.response);
                        $.each(errorInfo, function(key,valueObj){
                            showNotification('ERROR: ' + key, 'error', valueObj);
                        });
                    }
                    return false;
                }
            })
        }
    </script>
@endsection

@section('document-ready')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

            $('.form-date-past').datetimepicker(dateTimePickerDefaultOptions);
            $('.form-date-future').datetimepicker(dateTimePickerOptionsForFutureDates);

            $('#new-member-wizard').wizard()
                .on('finished.fu.wizard', function (evt) {
                    $('#new-member-form').validate();
                    if ($('#new-member-form').valid()) {
                        sendSubmit(evt);
                    }
                })
                .on('actionclicked.fu.wizard', function (evt, data) {
                    switch (data.direction) {
                        case 'previous':
                            break;
                        case 'next':
                            $('#new-member-form').validate();
                            if (!$('#new-member-form').valid()) {
                                evt.preventDefault();
                                return false;
                            }
                            break
                    }

                    return true
                })
                .on('changed.fu.wizard', function (evt, data) {
                    switch (data.step) {
                        case 1:
                            break;
                        case 2:
                            // Set the Transport Card Expiry date based on Personal ID
                            if ($('#personal-id').length > 0) {
                                // Remove dots and slashes and last item from string.
                                var dni = $('#personal-id').val().replace(/\.|\-|.$/g, "");
                                var expiryDate = moment().add(TRANSPORT_CARD_PERIOD, 'years').month(transportCardMonthMatching[dni.substr(dni.length-1)]);
                                $('#tc_expiration_date_input_id').datetimepicker('update', new Date(expiryDate.year(), expiryDate.month(), expiryDate.date()));
                                $('#tc_expiration_date_id').val(expiryDate.format('YYYY-MM-DD'));
                            }

                            $('#association-id').selectpicker('refresh');
                            if ($('#municipal-licence-id').val().length > 0) {

                                $('#telegram-alias').val('LM-' + parseInt($('#municipal-licence-id').val()).pad(3));
                            }
                            break;
                        case 3:
                            $('#car-brand-id').selectpicker('refresh');
                            break;
                    }
                })
        })
    </script>
@endsection

