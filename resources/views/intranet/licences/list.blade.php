@extends('intranet')
@section('header-css')
    {!! Html::style('assets/plugins/DataTables/datatables.css', ['media' => 'all', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['title' => $data['title'], 'path' => $data['path']])

    <section class="content rtl-margins-15">
        <table class="dt table table-hover table-responsive table-stripped rtl-full-width rtl-dt-head-line" id="licences-table">
            <thead>
            <tr>
                <th>Lic. Mun.</th>
                <th>Asociación</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Correo Electrónico</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>

        <section class="rtl-lists-footer">
            <div class="container-fluid">
                <section class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4 text-right">
                        <a href="{{ route('admin.licence.new') }}#">
                            <button id="add-new-member" class="btn btn-primary rtl-btn-primary">Agregar socio</button>
                        </a>
                    </div>
                </section>
            </div>
        </section>
    </section>
@endsection

@section('footer-javascript')
    @parent

    {!! Html::script('assets/plugins/DataTables/datatables.js') !!}

    <script type="text/javascript">
        $(function () {
            $('#licences-table').DataTable({
                processing: true,
                serverSide: true,
                language  : {
                    'url': "{{asset('assets/js/datatables-spanish.json')}}"
                },
                ajax      : '{{ route('ajax.licences.list') }}',
                dom       : 'Bfrtip',
                buttons   : ['csv', 'excel', 'pdf', 'print'],
                columns   : [
                    {
                        data      : 'municipal_licence',
                        name      : 'municipal_licence',
                        title     : 'Lic. Mun.',
                        orderable : true,
                        searchable: true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html(oData.municipal_licence);
                        }
                    },
                    {
                        data      : 'association.name',
                        name      : 'association.name',
                        title     : 'Asociación',
                        orderable : true,
                        searchable: true
                    },
                    {
                        data         : 'member.name',
                        name         : 'member.name',
                        title        : 'Nombre',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html(oData.member.name + "&nbsp;" + oData.member.lastname);
                        }
                    },
                    {
                        data         : 'member.mobile',
                        name         : 'member.mobile',
                        title        : 'Telefono',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if (oData.member.mobile !== 'N/A') {
                                $(nTd).html("<a href='tel:" + oData.member.mobile + "'>" + oData.member.mobile + "</a>");
                            }
                        }
                    },
                    {
                        data         : 'member.email',
                        name         : 'member.email',
                        title        : 'Correo Electronico',
                        orderable    : true,
                        searchable   : true,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            if (oData.member.email !== 'N/D') {
                                $(nTd).html("<a href='mailto:" + oData.member.email + "'>" + oData.member.email + "</a>");
                            } else {
                                $(nTd).html("- Correo no disponible -");
                            }
                        }
                    },
                    {
                        data      : 'action',
                        name      : 'action',
                        orderable : false,
                        searchable: false,
                        width     : '55px',
                        className : 'text-right'
                    },
                ],
            })
        })
    </script>
@endsection