@extends('intranet')

@section('header-css')
    {!! Html::style('assets/css/boxupload.css', ['media' => 'screen', 'rel' => 'stylesheet']) !!}
@endsection

@section ('intranet-content')

    @include('intranet.partials.snippets.content.header', ['data' => $data['data']['licence'], 'path' => $data['path'], 'title' => $data['title']])

    <section class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="licence-data">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="personalData" class="active"><a href="#personalData" aria-controls="personalData" role="tab" data-toggle="tab">Datos Personales</a></li>
                        <li role="vehicleData"><a href="#vehicleData" aria-controls="vehicleData" role="tab" data-toggle="tab" id="vehicleDataTab">Datos del vehículo y accesorios</a></li>
                        <li role="documentData"><a href="#documentData" aria-controls="documentData" role="tab" data-toggle="tab">Documentos</a></li>
                        <li role="economicData"><a href="#economicData" aria-controls="economicData" role="tab" data-toggle="tab">Documentos Económicos</a></li>
                        <li role="staffData"><a href="#staffData" aria-controls="staffData" role="tab" data-toggle="tab">Personal Asalariado</a></li>
                        <li role="preferencesData"><a href="#preferencesData" aria-controls="preferencesData" role="tab" data-toggle="tab">Preferencias e idiomas</a></li>
                    </ul>

                    <div class="tab-content">
                        @include('intranet.partials.snippets.content.licence.tabs.personaldata', ['data' => $data['data']['licence']])
                        @include('intranet.partials.snippets.content.licence.tabs.vehicledata', ['data' => $data['data']['licence'],'brands' => $data['data']['vehicles']['brands'],'models' => $data['data']['vehicles']['models']])
                        @include('intranet.partials.snippets.content.licence.tabs.documentdata', ['data' => $data['data']['licence']])
                        @include('intranet.partials.snippets.content.licence.tabs.economicdata', ['data' => $data['data']['licence']])
                        @include('intranet.partials.snippets.content.licence.tabs.staffdata', ['data' => $data['data']['licence']['staff']])
                        @include('intranet.partials.snippets.content.licence.tabs.preferences', ['data' => $data['data']['licence'],'languages' => $data['data']['languages'],'preferences' => $data['data']['preferences']])
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('intranet.partials.snippets.modals.annotations')
    @include('intranet.partials.snippets.modals.viewannotations')
@endsection

@include ('intranet.sections.js-functions.Licences')
