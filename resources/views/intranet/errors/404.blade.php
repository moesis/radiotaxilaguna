@extends('intranet')
@section ('intranet-content')

    <section class="content-header">
        <h1>
            404 Error Page - Elemento no encontrado
        </h1>
        <ol class="breadcrumb"></ol>
    </section>


    <section class="content" style="background-image: ">
        <div class="error-page">
            <img src="{{ asset('assets/img/errors/404.jpg') }}" class="user-image" alt="User Image">
            <h2 class="headline text-yellow"> 404</h2>
                <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! El elemento que busca no existe..</h3>
                <p>
                    Lo sentimos, no se ha podido encontrar el elemento que busca.
                    Puede volver al <a href="{{ route('admin.dashboard') }}"> panel de control </a>.
                </p>
            </div>
            <!-- /.error-content -->
        </div>
    </section>

@endsection