@extends('layouts.master')

@section('title', 'Área privada')

@section('content')

    <div class="container rtl-margin-top-100">
        <div class="row">
            <div class="col-md-5 col-md-offset-3">

                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ $error }}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="login-logo">
                    <a href="{{ route('public.home') }}"><b><span class="rtl-radio-block">Radi</span><span class="rtl-taxi-block">Taxi</span></b>
                        <span class="rtl-laguna-block">{{ Config::get('radiotaxilaguna.company.surname')  }}</span></a>
                </div>

                <div class="login-box-body">
                    <p class="login-box-msg">Para acceder a este área necesita las credenciales proporcionadas por la empresa.</p>
                    {!! Form::open(['name' => 'frmLogin', 'url' => route('post-login'), 'method' => 'POST']) !!}
                    <div class="form-group has-feedback">
                        {{ Form::text('email', '', ['type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) }}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required']) }}
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 block-center">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Acceder</button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#">He olvidado mi contraseña</a><br>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="register.html" class="text-center">Solicitar registro de usuario</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('footer-javascript')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            registerControlValidation('frmLogin', 'input', [
                "{{ Lang::get('general.html5.required.text') }}",
                "{{ Lang::get('general.html5.invalid.text') }}"
            ]);
        });
    </script>
@endsection