<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El \':attribute\' debe ser aceptado.',
    'active_url'           => 'El \':attribute\' no es una URL válida.',
    'after'                => 'El \':attribute\' debe ser una fecha posterior a :date.',
    'after_or_equal'       => 'El \':attribute\' debe ser fecha igual o posterior a :date.',
    'alpha'                => 'El \':attribute\' sólo puede contener letras.',
    'alpha_dash'           => 'El \':attribute\' sólo puede contener letras números y guiones.',
    'alpha_num'            => 'El \':attribute\' sólo puede contener letras y números.',
    'array'                => 'El \':attribute\' debe ser una matriz.',
    'before'               => 'El \':attribute\' debe ser una fecha anterior a :date.',
    'before_or_equal'      => 'El \':attribute\' debe ser una fecha igual o anterior a :date.',
    'between'              => [
        'numeric' => 'El \':attribute\' debe encontrarse entre :min y :max.',
        'file'    => 'El \':attribute\' debe encontrarse entre :min y :max kilobytes.',
        'string'  => 'El \':attribute\' debe encontrarse entre :min y :max caracteres.',
        'array'   => 'El \':attribute\' debe tener entre :min y :max elementos.',
    ],
    'boolean'              => 'El \':attribute\' campo debe ser verdadero o falso.',
    'confirmed'            => 'El \':attribute\' el campo confirmación no coincide.',
    'date'                 => 'El \':attribute\' no es una fecha válida.',
    'date_format'    => 'El \':attribute\' no concucuerda con el formato :format.',
    'different'      => 'El \':attribute\' y :other deben ser diferentes.',
    'digits'         => 'El \':attribute\' debe tener :digits digitos.',
    'digits_between' => 'El \':attribute\' debe tener entre :min y :max digitos.',
    'dimensions'     => 'El \':attribute\' imagen tiene una dimensión no válida.',
    'distinct'       => 'El \':attribute\' campo tiene un valor duplicado.',
    'email'          => 'El \':attribute\' debe ser una dirección de correo válida.',
    'exists'         => 'El \':attribute\' no tiene correlación con un registro en la base de datos.',
    'file'           => 'El \':attribute\' debe ser un fichero.',
    'filled'         => 'El \':attribute\' campo debe tener un valor.',
    'image'          => 'El \':attribute\' debe ser una imagen.',
    'in'             => 'El \':attribute\' seleccionado no es válido.',
    'in_array'       => 'El \':attribute\' campo no existe en :other.',
    'integer'        => 'El \':attribute\' debe ser un número entero.',
    'ip'             => 'El \':attribute\' debe ser una dirección IP válida.',
    'json'                 => 'El \':attribute\' debe ser una cadena JSON válida.',
    'max'                  => [
        'numeric' => 'El \':attribute\' No debe ser mayor que :max.',
        'file'    => 'El \':attribute\' No debe ser mayor que :max kilobytes.',
        'string'  => 'El \':attribute\' No debe ser mayor que :max caracteres.',
        'array'   => 'El \':attribute\' No debe tener más de :max elementos.',
    ],
    'mimes'                => 'El \':attribute\' debe ser un fichero de tipo: :values.',
    'mimetypes'            => 'El \':attribute\' debe ser un fichero de tipo: :values.',
    'min'                  => [
        'numeric' => 'El \':attribute\' debe tener un tamaño de al menos :min.',
        'file'    => 'El \':attribute\' debe tener un tamaño de al menos :min kilobytes.',
        'string'  => 'El \':attribute\' debe tener un tamaño de al menos :min caracteres.',
        'array'   => 'El \':attribute\' debe tener un tamaño de al menos :min elementos.',
    ],
    'not_in'               => 'El \':attribute\' seleccionado no es válido.',
    'numeric'              => 'El \':attribute\' debe ser un número.',
    'present'              => 'El campo \'\':attribute\'\' debe estar presente.',
    'regex'                => 'El campo \'\':attribute\'\' tiene un formato inválido.',
    'required'             => 'El campo \'\':attribute\'\' es requerido.',
    'required_if'          => 'El campo \'\':attribute\'\' es requerido cuando cuando :other sea :value.',
    'required_unless'      => 'El campo \'\':attribute\'\' es requerido a menos que :other esté en :values.',
    'required_with'        => 'El campo \'\':attribute\'\' es requerido cuando cuando :values esté presente.',
    'required_with_all'    => 'El campo \'\':attribute\'\' es requerido cuando cuando :values esté presente.',
    'required_without'     => 'El campo \'\':attribute\'\' es requerido cuando cuando :values no esté presente.',
    'required_without_all' => 'El campo \'\':attribute\'\' es requerido cuando cuando ninguno de estos :values estén presentes.',
    'same'                 => 'El \':attribute\' and :other must match.',
    'size'                 => [
        'numeric' => 'El \':attribute\' debe tener un tamaño de :size.',
        'file'    => 'El \':attribute\' debe tener un tamaño :size kilobytes.',
        'string'  => 'El \':attribute\' debe tener un tamaño :size characters.',
        'array'   => 'El \':attribute\' debe contener :size elementos.',
    ],
    'string'               => 'El \':attribute\' debe ser una cadena de texto.',
    'timezone'             => 'El \':attribute\' debe ser un huso horario válido.',
    'unique'               => 'El \':attribute\' debe ser único.',
    'uploaded'             => 'Ha fallado la carga del elemento \':attribute\' .',
    'url'                  => 'El formato de \':attribute\' no es válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
