<?php

return [

    // HTML5 Error messages.
    'html5.required.text' => 'Este campo es requerido. Debe ser rellenado.',
    'html5.invalid.text'  => 'El campo no tiene el formao adecuado.',

];
