<?php

return [
    'dripIntervalInMilliSeconds' => 300000,         // every 5 minutes
    'domain' => '/',                           // defaults to url('/')
    'route' => '/admin/genealabs/laravel-caffeine/drip',   // can be customized
];
