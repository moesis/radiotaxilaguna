<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'name' => 'required|string|min:3|max:20|unique:languages,name,@id',
    ],

    'fieldDefinitions' => [
        'name' => [
            'label'       => 'idioma',
            'type'        => 'text',
            'controlType' => 'INPUT',
            'placeholder' => '',
        ],
    ],
];