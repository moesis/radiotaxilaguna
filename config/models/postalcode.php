<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules'    => [
        'postal_code' => 'required|numeric|max:99999',
        'place'       => 'required|max:200',
        'locality_id' => 'required|exists:localities,id',
    ]
];