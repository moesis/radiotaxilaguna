<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Model Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'user'     => 'required|string|min:3|max: 200|unique:users,username@id',
        'email'    => 'required|email|unique:users,email,@id',
        'password' => 'required|string|min:4|max:12',
        'action'   => 'string|in:activate,deactivate',
    ],
];