<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Member Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules'    => [
        'pid'                 => 'required|string|min:9|max:11|unique:members,pid',
        'pid_expiration_date' => 'required|date',
        'name'                => 'required|string|min:3|max:40',
        'lastname'            => 'required|string|min:3|max:100',
        'address'             => 'required|string|min:5|max:200',
        'mobile'              => 'required|string|min:9',
        'email'               => 'required|email',
        'iban'                => 'required|string|min:24|max:34|unique:members,iban',
        'rest_day'            => 'required|alpha_num|min:2|max:2',
    ],
    'attributes' => [
        'pid'                 => 'DNI',
        'pid_expiration_date' => 'Fecha de caducidad del DNI.',
        'name'                => 'Nombre',
        'lastname'            => 'Apellidos',
        'address'             => 'Dirección',
        'mobile'              => 'Número de móvil',
        'email'               => 'Correo electrónico.',
        'iban'                => 'Código IBAN de la cuenta corriente.',
        'rest_day'            => 'Día de descanso.',
    ],

];