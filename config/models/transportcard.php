<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Transport Card Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules'      => [
        'transport_card'     => 'required|string|min:8|max:8|unique:members_transportcard,transport_card@id',
        'tc_expiration_date' => 'required|date',
    ],
    'attributes' => [
        'transport_card'     => 'Tarjeta de Transporte',
        'tc_expiration_date' => 'Fecha de caducidad de la tarjeta de transporte',
    ],
];
