<?php


return [
    /*
    |--------------------------------------------------------------------------
    | Model Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'name' => 'string|min:3|max:200|exists:roles,name,@id',
    ],
];