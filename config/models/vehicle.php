<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Vehicle Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'car_model_id'    => 'required|exists:car_models,id',
        'plate'           => 'required|unique:vehicles,plate@id',
        'enrollment_date' => 'required|date',
        'places'          => 'required|numeric|min:4|max:7',
    ],
    'attributes' => [
        'car_model_id'    => 'modelo de vehículo',
        'plate'           => 'matrícula',
        'enrollment_date' => 'fecha de registro',
        'places'          => 'plazas'
    ]
];
