<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Licence Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data for new record.
    */
    'rules' => [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp,pdf,doc,docx,xls,xlsx',
    ],

    'messages' => [
        'file.mimes'    => 'El fichero subido no está permitido.',
        'file.required' => 'Un fichero es requerido.',
    ],

    'attributes' => [
        'file' => 'Fichero.',
    ],


    /*
    |--------------------------------------------------------------------------
    | Allowed mime types
    |--------------------------------------------------------------------------
    |
    | Those are the allowed mime types.
    */
    'mime-types' => [

    ]


];
