<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Licence Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data for new record.
    */
    'rules'      => [
        'municipal_licence' => 'required|numeric|min:1|max:365|unique:licences,municipal_licence@id',
        'gps_serial'        => 'required|string|size:9|unique:licences,gps_serial@id',
        'auriga_id'         => 'required|numeric|min:0|max:9999',
    ],
    //    'messages' => [
    //        'municipal_licence.required' => 'Debe proporcionar un número de licencia municipal.',
    //        'municipal_licence.unique'   => 'La licencia municipal debe ser única.',
    //        'gps_serial.required'        => 'Debe proporcionar un número de serie para el GPS.',
    //        'gps_serial.unique'          => 'El número de serie del GPS debe ser único.',
    //        'auriga_id.required'         => 'Debe introducir un PIN de auriga para el conductor.'
    //    ],
    'attributes' => [
        'municipal_licence' => 'Número de Licencia Municipal.',
        'gps_serial'        => 'Número de serie para el GPS.',
        'auriga_id'         => 'PIN de auriga para el conductor.',
    ],
];
