<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Member Rules
    |--------------------------------------------------------------------------
    |
    | Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'pid'                 => 'required|string|min:9|max:11|unique:staff,pid@id',
        'pid_expiration_date' => 'required|date',
        'name'                => 'required|string|min:3|max:40',
        'lastname'            => 'required|string|min:3|max:100',
        'address'             => 'required|string|min:5|max:200',
        'mobile'              => 'required|string|min:9',
        'email'               => 'required|email',
    ],
    'messages' => [
        'pid.required'                  => 'Debe proporcionar un DNI.',
        'pid.unique'                    => 'El DNI debe ser único.',
        'pid_expiration_date.required'  => 'Debe proporcionar una fecha de caducidad del DNI.',
        'pid_expiration_date.date'      => 'La fecha de caducidad del DNI debe ser una fecha válida.',
        'name.required'                 => 'Debe proporcionar un nombre para el asalariado',
        'lastname.required'             => 'Debe proporcionar unos apellidos para el asalariado',
        'mobile.required'               => 'Debe proporcionar un número de móvil.',
        'email.required'                => 'Debe proporcionar un correo electrónico.',
        'email.email'                   => 'La dirección de correo electrónico debe ser una dirección válida.',

    ]
];