<?php

return [
    /*
    |--------------------------------------------------------------------------
   |Model Rules
    |--------------------------------------------------------------------------
    |
   |Those are the rules that will be used to validate model data.
    */
    'rules' => [
        'name'     => 'required|string|min:3|max:40',
        'new-name' => 'required_with:name|string|min:3|max:40',
    ],
];