<?php
/**
 * Radio Taxi Laguna config file.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        April 2017
 * @catergory   Config
 * @package     Radio Taxi Laguna Intranet
 * @version     1.0
 *
 * @todo
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Domain Name
    |--------------------------------------------------------------------------
    |
    | This value is the domain name of your application. This value is used when
    | the framework needs to place the application's domain name in any location
    | as required by the application or its packages.
    */
    'domain'          => 'radiotaxilaguna.app',

    /*
    |--------------------------------------------------------------------------
    | Domain Name
    |--------------------------------------------------------------------------
    |
    | This value is the domain name of your application. This value is used when
    | the framework needs to place the application's domain name in any location
    | as required by the application or its packages.
    */
    'timestampfields' => [
        'created_at',
        'updated_at',
        'deleted_at',
    ],

    /*
    |--------------------------------------------------------------------------
    | Skin
    |--------------------------------------------------------------------------
    |
    | This value is the name of skin used in the Radio Taxi Laguna Intranet.
    */
    'skin'            => 'skin-purple-light',

    /*
    |--------------------------------------------------------------------------
    | The rest Day lists
    |--------------------------------------------------------------------------
    |
    | This value indicate the rest day for a licence.
    */
    'restDays'        => [
        'A1' => 'A1',
        'A2' => 'A2',
        'B1' => 'B1',
        'B2' => 'B2',
        'C1' => 'C1',
        'C2' => 'C2',
        'D1' => 'D1',
        'D2' => 'D2',
        'E1' => 'E1',
        'E2' => 'E2',
    ],

    /*
    |--------------------------------------------------------------------------
    | The list that a licence belongs
    |--------------------------------------------------------------------------
    |
    | This value indicate the list for a licence.
    */
    'taxi-lists'      => [
        '1' => 'Lista 1',
        '2' => 'Lista 2',
        '3' => 'Lista 3',
        '4' => 'Lista 4',
        '5' => 'Lista 5',
        '6' => 'Lista 6',
    ],

    /*
    |--------------------------------------------------------------------------
    | Company Data
    |--------------------------------------------------------------------------
    |
    | This value is the name of skin used in the Radio Taxi Laguna Intranet.
    */
    'company'         => [
        'name'          => 'Radio Taxi',
        'surname'       => 'Laguna',
        'suffix'        => ', Sdad. Coop.',
        'abbreviated'   => 'RTL',
        'phone'         => '+34 922.255.555',
        'email'         => 'info@radiotaxilaguna.com',
        'app-email'     => 'app@radiotaxilaguna.com',
        'members-email' => 'socios@radiotaxilaguna.com',
        'vehicles'      => [
            'places'          => [
                'max' => 9,
                'min' => 4,
            ],
            'disabled-places' => [
                'max' => 2,
                'min' => 0,
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Postal Codes Files
    |--------------------------------------------------------------------------
    |
    | Files that always should exists where the postal codes will be imported.
    */
    'postal-codes'    => [
        'cities' => 'codciu.txt',
    ],

    /*
    |--------------------------------------------------------------------------
    | Setup & Config Data
    |--------------------------------------------------------------------------
    |
    | Those values are used in several application areas.
    */
    'setup'           => [
        'app'    => [
            'path' => 'inmogestion/inmobackend/',
        ],
        'fields' => [
            'password' => [
                'min' => 4,
                'max' => 12,
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Team
    |--------------------------------------------------------------------------
    |
    | Emails where the important messages would be sent, specially the ALERT,
    | CRITICAL,
    |
    */
    'team'            => [
        'developer'     => 'chernandez@elestadoweb.com',
        'developer1'    => 'josejavierhl@gmail.com',
        'administrator' => 'antonio.jorge@radiotaxilaguna.com',
        'owner'         => 'info@radiotaxilaguna.com',
        // Maybe would be necessary to define one or two more.
    ],

    /*
    |--------------------------------------------------------------------------
    | File Extensions.
    |--------------------------------------------------------------------------
    |
    | File extensions and the related fontawesome icon.
    |
    */
    'file-types'      => [
        'doc'  => 'fa-file-word-o',
        'docx' => 'fa-file-word-o',
        'xls'  => 'fa-file-excel-o',
        'xlsx' => 'fa-file-excel-o',
        'pdf'  => 'fa-file-pdf-o',
        'png'  => 'fa-file-image-o',
        'jpg'  => 'fa-file-image-o',
        'gif'  => 'fa-file-image-o',
        'zip'  => 'fa-file-archive-0',
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu
    |--------------------------------------------------------------------------
    |
    | This value is the name of skin used in the Radio Taxi Laguna Intranet.
    */
    'menu'            => [
        [
            'level'   => '',
            'link'    => '#',
            'label'   => 'Reservas y servicios',
            'icon'    => 'fa-car',
            'options' => [
                [
                    'level' => '',
                    'icon'  => 'fa-handshake-o',
                    'label' => 'Reservas',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-map-marker',
                    'label' => 'Servicios',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-user-circle-o',
                    'label' => 'Pasajeros',
                    'link'  => '#',
                ],
            ],
        ],
        [
            'level'   => '',
            'link'    => '#',
            'label'   => 'Informes',
            'icon'    => 'fa-print',
            'options' => [
                [
                    'level' => '',
                    'icon'  => 'fa-android',
                    'label' => 'Emisoras',
                    'link'  => 'admin.reports.stationandgps',
                ],
            ],
        ],
        [
            'level'   => '',
            'link'    => '#',
            'label'   => 'Sociedad',
            'icon'    => 'fa-address-book-o',
            'options' => [
                [
                    'level' => '',
                    'link'  => 'admin.licences.list',
                    'label' => 'Licencias',
                    'icon'  => 'fa-user',
                ],
                [
                    'level' => '',
                    'link'  => 'admin.staff.list',
                    'label' => 'Personal asalariado',
                    'icon'  => 'fa-user-o',
                ],
                [
                    'level'   => '',
                    'link'    => '#',
                    'label'   => 'Vehículos',
                    'icon'    => 'fa-car',
                    'options' => [
                        [
                            'level' => '',
                            'icon'  => 'fa-car',
                            'label' => 'Marcas',
                            'link'  => 'admin.carbrand.list',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-bullseye',
                            'label' => 'Modelos',
                            'link'  => 'admin.carmodel.list',
                        ],

                    ],
                ],
            ],
        ],
        [
            'level'   => '',
            'link'    => '#',
            'label'   => 'Mantenimientos',
            'icon'    => 'fa-cogs',
            'options' => [
                [
                    'level'   => '',
                    'link'    => '#',
                    'label'   => 'Idiomas y Lugares',
                    'icon'    => 'fa-life-ring',
                    'options' => [
                        [
                            'level' => '',
                            'icon'  => 'fa-comments-o',
                            'label' => 'Idiomas',
                            'link'  => 'admin.crud.language.list',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-soccer-ball-o',
                            'label' => 'Paises',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-map-o',
                            'label' => 'Provincias',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-map-signs',
                            'label' => 'Municipios',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-map-pin',
                            'label' => 'Localidades',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-location-arrow',
                            'label' => 'Códigos Postales',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-institution',
                            'label' => 'Lugares',
                            'link'  => '#',
                        ],
                    ],
                ],
                [
                    'level'   => '',
                    'link'    => '#',
                    'label'   => 'Administrativas',
                    'icon'    => 'fa-life-ring',
                    'options' => [
                        [
                            'level' => '',
                            'icon'  => 'fa-file-text-o',
                            'label' => 'Tipos de contrato',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-money',
                            'label' => 'Métodos de pago',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-pencil-square-o',
                            'label' => 'Cuotas',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-hand-o-right',
                            'label' => 'Tipos de Contrato',
                            'link'  => '#',
                        ],
                        [
                            'level' => '',
                            'icon'  => 'fa-motorcycle',
                            'label' => 'Precio de trayectos',
                            'link'  => '#',
                        ],
                    ],
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-list',
                    'label' => 'Listas',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-slideshare',
                    'label' => 'Personal',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-hand-o-right',
                    'label' => 'Preferencias',
                    'link'  => '#',
                ],
            ],
        ],
        [
            'level'   => '',
            'link'    => '#',
            'label'   => 'Seguridad',
            'icon'    => 'fa-key',
            'options' => [
                [
                    'level' => '',
                    'icon'  => 'fa-users',
                    'label' => 'Usuarios',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-vcard',
                    'label' => 'Roles',
                    'link'  => '#',
                ],
                [
                    'level' => '',
                    'icon'  => 'fa-unlock-alt',
                    'label' => 'Permisos',
                    'link'  => '#',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Expirations notices
    |--------------------------------------------------------------------------
    |
    | How many days before the app must warning about document expiration.
    |
    */
    'expirations'     => [
        'pid_expiration_date'                     => 30,
        'driver_licence_expiration_date'          => 30,
        'municipal_licence_expiration_date'       => 30,
        'tc_expiration_date'                      => 30,
        'staff-driver_licence_expiration_date'    => 30,
        'staff-municipal_licence_expiration_date' => 30,
        'staff-pid_expiration_date'               => 30,
    ],

    /*
    |--------------------------------------------------------------------------
    | Prefix
    |--------------------------------------------------------------------------
    |
    | Prefix used to look for into database and to recover from telegram
    |
    */
    'prefixes'        => [
        'Telegram'    => 'RTLLM',
        'application' => 'LM-',
    ],

];