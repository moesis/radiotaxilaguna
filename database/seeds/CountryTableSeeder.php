<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryTableSeeder extends Seeder
{
    protected $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'id'   => 1,
                'name' => 'España',
            ],
        ];

        DB::table('countries')->insert($this->data);
    }
}
