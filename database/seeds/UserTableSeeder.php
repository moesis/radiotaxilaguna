<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    protected $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'name'     => 'Antonio Jorge',
                'email'    => 'antonio.jorge@gmail.com',
                'password' => bcrypt('secret'),
                'activate' => 1,
                'created_at' => new \DateTime()

            ],
            [
                'name'     => 'Cayetano Hernández',
                'email'    => 'moesis@gmail.com',
                'password' => bcrypt('secret'),
                'activate' => 1,
                'created_at' => new \DateTime()
            ]
        ];

        DB::table('users')->insert($this->data);
    }
}
