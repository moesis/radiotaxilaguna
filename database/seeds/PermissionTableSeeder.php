<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    protected $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'name'       => 'agregar lenguages',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'editar lenguages',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'borrar lenguages',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'ver lenguages',
                'guard_name' => 'web',
            ],
        ];

        DB::table('permissions')->insert($this->data);
    }

}
