<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceTableSeeder extends Seeder
{
    protected $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'id'         => 1,
                'name'       => 'Santa Cruz de Tenerife',
                'country_id' => 1,
            ],
        ];

        DB::table('provinces')->insert($this->data);
    }
}
