<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    protected $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'name'       => 'root',
                'guard_name' => 'web',

            ],
            [
                'name'       => 'administrador',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'agente',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'operador',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'member',
                'guard_name' => 'web',
            ],
        ];

        DB::table('roles')->insert($this->data);
    }

}
