<?php
/**
 * Reservations
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        April 2017
 * @catergory   Migrations
 * @package     Radio Taxi Laguna Intranet.
 * @version     1.0
 *
 * @todo
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    protected $tablename = 'reservations';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->dateTime('date');

            // which user will create the reservation.
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            // Who create the reservation.
            $table->integer('agency_id')->unsigned();
            $table->foreign('agency_id')->references('id')->on('agencies');

            // The passenger data.
            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('passengers');

            // Passenger's name just in case is not a periodical user.
            $table->string('passenger', 200)->nullable();

            // Number of pax. We must calculate the taxi numbers.
            $table->tinyInteger('pax')->default(1);

            // From a know place.
            $table->integer('from_id')->unsigned();
            $table->foreign('from_id')->references('id')->on('places');

            // If place is not known
            $table->text('from')->nullable();

            $table->dateTime('collect_date');

            // To a know place.
            $table->integer('to_id')->unsigned();
            $table->foreign('to_id')->references('id')->on('places');

            // If place is not known
            $table->text('to')->nullable();

            $table->string('flight_number', 20)->nullable();
            $table->time('flight_time')->nullable();

            // Economic data.
            $table->decimal('price', 18, 2);        // Include complements.
            $table->decimal('tax', 18, 2);          // IGIC 3%

            // Payment Method
            $table->integer('payment_method_id')->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');

            // which user will cancel the reservation.
            $table->boolean('cancelled')->default(false);
            $table->integer('cancelled_by')->unsigned()->nullable();
            $table->foreign('cancelled_by')->references('id')->on('users');
            $table->string('cancellation_code', 200)->nullable();
            $table->dateTime('cancellation_date')->nullable();
            $table->string('cancelled_from', 20)->nullable();
            $table->text('cancelled_notes')->nullable();

            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_created_by_foreign');
            $table->dropColumn('created_by');

            $table->dropForeign($this->tablename . '_agency_id_foreign');
            $table->dropColumn('agency_id');

            $table->dropForeign($this->tablename . '_passenger_id_foreign');
            $table->dropColumn('passenger_id');

            $table->dropForeign($this->tablename . '_from_id_foreign');
            $table->dropColumn('from_id');

            $table->dropForeign($this->tablename . '_to_id_foreign');
            $table->dropColumn('to_id');

            $table->dropForeign($this->tablename . '_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');

            $table->dropForeign($this->tablename . '_cancelled_by_foreign');
            $table->dropColumn('cancelled_by');
        });

        Schema::dropIfExists($this->tablename);

    }
}