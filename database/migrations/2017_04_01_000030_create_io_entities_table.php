<?php
/**
 * Registro de entrada y salida
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIOEntitiesTable extends Migration
{
    // Registro de entrada y salida
    protected $tablename = 'io_entities';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('entity', 255);
            $table->string('department', 255);
            $table->string('name', 255);
            $table->string('lastname', 255);
            $table->string('address', 255);
            $table->integer('postal_code_id')->unsigned();
            $table->string('phone_1', 12);
            $table->string('phone_2', 12)->nullable();
            $table->string('mobile_1', 12)->nullable();
            $table->string('mobile_2', 12)->nullable();
            $table->string('contact', 255)->nullable();
            $table->string('website', 200)->nullable();
            $table->integer('telegram_id')->unsigned()->nullable();
            $table->string('telegram_alias', 200)->nullable();

            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
