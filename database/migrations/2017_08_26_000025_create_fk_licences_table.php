<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkLicencesTable extends Migration
{
    protected $tablename = 'licences';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('association_id')->references('id')->on('associations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_member_id_foreign');
            $table->dropColumn('member_id');

            $table->dropForeign($this->tablename . '_association_id_foreign');
            $table->dropColumn('association_id');
        });
    }
}
