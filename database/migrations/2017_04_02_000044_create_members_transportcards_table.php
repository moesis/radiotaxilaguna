<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTransportcardsTable extends Migration
{
    protected $tablename = 'members_transportcard';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('member_id')->unsigned();
            $table->string('transport_card', 30);               // Transport card emitted by TownHall.
            $table->date('tc_expiration_date');                    // Transport card expiration date

            $table->tinyInteger('disabled')->default(0);        // 1 if it is disabled, 0 otherwise
            $table->date('disable_date')->nullable();           // date when it was disabled
            $table->text('reason')->nullable();                 // reason for deactivation

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
