<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersFeePaymentsTable extends Migration
{
    protected $tablename = 'members_fee_payments';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('member_id')->unsigned();
            $table->integer('member_fee_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();

            $table->dateTime('payment_date');
            $table->decimal('amount', 18, 2);
            $table->decimal('debt', 18,2)->default(0);          // if the payment is not by the full amount.
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
