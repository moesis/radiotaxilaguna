<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationsTable extends Migration
{
    protected $tablename = 'associations';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('name', 200)->unique();
            $table->string('pid', 255)->unique()->nullable();                         // see http://bit.ly/2obEjOA
            $table->string('address', 200)->nullable();
            $table->integer('postal_code_id')->unsigned()->nullable();
            $table->string('email', 255)->unique()->nullable();
            $table->string('phone_1', 12)->nullable();
            $table->string('phone_2', 12)->nullable();
            $table->string('mobile_1', 12)->nullable();
            $table->string('mobile_2', 12)->nullable();
            $table->string('website', 200)->nullable();
            $table->string('logo', 200)->nullable();            // Ruta relativa al Public donde esté el logo de la asociación.
            $table->string('president', 255)->nullable();
            $table->string('secretary', 255)->nullable();
            $table->string('treasurer', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
