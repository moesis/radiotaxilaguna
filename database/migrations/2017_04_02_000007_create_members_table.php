<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    protected $tablename = 'members';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('pid', 11)->index();                             // ID Card of licence owner
            $table->date('pid_expiration_date');                            // Expiration date for personal ID
            $table->date('dob')->nullable();
            $table->string('name', 40);
            $table->string('lastname', 100);
            $table->string('address', 200);
            $table->string('address_1', 200)->nullable();
            $table->integer('postal_code_id')->unsigned();
            $table->string('phone', 30)->nullable();                        // Several telephone numbers
            $table->string('mobile', 30);
            $table->string('email', 255)->unique();
            $table->string('telegram_alias', 255)->unique()->nullable();    // Por defecto será la concatenación de 'RTL' con la licencia, p.e. RTL120
            $table->integer('telegram_id')->unique()->nullable();
            $table->string('iban', 34)->unique();                           // see https://en.wikipedia.org/wiki/International_Bank_Account_Number
            $table->string('rest_day', 2);                                  // A1, A2, B1, B2, C1, C2, D1, D2, E1, E2
            $table->tinyInteger('list')->unsigned();                        // 1, 2, 3, 4, 5, 6
            $table->date('driver_licence_expiry_date')->nullable();         // Fecha de caducidad del carnet de conducir
            $table->date('municipal_licence_expiry_date')->nullable();      // Fecha de cadudidad de la licencia de taxi.
            $table->date('application_date')->nullable();                   // Fecha de solicitud de inscripción.
            $table->date('council_date')->nullable();                       // Fecha de Consejo Rector.
            $table->date('communication_date')->nullable();                 // Fecha de comunicación del resultado

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}