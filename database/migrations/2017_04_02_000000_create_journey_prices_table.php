<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneyPricesTable extends Migration
{
    protected $tablename = 'journey_prices';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('name')->unique();

            $table->integer('from_id')->unsigned();
            $table->foreign('from_id')->references('id')->on('places');

            $table->integer('to_id')->unsigned();
            $table->foreign('to_id')->references('id')->on('places');

            $table->decimal('price', 18,2);

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function (Blueprint $table)
        {
            $table->dropForeign($this->tablename . '_from_id_foreign');
            $table->dropColumn('from_id');

            $table->dropForeign($this->tablename . '_to_id_foreign');
            $table->dropColumn('to_id');
        });

        Schema::dropIfExists($this->tablename);
    }
}
