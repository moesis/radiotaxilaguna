<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkMembersLanguagesTable extends Migration
{
    protected $tablename = 'members_languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_member_id_foreign');
            $table->dropColumn('member_id');

            $table->dropForeign($this->tablename . '_language_id_foreign');
            $table->dropColumn('language_id');
        });
    }
}
