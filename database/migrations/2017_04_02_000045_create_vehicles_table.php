<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    protected $tablename = 'vehicles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('car_model_id')->unsigned();
            $table->string('plate')->unique();                      // Matrícula del coche
            $table->tinyInteger('places');                          // Número de plazas totales.
            $table->tinyInteger('disabled_places')->default(0);     // Número de plazas minusválidos (ocupan 2 plazas normales)
            $table->date('enrollment_date');                        // Fecha de alta
            $table->date('cancellation_date')->nullable();          // Fecha de cancelación del vehículo
            $table->text('notes')->nullable();                      // Notes for cancelation date.
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
