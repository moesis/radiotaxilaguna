<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModelsTable extends Migration
{
    protected $tablename = 'car_models';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('car_brands');
            $table->string('name', 200);
            $table->unique(['brand_id', 'name']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function (Blueprint $table)
        {
            $table->dropForeign($this->tablename . '_brand_id_foreign');
            $table->dropColumn('brand_id');
        });

        Schema::dropIfExists($this->tablename);
    }
}
