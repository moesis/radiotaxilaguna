<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    protected $tablename = 'receipts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            // Receipt Number would be 999-9999999 where
            // 999      - agency code
            // 9999999  - reservation_id
            $table->string('reference', 11)->unique();
            $table->integer('service_id')->unsigned();

            // Document date.
            $table->dateTime('date');

            // Document number, receipt number or invoice number
            $table->string('document_number', 10)->unique();

            // Economic data.
            // Total price of service. if it's different from reservation->price,
            // it prevail over resservation->price, otherwise will be copied from
            // reservation->price.
            $table->decimal('price', 18, 2);        // Include complements.
            $table->decimal('tax', 18, 2);          // IGIC 3%

            // If it is paid
            $table->boolean('paid')->default(false);

            // When the payment is done.
            $table->datetime('paid_in');

            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table($this->tablename, function ($table)
//        {
//            $table->dropForeign($this->tablename . '_service_id_foreign');
//            $table->dropColumn('service_id');
//        });

        Schema::dropIfExists($this->tablename);
    }
}
