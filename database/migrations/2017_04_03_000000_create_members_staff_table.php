<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersStaffTable extends Migration
{
    protected $tablename = 'members_staff';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->integer('staff_id')->unsigned();

            $table->integer('contract_type_id')->unsigned();    // Contract type
            $table->string('contract_reference')->nullable();               // Staff contract identifier in hard disk - an UUID
            $table->dateTime('start_date')->nullable();                     // Start contract date.
            $table->dateTime('end_date')->nullable();                       // End contract date.

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
