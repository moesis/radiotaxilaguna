<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkMembersPreferencesTable extends Migration
{
    protected $tablename = 'members_preferences';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('preference_id')->references('id')->on('preferences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_member_id_foreign');
            $table->dropColumn('member_id');

            $table->dropForeign($this->tablename . '_preference_id_foreign');
            $table->dropColumn('preference_id');
        });
    }
}
