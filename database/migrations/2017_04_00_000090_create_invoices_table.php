<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    protected $tablename = 'invoices';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            // Document date.
            $table->dateTime('date');

            // Billed to
            $table->string('invoice_to_name', 100);
            $table->string('invoice_to_tax', 50);
            $table->string('invoice_to_address_line_1', 100);
            $table->string('invoice_to_address_line_2', 100);
            $table->string('invoice_to_postal_code', 5);
            $table->string('invoice_to_municipality', 100);
            $table->string('invoice_to_locality', 100);
            $table->string('invoice_to_province', 100);
            $table->string('invoice_to_country', 100);

            // Economic data.
            $table->decimal('taxable_base', 18, 2);     // Amount before taxex
            $table->decimal('tax', 18, 2);              // IGIC 3%
            $table->decimal('grand_total', 18, 2);      // IGIC 3%

            // If it is paid
            $table->boolean('paid')->default(false);

            // When the payment is done.
            $table->datetime('paid_in');
            $table->text('footer')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
