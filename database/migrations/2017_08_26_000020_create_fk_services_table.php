<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkServicesTable extends Migration
{
    protected $tablename = 'services';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {

            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('done_by')->references('id')->on('users');
            $table->foreign('sent_to')->references('id')->on('agencies');
            $table->foreign('cancelled_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_reservation_id_foreign');
            $table->dropColumn('reservation_id');

            $table->dropForeign($this->tablename . '_done_by_foreign');
            $table->dropColumn('done_by');

            $table->dropForeign($this->tablename . '_sent_to_foreign');
            $table->dropColumn('sent_to');

            $table->dropForeign($this->tablename . '_cancelled_by_foreign');
            $table->dropColumn('cancelled_by');
        });
    }
}
