<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicencesTable extends Migration
{
    protected $tablename = 'licences';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('member_id')->unsigned();
            $table->integer('association_id')->unsigned();
            $table->integer('municipal_licence')->unsigned();               // Municipal Licence Number.
            $table->string('station_brand', 20)->nullable();                // Marca de la emisora
            $table->string('station_model', 20)->nullable();                // Modelo de la emisora
            $table->string('station_serial', 50)->nullable();               // Número de serie de la emisora
            $table->string('gps_brand', 20)->nullable();                    // Marca del GPS
            $table->string('gps_model', 20)->nullable();                    // Modelo del GPS
            $table->string('gps_serial', 50)->unique();                     // Número de serie del GPS fmto: 0082-9999
            $table->string('gps_mac', 50)->nullable();                      // Mac address del dispositivo GPS
            $table->string('gps_iccid', 19)->nullable();                    // ICCID of GPS SIM
            $table->string('auriga_id', 4);                                 // Auriga PIN code
            $table->string('alternative_auriga_id', 4)->nullable();         // Auriga PIN code del asalariado
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
