<?php
/**
 * Registro de entrada y salida
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkIOTable extends Migration
{
    // Registro de entrada y salida
    protected $tablename = 'io';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('io_document_types');
            $table->foreign('transport_id')->references('id')->on('io_transports');
            $table->foreign('from_member_id')->references('id')->on('members');
            $table->foreign('from_entity_id')->references('id')->on('io_entities');
            $table->foreign('to_member_id')->references('id')->on('members');
            $table->foreign('to_entity_id')->references('id')->on('io_entities');
            $table->foreign('answer_document_id')->references('id')->on('io');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_created_by_foreign');
            $table->dropColumn('created_by');

            $table->dropForeign($this->tablename . '_type_id_foreign');
            $table->dropColumn('type_id');

            $table->dropForeign($this->tablename . '_transport_id_foreign');
            $table->dropColumn('transport_id');

            $table->dropForeign($this->tablename . '_from_member_id_foreign');
            $table->dropColumn('from_member_id');

            $table->dropForeign($this->tablename . '_to_member_id_foreign');
            $table->dropColumn('to_member_id');

            $table->dropForeign($this->tablename . '_from_entity_id_foreign');
            $table->dropColumn('from_entity_id');

            $table->dropForeign($this->tablename . '_to_entity_id_foreign');
            $table->dropColumn('to_entity_id');

            $table->dropForeign($this->tablename . '_answer_document_id_foreign');
            $table->dropColumn('answer_document_id');
        });
    }
}
