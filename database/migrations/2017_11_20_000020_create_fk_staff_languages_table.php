<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkStaffLanguagesTable extends Migration
{
    protected $tablename = 'staff_languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('staff_id')->references('id')->on('staff');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_staff_id_foreign');
            $table->dropColumn('staff_id');

            $table->dropForeign($this->tablename . '_language_id_foreign');
            $table->dropColumn('language_id');
        });
    }
}
