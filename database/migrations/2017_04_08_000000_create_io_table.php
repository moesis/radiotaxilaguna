<?php
/**
 * Registro de entrada y salida
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIOTable extends Migration
{
    // Registro de entrada y salida
    protected $tablename = 'io';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('record_number', 20)->unique();      // <year> + id
            $table->tinyInteger('direccion');                   // 1 -> In; 2 -> Out
            $table->string('document_code')->unique();          // Generated UUID
            $table->text('description');
            $table->integer('created_by')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('transport_id')->unsigned();
            $table->integer('from_member_id')->unsigned()->nullable();
            $table->integer('from_entity_id')->unsigned()->nullable();
            $table->integer('to_member_id')->unsigned()->nullable();
            $table->integer('to_entity_id')->unsigned();
            $table->integer('answer_document_id')->unsigned();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
