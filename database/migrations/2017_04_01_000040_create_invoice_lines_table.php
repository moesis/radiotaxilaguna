<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceLinesTable extends Migration
{
    protected $tablename = 'invoice_lines';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('invoice_id')->unsigned();
            $table->foreign('invoice_id')->references('id')->on('invoices');

            $table->integer('quantity')->default(1);
            $table->string('journey_from', 200);
            $table->string('journey_to', 200);
            $table->string('reference_id', 11);
            $table->decimal('price', 18, 2);
            $table->decimal('discounts', 18, 2)->default(0.00);

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_invoice_id_foreign');
            $table->dropColumn('invoice_id');
        });

        Schema::dropIfExists($this->tablename);
    }
}
