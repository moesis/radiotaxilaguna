<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    protected $tablename = 'services';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->dateTime('date');                                       // Date when service is done.
            $table->integer('reservation_id')->unsigned();                  // Object to be converted to service.
            $table->integer('done_by')->unsigned()->nullable();             // Done by an associated member.
            $table->integer('sent_to')->unsigned()->nullable();             // Done by an external agency.
            $table->integer('cancelled_by')->unsigned()->nullable();
            $table->boolean('cancelled')->default(false);                   // which user will cancel the service.
            $table->string('cancellation_code', 200)->nullable();
            $table->dateTime('cancellation_date')->nullable();
            $table->string('cancelled_from', 20)->nullable();
            $table->text('cancelled_notes')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
