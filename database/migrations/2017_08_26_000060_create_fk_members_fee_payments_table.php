<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkMembersFeePaymentsTable extends Migration
{
    protected $tablename = 'members_fee_payments';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('member_fee_id')->references('id')->on('members_fees');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_member_id_foreign');
            $table->dropColumn('member_id');

            $table->dropForeign($this->tablename . '_member_fee_id_foreign');
            $table->dropColumn('member_fee_id');

            $table->dropForeign($this->tablename . '_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });
    }
}
