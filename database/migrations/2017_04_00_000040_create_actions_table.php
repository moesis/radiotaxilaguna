<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    protected $tablename = 'actions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('name', 200);
            $table->integer('expiration');            // days to end
            $table->integer('unit');                  // 1 - Dias; 2 - Año renovación
            $table->text('conditions')->nullable();   // JSON formatted string with the contidions as array with
                                                      // pattern
                                                      // [
                                                      //   'name' => '',
                                                      //   'field' => '',
                                                      //   'values' => [
                                                      //       'min' => 0,
                                                      //       'max' => 0,
                                                      //   ]
                                                      // ]
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
