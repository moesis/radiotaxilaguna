<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    protected $tablename = 'agencies';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->string('code', 255)->unique();
            $table->string('name', 255);
            $table->string('vat', 255)->unique();                         // see http://bit.ly/2obEjOA
            $table->string('address', 255);
            $table->integer('postal_code_id')->unsigned();
            $table->string('phone_1', 12);
            $table->string('phone_2', 12)->nullable();
            $table->string('mobile_1', 12)->nullable();
            $table->string('mobile_2', 12)->nullable();
            $table->string('contact', 255)->nullable();
            $table->string('website', 200)->nullable();
            $table->boolean('credit')->default(false);
            $table->decimal('max_discount', 18, 2)->default(0.00);
            $table->decimal('max_increase', 18, 2)->default(0.00);
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists($this->tablename);
    }
}
