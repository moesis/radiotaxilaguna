<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffActionsTable extends Migration
{
    protected $tablename = 'staff_actions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('staff_id')->unsigned();
            $table->foreign('staff_id')->references('id')->on('staff');

            $table->integer('action_id')->unsigned();
            $table->foreign('action_id')->references('id')->on('actions');

            $table->date('date_of_issue');
            $table->date('date_of_expiry');
            $table->dateTime('notified_on_date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table) {

            $table->dropForeign($this->tablename . '_staff_id_foreign');
            $table->dropColumn('staff_id');

            $table->dropForeign($this->tablename . '_action_id_foreign');
            $table->dropColumn('action_id');
        });

        Schema::dropIfExists($this->tablename);
    }
}
