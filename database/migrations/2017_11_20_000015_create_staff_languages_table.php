<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffLanguagesTable extends Migration
{
    protected $tablename = 'staff_languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('staff_id')->unsigned();
            $table->integer('language_id')->unsigned();

            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
