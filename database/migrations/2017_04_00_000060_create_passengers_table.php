<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    protected $tablename = 'passengers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unisgned();

            $table->string('name', 50);
            $table->string('lastname', 100);
            $table->string('vat', 10)->unique();                    // see http://bit.ly/2obViAh
            $table->string('phone', 30)->nullable();                // several phone numbers
            $table->string('mobile', 30)->nullable();               // several mobile numbers
            $table->string('email', 200)->unique();
            $table->string('telegram_alias', 255)->unique()->nullable();    // Por defecto será la concatenación de 'RTL' con la licencia, p.e. RTL120
            $table->integer('telegram_id')->unique()->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
