<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkStaffPreferencesTable extends Migration
{
    protected $tablename = 'staff_preferences';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('staff_id')->references('id')->on('staff');
            $table->foreign('preference_id')->references('id')->on('preferences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_staff_id_foreign');
            $table->dropColumn('staff_id');

            $table->dropForeign($this->tablename . '_preference_id_foreign');
            $table->dropColumn('preference_id');
        });
    }
}
