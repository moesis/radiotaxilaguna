<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    protected $tablename = 'staff';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tablename, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id')->unisgned();
            $table->string('pid', 10)->unique();                            // see http://bit.ly/2obViAh
            $table->date('pid_expiration_date');                            // Expiration date for personal ID
            $table->date('dob')->nullable();
            $table->string('name', 50);
            $table->string('lastname', 100);
            $table->string('address', 200);
            $table->string('address_1', 200)->nullable();
            $table->integer('postal_code_id')->unsigned();
            $table->string('phone', 30)->nullable();                        // several phone numbers
            $table->string('mobile', 30)->nullable();                       // several mobile numbers
            $table->string('email', 200)->unique();
            $table->date('driver_licence_expiry_date')->nullable();         // Fecha de caducidad del carnet de conducir
            $table->date('municipal_licence_expiry_date')->nullable();      // Fecha de cadudidad de la licencia de taxi.
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablename);
    }
}
