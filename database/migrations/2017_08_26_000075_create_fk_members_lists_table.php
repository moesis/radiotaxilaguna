<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkMembersListsTable extends Migration
{
    protected $tablename = 'members_lists';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('list_id')->references('id')->on('lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_member_id_foreign');
            $table->dropColumn('member_id');

            $table->dropForeign($this->tablename . '_list_id_foreign');
            $table->dropColumn('list_id');
        });
    }
}
