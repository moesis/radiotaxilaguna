<?php
/**
 * Registro de entrada y salida
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFkIOEntitiesTable extends Migration
{
    // Registro de entrada y salida
    protected $tablename = 'io_entities';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {

            $table->foreign('postal_code_id')->references('id')->on('postal_codes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_postal_code_id_foreign');
            $table->dropColumn('postal_code_id');
        });
    }
}
