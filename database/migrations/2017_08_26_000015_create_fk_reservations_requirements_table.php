<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkReservationsRequirementsTable extends Migration
{
    protected $tablename = 'reservations_requirements';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tablename, function (Blueprint $table) {
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('requirement_id')->references('id')->on('requirements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tablename, function ($table)
        {
            $table->dropForeign($this->tablename . '_reservation_id_foreign');
            $table->dropColumn('reservation_id');

            $table->dropForeign($this->tablename . '_requirement_id_foreign');
            $table->dropColumn('requirement_id');
        });
    }
}
