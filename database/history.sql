-- 2017.10.02
ALTER TABLE vehicles ALTER COLUMN cancellation_date SET DEFAULT NULL;
ALTER TABLE radiotaxilaguna.vehicles DROP air_conditioner;

-- 2018.06.15
ALTER TABLE notifications ADD staff_id INT UNSIGNED DEFAULT NULL;
ALTER TABLE notifications ADD INDEX notifications_staff_foreign_key (`staff_id`);
ALTER TABLE notifications ADD CONSTRAINT notifications_staff_foreign_key FOREIGN KEY (staff_id) REFERENCES staff(id);
