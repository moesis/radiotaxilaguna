<?php
/**
 * Facade for helpers.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Facades
 * @package     Radio Taxi Laguna Admin
 * @version
 * @todo
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class RtlHelpers extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Classes\RtlHelpers::class;
    }

}