<?php
/**
 * Facade for Telegram functions.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Feb 2018
 * @category    Facades
 * @package     Telegram
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Telegram extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Classes\Telegram::class;
    }


}