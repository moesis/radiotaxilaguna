<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contract_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Return the members that have the current contract type
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function members(): \Illuminate\Database\Eloquent\Relations\belongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_staff', 'member_id', 'staff_id')
                    ->withPivot([
                                    'contact_id',
                                    'contract_type_id',
                                    'contract_reference',
                                    'start_date',
                                    'end_date',
                                ])->withTimestamps();
    }

    /**
     * Return the Staff for the current contract type
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staff(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_staff', 'member_id', 'contact_id')
                    ->withPivot([
                                    'staff_id_id',
                                    'contract_type_id',
                                    'contract_reference',
                                    'start_date',
                                    'end_date',
                                ])->withTimestamps();
    }

}
