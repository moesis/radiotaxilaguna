<?php
/**
 * Language model.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Models
 * @package     Radio Taxi Laguna Core
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class Language extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name' ];

    /**
     * Return the members that speak the current language.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_languages', 'member_id', 'language_id');
    }

    /**
     * Return the field definitions for a model form.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2018
     *
     * @return mixed
     */
    public function getFieldDefinition()
    {
        return Config::get('models.language.fieldDefinitions', []);
    }

}