<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preference extends Model
{
    const DRIVER_PREFERENCE = 'DRIVER';
    const VEHICLE_PREFERENCE = 'VEHICLE';
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preferences';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'type', 'value'];

    /**
     * Return the member that have the current preference.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */
    public function members() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_preferences', 'member_id', 'preference_id');
    }

    /**
     * Return the staff that has the current preference
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */
    public function staff() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Staff::class, 'staff_preferences', 'staff_id', 'preference_id');
    }

    /**
     * Return the relationship between vehicle and preferences.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @return BelongsToMany
     */
    public function vehicles() :  \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Vehicle::class, 'vehicles_preferences', 'preference_id', 'vehicle_id')->withTimestamps();
    }

    /**
     * Scope to filter all preferences that belongs to 'driver'
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param $query
     *
     * @return mixed
     *
     */
    public function scopeDriver($query)
    {
        return $query->where('type', self::DRIVER_PREFERENCE);
    }

    /**
     * Scope to filter all preferences that belongs to 'vehicle'
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param $query
     *
     * @return mixed
     *
     */
    public function scopeVehicle($query)
    {
        return $query->where('type', self::VEHICLE_PREFERENCE);
    }
}
