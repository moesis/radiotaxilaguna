<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locality extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'localities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'postal_code'];

    /**
     * Return the relationship between Localities and Provinces
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * Return the relationship between postal codes and localities
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postalCodes()
    {
        return $this->hasMany(PostalCode::class, 'locality_id', 'id');
    }

    /**
     * Return true if the locality with $id exists in DB, otherwise return false.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return bool
     */
    public function isIn(int $id): bool
    {
        return !is_null(self::find($id));
    }

}
