<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vehicles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['plate','enrollment_date','air_conditioner','places',
                           'disabled_places','cancellation_date', 'notes',
                           'created_at','updated_at','deleted_at'];

    /**
     * Return the vehicle owner(s).
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members() : BelongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_vehicles', 'vehicle_id', 'member_id')->withTimestamps();
    }

    /**
     * Return the relationship between vehicle and car_models
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return BelongsTo
     */
    public function carModel() : BelongsTo
    {
        return $this->belongsTo(CarModel::class, 'car_model_id', 'id');
    }

    /**
     * Return the relationship between vehicle and preferences.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @return BelongsToMany
     */
    public function preferences() : BelongsToMany
    {
        return $this->belongsToMany(Preference::class, 'vehicles_preferences', 'vehicle_id', 'preference_id')->withTimestamps();
    }

}



