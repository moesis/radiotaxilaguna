<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportCard extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'members_transportcard';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transport_card', 'tc_expiration_date', 'disabled', 'disabled_date', 'reason'];

    /**
     * return the member owner of the transport card.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return HasOne
     */
    public function member() : HasOne
    {
        return $this->hasOne(Member::class, 'id', 'member_id');
    }

    /**
     * Return true if the transport card is enabled, false otherwise
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return ($this->disabled === true);
    }
}
