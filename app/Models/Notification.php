<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['licence_id', 'staff_id', 'date', 'type', 'status'];

    /**
     * Return the Licence that current model belongs to
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function licence(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Licence::class, 'id', 'licence_id');
    }

    /**
     * Set default date value as CURRENT TIME STAMP
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @param $value
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::now(new \DateTimeZone('Europe/London'))->toDateTimeString();
    }
}
