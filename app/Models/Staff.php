<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'staff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid', 'pid_expiration_date', 'dob', 'name', 'lastname',
                            'address', 'address_1', 'postal_code_id', 'phone', 'mobile',
                            'email', 'driver_licence_expiry_date', 'municipal_licence_expiry_date'];

    /**
     * Return the morphing relationship between Staff and Documents
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Document::class, 'owner');
    }

    /**
     * Return the relationship between Members and Postal Codes
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function postalCode() : \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(PostalCode::class, 'postal_code_id', 'id');
    }

    /**
     * return the members of current staff.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Member::class, 'members_staff', 'staff_id', 'member_id')->withPivot(['member_id', 'contract_type_id', 'contract_reference', 'start_date', 'end_date'])->withTimestamps();
    }

    /**
     * Return the relationship with languages
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Language::class, 'staff_languages', 'staff_id', 'language_id')->withTimestamps();
    }

    /**
     * return the contracts of the current staff
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */
    public function contract() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ContractType::class, 'members_staff', 'staff_id', 'contract_type_id')->withPivot(['member_id', 'contract_type_id', 'contract_reference', 'start_date', 'end_date'])->withTimestamps();
    }

    /**
     * Return the staff preferences.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec, 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */
    public function preferences() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Preference::class, 'staff_preferences', 'staff_id', 'preference_id')->withTimestamps();
    }
    
    /**
     * Return the member's full name
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param $value
     *
     * @return string
     */
    public function getFullNameAttribute($value) : string
    {
        return sprintf("%s %s", $this->name, $this->lastname);
    }

    /**
     * Return the full address
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param $value
     *
     * @return string
     */
    public function getFullAddressAttribute($value) : string
    {
        return sprintf("%s\n%s", $this->address, $this->address1);
    }

}
