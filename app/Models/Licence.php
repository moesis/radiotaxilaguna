<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licence extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'licences';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['municipal_licence', 'association_id', 'member_id',
                            'station_brand', 'station_model', 'station_serial',
                            'gps_brand', 'gps_model', 'gps_serial',
                            'auriga_id', 'alternative_auriga_id'];

    /**
     * Return the relationship between Licence and Associations
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function association() : \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Association::class, 'association_id', 'id');
    }

    /**
     * Return the relationship between Licence and Member
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function member() : \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }

    /**
     * Return the morphing relationship between Members and Documents
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Document::class, 'owner');
    }

    /**
     * Return the notifications for the current licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Notification::class, 'licence_id', 'id');
    }


}
