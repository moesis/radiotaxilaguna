<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Association extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'associations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'vat', 'address', 'postal_code_id', 'postal_code_id',
                            'email', 'phone_1', 'phone_2', 'mobile_1', 'mobile_2', 'website',
                            'logo', 'president', 'secretary', 'treasurer'];

    /**
     * Return the relationship between Country and Provinces
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function provinces()
    {
        return $this->belongsTo(Province::class, 'country_id', 'id');
    }

    /**
     * Return the relationship between Association and Postal Codes
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function postalCode()
    {
        return $this->belongsTo(PostalCode::class, 'postal_code_id', 'id');
    }

    /**
     * Return all members that belongs to the current association
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function members()
    {
        return $this->hasMany(Member::class, 'association_id', 'id');
    }

    /**
     * Return the acronym based on the name
     *
     * @param $value
     *
     * @return string
     */
    public function getAssociationAcronymAttribute($value) : string
    {
        $acronym = '';
        foreach (preg_split("/\s+/", $this->name) as $word) {
            if (!empty($word)) {
                $acronym .= sprintf ('%s.', strtoupper($word[0]));
            }
        }

        return $acronym;
    }

}
