<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'filename', 'notes'];

    /**
     * Models allowed to persist the relationship with Documents
     *
     * @var array
     */
    protected $allowedModels = ['licence-id' => 'licence', 'staff-id' => 'staff', 'member-id' => 'member'];

    /**
     * Return the morphing relationship
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function owner()
    {
        return $this->morphTo();
    }

    /**
     * Return the allowed models to relation with this.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return array
     */
    public function getAllowedModels() : array
    {
        return $this->allowedModels;
    }
}
