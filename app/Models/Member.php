<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid', 'pid_expiration_date', 'dob', 'name', 'lastname',
                            'address', 'address_1', 'postal_code_id', 'phone', 'mobile',
                            'email', 'telegram_alias', 'telegram_id', 'iban', 'rest_day',
                            'list', 'driver_licence_expiry_date', 'municipal_licence_expiry_date',
                            'council_date', 'communication_date'];

    /**
     * Return the morphing relationship between Members and Documents
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Document::class, 'owner');
    }

    /**
     * Return the relationship between Members and Postal Codes
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function postalCode() : \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(PostalCode::class, 'postal_code_id', 'id');
    }

    /**
     * return the vehicles of current member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vehicles() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Vehicle::class, 'members_vehicles', 'member_id', 'vehicle_id')->withTimestamps();
    }

    /**
     * Return the licence of the current member licence information.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function licence() : \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Licence::class, 'id', 'member_id');
    }

    /**
     * Return the transport card that belongs to the current member
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */
    public function transportCard() : \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany(TransportCard::class, 'member_id', 'id');
    }

    /**
     * Return the languages spoken by member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Language::class, 'members_languages', 'member_id', 'language_id');
    }

    /**
     * Return the preferences for the current member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */
    public function preferences() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Preference::class, 'members_preferences', 'member_id', 'preference_id')->withTimestamps();
    }

    /**
     * return the staff of current member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staff() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Staff::class, 'members_staff', 'member_id', 'staff_id')->withPivot(['contract_type_id', 'contract_reference', 'start_date', 'end_date'])->withTimestamps();
    }

    /**
     * Create the telegram ID
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param string $value
     */
    public function setTelegramId(string $value)
    {
        $this->telegram_id = sprintf ('@RTL%s', $this->municipal_licence);
    }

    /**
     * Return the member's full name
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param $value
     *
     * @return string
     */
    public function getFullNameAttribute($value) : string
    {
        return sprintf("%s %s", $this->name, $this->lastname);
    }

    /**
     * Return the full address
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param $value
     *
     * @return string
     */
    public function getFullAddressAttribute($value) : string
    {
        return sprintf("%s\n%s", $this->address, $this->address1);
    }

}
