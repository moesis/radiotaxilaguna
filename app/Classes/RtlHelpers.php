<?php
/**
 * Helpers used in the Radio Taxi Laguna Application.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Helpers
 * @package     Radio Taxi Laguna Admin
 * @version     1.0
 * @todo
 */

namespace App\Classes;

use App\Mail\UserMailable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;
use Carbon\Carbon;
use DateTimeZone;

class RtlHelpers
{
    const NAMESPACE_SEPARATOR = '\\';

    // Date formats.
    const DATE_WITH_SLASH = 'd/m/Y';
    const DATE_WITH_DASH = 'Y-m-d';
    const HUMAN_READABLE_SHORT_DATE = 'd-m-Y';
    const HUMAN_READABLE_LONG_DATE = 'l d \d\e F \d\e Y';

    /**
     * Generate a password string with ·$length and symbols if $useSymbols is true.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param      $length
     * @param bool $useSymbols
     *
     * @return string
     * @todo
     */
    public function generatePassword($length, $useSymbols = false)
    {
        $lowercase = "qwertyuiopasdfghjklzxcvbnm";
        $uppercase = "ASDFGHJKLZXCVBNMQWERTYUIOP";
        $numbers = "1234567890";
        $specialcharacters = "{}[];:,./<>?_+~!@#";
        $randomCode = "";

        mt_srand(crc32(microtime()));
        $max = strlen($lowercase) - 1;
        for ($x = 0; $x < abs($length / 3); $x++) {
            $randomCode .= $lowercase{mt_rand(0, $max)};
        }

        $max = strlen($uppercase) - 1;
        for ($x = 0; $x < abs($length / 3); $x++) {
            $randomCode .= $uppercase{mt_rand(0, $max)};
        }

        if ($useSymbols) {
            $max = strlen($specialcharacters) - 1;
            for ($x = 0; $x < abs($length / 3); $x++) {
                $randomCode .= $specialcharacters{mt_rand(0, $max)};
            }
        }

        $max = strlen($numbers) - 1;
        for ($x = 0; $x < abs($length / 3); $x++) {
            $randomCode .= $numbers{mt_rand(0, $max)};
        }

        return str_shuffle($randomCode);
    }

    /**
     * Show and log the message with extra information. In addition,
     * send an email to the specific member team in order to communicate
     * the situation and based on $stop parameter put the application
     * in maintenance mode in order to prevent damages
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param       $level
     * @param       $message
     * @param       $class
     * @param bool  $stop
     *
     * @todo
     */
    public function log($level, $message, $class = null, $stop = false)
    {
        $debugInfo = debug_backtrace();
        $msg = sprintf('%s|%s(%s)|%s',
            substr($debugInfo[0]['file'], strpos($debugInfo[0]['file'], Config::get('radiotaxilaguna.setup.app.path'))),
            $debugInfo[1]['function'],
            $debugInfo[0]['line'],
            $message);

        switch ($level) {
            case Logger::INFO:
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $class->info($message);
                }
                Log::info($msg);
                break;
            case Logger::WARNING:
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $class->warn($message);
                }
                Log::warning($msg);
                break;
            case Logger::ERROR:
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $class->error($message);
                }
                Log::alert($msg);
                $this->sendMessage($msg);
                break;
            case Logger::ALERT:
                Log::alert($msg);
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $this->sendMessage($msg);
                }
                break;
            case Logger::CRITICAL:
                $this->alert($msg);
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $this->critical($msg);
                }
                $this->sendMessage();
                if ($stop) {
                    App::isDownForMaintenance();
                }
                // Maybe Stop the application.
                break;
            case Logger::EMERGENCY:
                $this->alert($msg);
                if (App::runningInConsole() && is_a($class, \Illuminate\Console\Command::class)) {
                    $class->emergency($msg);
                }
                $this->sendMessage();
                if ($stop) {
                    App::isDownForMaintenance();
                }
                break;
        }
    }

    /**
     * Send a specific message to specific team member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     */
    public function sendMessage($message)
    {
        Mail::to(Config::get('radiotaxilaguna.team.administrator'))
            ->send(new UserMailable(['title' => 'Mensaje de la aplicación.', 'view' => 'email.alert', 'message' => $message]));
    }

    /**
     * Load rules from config file. The name must be the same as model.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param string  $model
     * @param $request
     *
     * @return array
     */
    public function loadRules(string $model, $request = null): array
    {
        $namespaceParts = explode(self::NAMESPACE_SEPARATOR, $model);
        $modelName = strtolower(array_pop($namespaceParts));

        $rules = Config::get(sprintf('models.%s.rules', $modelName), []);
        $messages = Config::get(sprintf('models.%s.messages', $modelName), []);
        $attributes = Config::get(sprintf('models.%s.attributes', $modelName), []);

        if (!is_null($request)) {
            // We need to check if rules has parameters in form @name in order to replace them
            foreach ($rules as $index => $rule) {
                if (str_contains($rule, '@')) {
                    // Remove the @
                    preg_match_all('/\@(.*)\s?\S?,?/si', $rule, $matches);
                    // Look for any parameter in $request object and try to replace with right value
                    if (gettype($request) === 'array') {
                        $request = collect($request);
                    }
                    if ($request->has($matches[1])) {
                        $rules[$index] = str_replace($matches[0][0], ','.$request->get($matches[1][0]), $rule);
                    } else {
                        $rules[$index] = str_replace($matches[0][0], '', $rule);
                    }
                }
            }
        }

        return ['rules' => $rules, 'messages' => $messages, 'attributes' => $attributes];
    }

    /**
     * Load customized messages for given Model.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $model
     *
     * @return mixed
     */
    public function loadMessages(string $model)
    {
        $namespaceParts = explode(self::NAMESPACE_SEPARATOR, $model);
        $modelName = strtolower(array_pop($namespaceParts));

        return Config::get(sprintf('models.%s.messages', $modelName), []);
    }

    /**
     * Return true if the ICCID number is valid, false otherwise.
     *
     * @see https://www.justinsilver.com/technology/sim-card-iccid-validation/
     *
     * @param $number
     *
     * @return bool
     */
    public function isValidLuhn(string $number) : bool
    {
        $sumTable = [
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            [0, 2, 4, 6, 8, 1, 3, 5, 7, 9],
        ];
        $sum = 0;
        $flip = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$number[$i]];
        }

        return $sum % 10 === 0;
    }

    /**
     * Transform a date as string into a string or carbon object based on $asCarbon parameter.
     *toDateTimeString
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @param null|string   $date
     * @param string        $format
     * @param string        $timeZone
     * @param bool          $asCarbon
     *
     * @return null|string|static
     */
    public function convertDateFromString($date, string $format = 'd/m/Y', string $timeZone = 'Europe/London', bool $asCarbon = false)
    {
        $transformedDate = null;
        if (!empty($date)) {
            $transformedDate = Carbon::createFromFormat($format, $date, new DateTimeZone($timeZone));
        }

        return  $asCarbon ? $transformedDate : $transformedDate->toDateString();
    }

    /**
     * Check the personal ID letter and if the given letter is wrong
     * then replace the given by the correct.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param string $pId
     *
     * @return array   The personalId and the correct letter
     *
     */
    public function checkPersonalId(string $pId)
    {
        // Remove all . and -
        $personalId = str_replace(['.', '-'], '', $pId);

        // Check the letter if it is wrong, change to right letter and check
        $letterToResponse = '';
        $letter = strtoupper(substr($personalId, -1, 1));
        if (!ctype_alpha($letter)){
            $letter = '';
        }
        $personalId = substr($personalId, 0, 8);

        // if it's a NIE we must change the first letter by 0, 1 ó 2 depending on X, Y or Z.
        $personalId = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $personalId);

        $letterFromCalc = strtoupper(substr("TRWAGMYFPDXBNJZSQVHLCKEO", intval($personalId) % 23, 1));
        $newLicence = $personalId;
        $letterToResponse = '';
        if ($letter !== $letterFromCalc) {
            $newLicence = $personalId . $letterFromCalc;
            $letterToResponse = $letterFromCalc;
        }
        return ['personal-licence' => $personalId, 'letter' => $letterToResponse];
    }

    /**
     * Return the date as string or null if no date is provided.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param $date
     * @param string $format
     * @param string $timeZone
     * @param bool $asCarbon
     *
     * @return null
     */
    public function dateOrNull($date, string $format = null, string $timeZone = 'Europe/London', bool $asCarbon = false)
    {
        return !empty($date) ? RtlHelpers::convertDateFromString($date, (is_null($format) ? $this->setCorrectFormatDate($date) : $format), $timeZone, $asCarbon) : null;
    }

    /**
     * Set the appropriate date format
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec 2017
     *
     * @param string $date
     *
     * @return null|string
     */
    public function setCorrectFormatDate($date)
    {
        $format = null;
        if (str_contains($date, ['/'])){
            $format = self::DATE_WITH_SLASH;
        } else {
            if (str_contains($date, ['-'])) {
                $format = self::DATE_WITH_DASH;
            }
        }

        return $format;
    }

    /**
     * Return the date given, or today if null is given, formatted as $format set.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @param Carbon $date
     * @param string $format
     *
     * @return string
     */
    private function getDate(Carbon $date, string $format)
    {
        Carbon::setLocale('es');

        if ($date) {
            return $date->format($format);
        }

        return Carbon::now(new \DateTimeZone('Europe/London'))->format($format);
    }

    /**
     * Return the short version of given date, or today if $date is null
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @param Carbon|null $date
     *
     * @return string
     */
    public function getShortDate(Carbon $date = null)
    {
        return $this->getDate($date, self::HUMAN_READABLE_SHORT_DATE);
    }

    /**
     * Return the long version of given date, or today if $date is null.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @param Carbon|null $date
     *
     * @return string
     */
    public function getLongDate(Carbon $date = null)
    {
        return $this->getDate($date, self::HUMAN_READABLE_SHORT_DATE);
    }

}