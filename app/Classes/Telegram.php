<?php

namespace App\Classes;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundation;

/**
 * Class to interact with Telegram API.
 *
 * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version Feb 2018
 *
 * @package App\Http\Controllers
 */
class Telegram
{
    /**
     * Telegram Bot token
     *
     * @var string
     */
    protected $token;

    /**
     * URL to use Telegram API
     *
     * @var string
     */
    protected $url;

    /**
     * Telegram Connection handle.
     *
     * @var \GuzzleHttp\Client
     */
    protected $telegramClient;

    /**
     * TelegramController constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setToken(Config::get('telegram.bot.token', ''));
        $this->telegramClient = null;

        if (empty($this->getToken())) {
            throw new \Exception('Missing Telegram token.');
        }

        $this->setUrl(Config::get('telegram.url', ''));
        if (empty($this->getUrl())) {
            throw new \Exception('Missing Telegram API URL.');
        }
    }

    /**
     * return the JSON response for the getUpdates API call
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @param int $offset      Identifier of the first update to be returned.
     *                         Must be greater by one than the highest among the
     *                         identifiers of previously received updates.
     *                         By default, updates starting with the earliest
     *                         unconfirmed update are returned. An update is
     *                         considered confirmed as soon as getUpdates is
     *                         called with an offset higher than its update_id.
     *                         The negative offset can be specified to retrieve
     *                         updates starting from -offset update from the end
     *                         of the updates queue. All previous updates will forgotten.
     * @param int $limit       Limits the number of updates to be retrieved.
     *                         Values between 1—100 are accepted. Defaults to 100.
     * @param int $timeout     Timeout in seconds for long polling.
     *                         Defaults to 0, i.e. usual short polling
     *
     * @return Collection
     * @throws \Exception
     */
    public function getUpdates(int $offset = 0, int $limit = 0, int $timeout = 0): Collection
    {
        $this->telegramClient = new Client();
        $url = $this->composeURL(TelegramConstants::GETUPDATES);
        $response = $this->telegramClient->get($url);
        $content = json_decode($response->getBody()->getContents(), true);

        if (!$content['ok']) {
            throw new \Exception('There was a problem processing the Telegram message');
        }

        $results = [];
        // Process the results.
        foreach ($content['result'] as $items) {
            foreach ($items as $key => $value) {
                $user = [
                    'id'       => null,
                    'username' => null,
                ];
                if ($key == 'message' && (!$value['from']['is_bot'])) {
                    if (array_key_exists('id', $value['from'])) {
                        $user['id'] = $value['from']['id'];
                    }
                    if (array_key_exists('username', $value['from'])) {
                        $user['username'] = $value['from']['username'];
                    } else {
                        $name = $lastname = '';
                        // To notify that the user has not set the username.
                        if (array_key_exists('first_name', $value['from'])) {
                            $name = $value['from']['first_name'];
                        }
                        if (array_key_exists('last_name', $value['from'])) {
                            $lastname = $value['from']['last_name'];
                        }
                        $user['name'] = $name . ' ' . $lastname;
                    }
                }
            }
            $results[] = $user;
        }

        $telegramUsers = collect([]);
        if (!empty($results)) {
            $telegramUsers = collect($results);
            $telegramUsers = $telegramUsers->unique('id');
        }

        return $telegramUsers;
    }

    /**
     * Send a message to a user using Telegram API, return
     * TELEGRAM_MESSAGE_SENT (200) if the message has been
     * sent successfully of TELEGRAM_MESSAGE_FAILED(500)
     * otherwise
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param int $telegramId
     * @param string $message
     *
     * @return int
     */
    public function sendMessage(int $telegramId, string $message)
    {
        $this->telegramClient = new Client();
        $url = $this->composeURL(TelegramConstants::SENDMESSAGE);
        $params = [
            'query' => [
                'chat_id' => $telegramId,
                'text'    => $message,
            ],
        ];
        $response = $this->telegramClient->post($url, $params);

        if ($response->getStatusCode() !== HttpFoundation::HTTP_OK) {
            return TelegramConstants::TELEGRAM_MESSAGE_FAILED;
        }

        return TelegramConstants::TELEGRAM_MESSAGE_SENT;
    }

    /**
     * Return the composed url to use the entrypoint
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param string $command
     *
     * @return string
     */
    protected function composeURL(string $command = 'getMe'): string
    {
        return sprintf("%sbot%s/%s", $this->getUrl(), $this->getToken(), $command);
    }

    /**
     * Return Telegram Token
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Set the Telegram Token
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return Telegram
     */
    public function setToken(string $token): Telegram
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Return the Telegram URL
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Set the Telegram URL
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param string $url
     *
     * @return Telegram
     */
    public function setUrl(string $url): Telegram
    {
        $this->url = $url;

        return $this;
    }


}
