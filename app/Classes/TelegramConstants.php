<?php

namespace App\Classes;

class TelegramConstants
{
    /**
     * Constants with the API commands.
     */
    const GETME = 'getMe';
    const GETUPDATES = 'getUpdates';
    const SENDMESSAGE = 'sendMessage';

    const NO_TELEGRAM_ACCOUNT = 404;
    const TELEGRAM_MESSAGE_SENT = 200;
    const TELEGRAM_MESSAGE_FAILED = 500;
}