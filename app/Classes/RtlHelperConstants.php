<?php

namespace App\Classes;

class RtlHelperConstants
{
    const NAMESPACE_SEPARATOR = '\\';

    // Date formats.
    const DATE_WITH_SLASH = 'd/m/Y';
    const DATE_WITH_DASH = 'Y-m-d';
    const HUMAN_READABLE_SHORT_DATE = 'd-m-Y';
    const HUMAN_READABLE_LONG_DATE = 'l d \d\e F \d\e Y';

}