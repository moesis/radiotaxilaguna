<?php

namespace App\Console\Commands;

use App\Classes\TelegramConstants;
use App\Facades\RtlHelpers;
use App\Facades\Telegram;
use App\Http\Controllers\DashboardController;
use App\Mail\UserMailable;
use App\Models\Notification;
use App\Repositories\MemberRepository;
use App\Repositories\StaffRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;
use Illuminate\Console\Command;

class CheckExpiryDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:CheckExpiryDates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the expiry dates for all members and notify them through Telegram.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Process the Telegram Users and Syncrhro with members table
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @throws \Exception
     */
    public function handle()
    {
        $start = microtime(true);

        $email = Config::get('radiotaxilaguna.company.email');
        $operationsLog = [];

        $memberRepository = new MemberRepository();
        $staffRepository = new StaffRepository();

        $expirations = $memberRepository->getExpirations();
        $staffExpirations = $staffRepository->getExpirations();

        $dashController = new DashboardController();
        $expirations = [
            'membersExpirations' => $dashController->calculateMembersExpiration($expirations),
            'staffExpirations'   => $dashController->calculateStaffExpiration($staffExpirations),
        ];

        $today = Carbon::now(new \DateTimeZone('Europe/London'));
        foreach ($expirations as $expirationName => $expirationsDates) {
            foreach ($expirationsDates as $data) {
                switch ($expirationName) {
                    case 'membersExpirations':
                        $status = TelegramConstants::TELEGRAM_MESSAGE_FAILED;
                        $readyToSend = true;
                        foreach ($data as $record) {
                            if ($record['telegram_id']) {
                                // we will only send the message if one third of the number of days for its expiration has elapsed
                                $lapse = intval(Config::get('radiotaxilaguna.expirations.' . (snake_case($record['type']) . '_expiration_date')));

                                if (!is_null($record['last-date'])) {
                                    $lastNotification = Carbon::createFromFormat('Y-m-d H:i:s', $record['last-date'], 'Europe/London');
                                    $nextDate = $lastNotification->addDays($lapse);

                                    // send the message if telegram_id is available.
                                    if ($nextDate->gt($today)) {
                                        $readyToSend = false;
                                    }
                                }

                                if ($readyToSend) {
                                    $status = Telegram::sendMessage($record['telegram_id'], $record['message']);
                                    $this->warn('Telegram: ' . $record['message']);
                                    sleep(1);

                                    // Save the data into notifications
                                    $notification = new Notification([
                                        'licence_id' => $record['id'],
                                        'type'       => $record['type'],
                                        'status'     => $status,
                                    ]);
                                    $notification->save();
                                }
                            } else {
                                $status = TelegramConstants::NO_TELEGRAM_ACCOUNT;
                            }

                            // Update the record info.
                            $record['status'] = $status;
                            $record['last-date'] = RtlHelpers::getShortDate($today);
                        }
                    break;
                    case 'staffExpirations':
                        foreach ($data as $record) {

                            if ($record['type'] == 'staff-pid') {

                                foreach ($record['licence'] as $licenceId => $licence) {
                                    if ($record['email']) {
                                        // only send an email to the user with hidden copy to the Radio Taxi administrator
                                        $this->info('sending email...');
                                        Mail::to($record['email'])
                                            ->bcc(Config::get('radiotaxilaguna.team.developer1'))
                                            ->send(new UserMailable([
                                                'title' => 'Mensaje de aviso de cadudidad en documento.',
                                                'from-email' => 'app@radiotaxilaguna.com',
                                                'view' => 'email.telegram.expirynotification',
                                                'message' => $record['message'],
                                            ]));
                                    }

                                    // Save the data into notifications
                                    $notification = new Notification([
                                        'licence_id' => $licenceId,
                                        'staff_id' => $record['id'],
                                        'type' => $record['type'],
                                        'status' => 200,
                                    ]);
                                    $this->warn('Saving the data... ' . implode(PHP_EOL, $notification->toArray()));
                                    $notification->save();
                                }

                            } else {

                                foreach ($record['licence'] as $licenceId => $licence) {

                                    if (!is_null($licence['telegram_id'])) {

                                        $readyToSend = true;
                                        // we will only send the message if one third of the number of days for its expiration has elapsed
                                        $lapse = intval(Config::get('radiotaxilaguna.expirations.' . (snake_case($record['type']) . '_expiration_date')));

                                        if (!is_null($record['last-date'])) {
                                            $lastNotification = Carbon::createFromFormat('Y-m-d H:i:s', $record['last-date'], 'Europe/London');
                                            $nextDate = $lastNotification->addDays($lapse);

                                            // send the message if telegram_id is available.
                                            if ($nextDate->gt($today)) {
                                                $readyToSend = false;
                                            }
                                        }

                                        if ($readyToSend) {
                                            $status = Telegram::sendMessage($licence['telegram_id'],
                                                $record['message'] . ' (Personal Asalariado ' . $record['name'] . ')');

                                            $status = 200;
                                            $this->warn('Telegram: ' . $record['message'] . ' (Personal Asalariado ' . $record['name'] . ')');
                                            // Save the data into notifications
                                            $notification = new Notification([
                                                'licence_id' => $licenceId,
                                                'staff_id' => $record['id'],
                                                'type' => $record['type'],
                                                'status' => $status,
                                            ]);
                                            $this->warn('Saving the data... ' . implode(PHP_EOL,
                                                    $notification->toArray()));
                                            $notification->save();
                                        }
                                    }
                                }
                            }
                        }
                    break;

                }
            }
        }

        /*Mail::to(Config::get('radiotaxilaguna.team.developer1'))
            ->send(new UserMailable([
                'title'       => 'Log de notificaciones de Telegram',
                'from-email'  => 'app@radiotaxilaguna.com',
                'view'        => 'email.telegram.expirynotifications',
                'operations' => $expirations,
            ]));*/

        RtlHelpers::log(Logger::INFO, sprintf('The command has take %.2f seconds', microtime(true) - $start), $this);
    }
}