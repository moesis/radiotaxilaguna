<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class UpdatePermission extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:updatePermission
                                { permission        : permission name }
                                { new-permission    : new name for persmission }
                                { --runAs=          : User that run the command. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rename a existing permission';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $permission = $this->argument('permission') ? $this->argument('permission') : '';
        $newPermission = $this->argument('new-permission') ? $this->argument('new-permission') : '';

        $pRepository = new PermissionRepository();
        $this->info('Validating the provided data...');
        if ($pRepository->validate([
            'name'     => $permission,
            'new-name' => $newPermission,
        ])
        ) {
            foreach ($pRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide new credentials.', $this);

            return;
        }

        try {
            // Trying to login with
            if (!$this->login($sysUser)) {
                RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

                return;
            }

            $this->info('Updating the permission...');
            $permissionResult = $pRepository->update($permission, $newPermission);
            $this->logout();
            $this->info('Done! The permission has been updated sucessfully');

        } catch (\Exception $e) {
            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
