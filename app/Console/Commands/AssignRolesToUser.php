<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class AssignRolesToUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:assignRolesToUser 
                                { email     : User email },
                                { roles*    : Role names }
                                { --runAs=  : User to be authenticated }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign one or more roles to the user.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $roles = $this->argument('roles');

        // Only use the roles that exists in database.
        $uRepo = new UserRepository();
        $rRepo = new RoleRepository();

        $dbRoles = $rRepo->getExistingRoles($roles);
        $user = $uRepo->findByEmail($email);

        if ($dbRoles->isEmpty()) {
            RtlHelpers::log(Logger::ERROR, 'You must specify at least one existing role name to run the command', $this);

            return;
        }

        if ($user == null) {
            RtlHelpers::log('The specified user cannot be found on our databases');

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = $uRepo->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            // Trying to login with
            if (!$this->login($sysUser)) {
                RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

                return;
            }


            $uRepo->assignRole($user, $dbRoles);
            RtlHelpers::log(Logger::INFO, sprintf('The role(s) [%s] have been assigned to the user %s', implode(',', $dbRoles->pluck('name')->toArray()), $email), $this);
            $this->logout();

        } catch (\Exception $e) {

            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
