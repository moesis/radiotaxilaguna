<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\BaseRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;


class RemoveRole extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:removeRole 
                                { name    : Role name }
                                { --runAs= : User that will run the command. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Role.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        $rRepository = new RoleRepository();

        $this->info('Validating new role name...');
        if ($rRepository->validate(['name' => $name], BaseRepository::UPDATE_RULES)) {
            foreach ($rRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        // Trying to login with
        if (!$this->login($sysUser)) {
            RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            $this->info('Creating the role');
            $rRepository->delete($name);
            $this->logout();

            $this->info(sprintf('The role has been removed successfully'));
        } catch (\Exception $e) {
            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
