<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Facades\Telegram;
use App\Mail\UserMailable;
use App\Models\Member;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;
use Illuminate\Console\Command;

class SyncTelegram extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:SyncTelegram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize the Telegram Bot RadioTaxiLaguna Users with the Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Process the Telegram Users and Syncrhro with members table
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @throws \Exception
     */
    public function handle()
    {
        $start = microtime(true);

        $dbPrefix = Config::get('radiotaxilaguna.prefixes.application');
        $telegramPrefix = Config::get('radiotaxilaguna.prefixes.telegram');

        $email = Config::get('radiotaxilaguna.company.email');
        $operationsLog = [];

        // Takes the Bot RadioTaxiLagunaBot users. (Collection)
        $users = Telegram::getUpdates();
        if (!$users->isEmpty()) {

            foreach ($users as $user) {

                $telegramUser = $user['username'];

                if (substr($telegramUser, 0, strlen($telegramPrefix)) === $telegramPrefix) {

                    $user['username'] = str_replace($telegramPrefix, $dbPrefix, $telegramUser);
                    if (!array_key_exists('name', $user)){
                        $operationsLog[$user['username']][] = 'Nombre no configurado.';
                    }

                    $member = Member::where('telegram_alias', $user['username'])->first();
                    if ($member) {
                        if (is_null($member->telegram_id)) {
                            // If exists update.
                            $member->telegram_id = $user['id'];
                            if ($member->save()) {
                                $operationsLog[$user['username']][] = 'Datos actualizados satisfactoriamente.';
                            } else {
                                $operationsLog[$user['username']][] = 'Ha fallaado la actualización.';
                            }
                        } else {
                            $operationsLog[$user['username']][] = 'Telgram ID existente.';
                        }
                    } else {
                        // Add to errors array
                        $operationsLog[$user['username']][] = 'Licencia no existente en la base de datos.';
                    }
                } else {
                    $message = '';
                    if (array_key_exists('name', $user)) {
                        $message .= ' ' . $user['name'];
                    }
                    $operationsLog['DESCONOCIDO'][] = 'Datos no válidos: ' . $message;

                    unset($user);
                }
            }
        }

        Mail::to(Config::get('radiotaxilaguna.team.developer'))->send(new UserMailable(['title' => 'Log de comando Telegram', 'from-email' => 'app@radiotaxilaguna.com', 'view' => 'email.telegram.newmembers', 'operations' => $operationsLog]));
        RtlHelpers::log(Logger::INFO, sprintf('The command has take %.2f seconds', microtime(true) - $start), $this);
    }
}
