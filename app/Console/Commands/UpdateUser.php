<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Mail\UserMailable;
use App\Repositories\BaseRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;

class UpdateUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:updateUser 
                                { email       : Email that identifies the user }
                                { --username= : User name }
                                { --password= : Password that will be used as user password. if it is empty the password will be generated and will be send to the user email.}
                                { --runAs=    : User that run the command. }
                                { --no-mail   : Do not send notification mail if the flag is present }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user in the system and send an email to owner. If password is not provided the command generate ones';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = $this->option('username') ? $this->option('username') : '';
        $password = $this->option('password') ? $this->option('password') : '';

        $data = array_filter([
            'user'     => $user,
            'email'    => $email,
            'password' => $password,
        ]);

        $uRepository = new UserRepository();

        $this->info('Validation the provided data...');
        if ($uRepository->validate($data, BaseRepository::UPDATE_RULES)) {
            foreach ($uRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        if (!$this->option('runAs')) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        // Trying to login with
        if (!$this->login($sysUser)) {
            RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            $this->info('Updating the user...');
            $user = $uRepository->update($email, $user, $password);

            if (!$this->option('no-mail')) {
                // Send an email to the user in order to comunicate the new password.
                $this->info('Sending the email to the user...');
                Mail::to($email)->send(new UserMailable(['title' => 'Actualización del usuario', 'view' => 'email.updateuser', 'user' => $data['email']]));
            }
            $this->logout();

            RtlHelpers::log(Logger::INFO, 'Done! The user has been updated sucessfully', $this);
        } catch (\Exception $e) {
            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }


}
