<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Mail\UserMailable;
use App\Repositories\BaseRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;

class ActivateUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:activateUser 
                                { email             : Email that identifies the user }
                                { --action=activate : Activate or deactivate the user. To activate the user use \'activate\'. To deactivate \'deactivate\'. }
                                { --runAs=          : User that run the command. }
                                { --no-email        : if present send an email to user. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate or deactivate an user.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $action = strtolower($this->option('action'));

        $uRepository = new UserRepository();

        $this->info('Checking the provided data...');
        if ($uRepository->validate([
            'email'  => $email,
            'action' => $action,
        ], BaseRepository::UPDATE_RULES)
        ) {

            foreach ($uRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        // Trying to login with
        if (!$this->login($sysUser)) {
            RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            $this->info(sprintf('%s the user...', ucfirst($action)));
            $user = $uRepository->activate($email, $action);

            // Send an email to the user in order to comunicate the new password.
            if ($this->option('-no-email')) {
                $this->info('Sending the email to the user...');
                Mail::to($email)->send(new UserMailable(['title' => 'Activación del usuario', 'view' => 'email.updateuser', 'user' => $email]));
            }

            $this->logout();

            RtlHelpers::Log(Logger::INFO, sprintf('The user has been %sed successfully', $action), $this);
        } catch (\Exception $e) {

            $this->error($e->getMessage());

            return;
        }
    }
}
