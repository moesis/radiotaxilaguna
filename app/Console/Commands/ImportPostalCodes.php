<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Models\Country;
use App\Models\Locality;
use App\Models\PostalCode;
use App\Models\Province;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Monolog\Logger;

class ImportPostalCodes extends BaseCommand
{
    const URL = 'http://www.solosequenosenada.com/misc/postalcodes/cp%d.php';
    const SPAIN = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:ImportPostalCodes 
                                { province      : Province code },
                                { path          : Path where the files are }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import postal codes from file downloaded from https://www.codigospostales.com';

    /**
     * Province Code.
     *
     * @var integer
     */
    protected $province;

    /**
     * Province name
     *
     * @var string
     */
    protected $provinceName;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);

        $this->province = $this->argument('province');
        $path = $this->argument('path');

        $fullpath = sprintf('%s%s%s*.txt', $path, DIRECTORY_SEPARATOR, $this->province);
        $mandatoryFile = sprintf('%s%s%s', $path, DIRECTORY_SEPARATOR, Config::get('radiotaxilaguna.postal-codes.cities'));
        // Check if file exists and is readable
        if (!file_exists($mandatoryFile) && is_readable($mandatoryFile)) {
            RtlHelpers::log(Logger::ERROR, 'Cannot find the mandatory files on the path given ', $this, true);

            return;
        }

        $this->cleanAllRelatedTables();

        try {
            $cities = Collect($this->loadProvinces($mandatoryFile));
            $localities = $this->importLocalitiesFromWebsite();
            $files = Collect(glob($fullpath));
            $files->each(function ($value, $keu) use ($cities, $localities) {
                if ($cities->contains(substr(basename($value), 0, 3))) {
                    RtlHelpers::log(Logger::INFO, sprintf('Importing postal codes from [%s]', $value), $this);
                    // Read and import the file contents.
                    $this->loadPostalCodes($value, $localities);
                    $this->info('');
                }
            });


        }
        catch (\Exception $e) {
            RtlHelpers::log(Logger::INFO, sprintf('An error has been occurred.%s%s', PHP_EOL, $e->getMessage()), $this, true);
        }


        RtlHelpers::log(Logger::INFO, sprintf('The command has take %.2f seconds', microtime(true) - $start), $this);

        return 0;
    }

    public function cleanAllRelatedtables()
    {
        $this->info('Truncating the tables...');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Province::query()->truncate();
        Locality::query()->truncate();
        PostalCode::query()->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->info('Done!');
    }

    /**
     * Import the localities scrapping the web http://www.solosequenosenada.com/misc/postalcodes/
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @return array
     */
    public function importLocalitiesFromWebsite()
    {
        $pageContent = file_get_contents(sprintf(self::URL, $this->province));
        $re = '/^|(\d{5})\s+((\w|\s|-|,|ñ|Ñ|(|[|(a-zA-Z]*)])*\s+[a-zA-Z ()Ññ,.-]+)/mi';
        preg_match_all($re, $pageContent, $matches, PREG_SET_ORDER, 0);

        $province = Province::find($this->province);
        $localities = [];
        $lines = array_filter($matches, function ($value) {
            return $value !== '' && isset($value[0]) && $value[0] !== '';
        });
        $bar = $this->output->createProgressBar(count($lines));
//        if (count($lines) > 0) {
//            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//            Locality::query()->truncate();
//            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//        }
        $locality = null;
        foreach ($lines as $line) {

            try {
                $exists = Locality::where('name', trim($line[2]))->first();
                if ($exists) {
                    // If not exists we'll add to the global array to save the correct ID in postal code process.
                    $localities[$line[1]] = [
                        'id' => $locality->id,
                        'name' => trim($line[2]),
                    ];
                } else {
                    $locality = new Locality();
                    $locality->name = trim($line[2]);
                    $locality->province()->associate($province);

                    // Persist the locality
                    $locality->saveOrFail();
                    // Add to global array to be used later on postal code insertions
                    $localities[$line[1]] = [
                        'id' => $locality->id,
                        'name' => trim($line[2]),
                    ];
                }
            }
            catch (\Exception $e) {
                $this->error($e);
                die();
            }
            $bar->advance();
        }

        return $localities;
    }

    /**
     * Return the filenames what we need to read to get the postal codes.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param string $file
     *
     * @return \Illuminate\Support\Collection
     */
    public function loadProvinces(string $file): Collection
    {
        $lines = (new Collection(file($file, FILE_IGNORE_NEW_LINES)))->sort();

        return $lines->map(function ($value, $key) {
            if (starts_with($value, $this->province)) {
                preg_match_all('/^((\d{2})(\d|x{1}))(.*)/', $value, $m);
                if ($m[3][0] == 'x') {
                    // This is the province... We need to persist into DB
                    $province = new Province();
                    if (!$province->isIn($m[2][0])) {
                        $province->id = $m[2][0];
                        $province->name = $m[4][0];
                    } else {
                        $province = $province->find($m[2][0]);
                    }
                    // By default countries:id:1 is Spain
                    $province->country()->associate(Country::find(self::SPAIN));
                    $province->saveOrFail();
                    $this->provincenName = $m[4][0];
                }

                return $m[1][0];
            }
        })->filter(function ($value) {
            if (!is_null($value)) {
                return $value;
            }
        });
    }

    /**
     * Persist all postal codes in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param string $file
     */
    public function loadPostalCodes(string $file, array $localities)
    {
        $this->info('Inserting the Postal Codes.');
        $lines = new Collection(file($file, FILE_IGNORE_NEW_LINES));
        $bar = $this->output->createProgressBar($lines->count());
        $lines->map(function ($value) use ($bar, $localities) {

            $record = explode(':', $value);

            // Persist the postal code.
            $postalCode = new PostalCode();
            $postalCode->postal_code = trim($record[0]);
            $postalCode->name = trim($record[1]);

            // Look for the localityId
            if (isset($localities[$record[0]])) {
                $postalCode->locality_id = $localities[$record[0]]['id'];
            } else {
                $postalCode->locality_id = (Locality::where('province_id', $this->province)->orderBy('id')->first(['id'])['id']);
            }

            try {
                $postalCode->saveOrFail();
            }
            catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $bar->advance();
        });
    }

}
