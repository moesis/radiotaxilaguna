<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class GivePermissionsToUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:givePermissionsToUser 
                                { email         : User email },
                                { permissions*  : Permissions names }
                                { --runAs=      : User to be authenticated }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign one or more permissions to user.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $permissions = $this->argument('permissions');

        // Only use the Permissions that exist in database.
        $uRepo = new UserRepository();
        $pRepo = new PermissionRepository();

        $dbPermissions = $pRepo->getExistingPermissions($permissions);
        $dbUser = $uRepo->findByEmail($email);

        if ($dbPermissions->isEmpty()) {
            RtlHelpers::log(Logger::ERROR, 'You must specify at least one existing permission name to run the command', $this);

            return;
        }

        if ($dbUser == null) {
            RtlHelpers::log(Logger::ERROR, sprintf('The user [%s] cannot be found on our databases.', $user), $this);

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = $uRepo->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            // Trying to login with
            if (!$this->login($sysUser)) {
                RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

                return;
            }
            $uRepo->assignPermissionsTo($dbUser, $dbPermissions);
            RtlHelpers::log(Logger::INFO, sprintf('The permission(s) %s have been assigned to the user %s', implode(',', $dbPermissions), $email), $this);
            $this->logout();

        } catch (\Exception $e) {

            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
