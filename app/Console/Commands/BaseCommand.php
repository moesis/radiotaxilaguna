<?php
/**
 * Base Command. All common methods of Commands.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     May 2017
 * @catergory   Commands
 * @package     App\Commands
 *
 * @todo
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class BaseCommand extends Command
{

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Login with specific user. The password is asked by prompt
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param $user
     *
     * @return bool
     * @todo
     */
    public function login($user)
    {
        $password = $this->secret('Enter the password for user ' . $user->email);
        if (!Auth::validate(['email' => $user->email, 'password' => $password])) {
            return false;
        }

        // Todo: We must add check security issues in order to se if the user is able, or not, to run the command...
        // We must check if the user has the right permission to do that.

        return Auth::loginUsingId($user->id);
    }

    /**
     * Logout the user
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @todo
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
    }

}