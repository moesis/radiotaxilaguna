<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class RemoveRolesFromUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:removeRolesFromUser 
                                { email   : User email },
                                { roles*  : Role name }
                                { --runAs= : User to be authenticated }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the roles from user.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $roles = $this->argument('roles');

        // Only use the roles that exists in database.
        $uRepo = new UserRepository();
        $rRepo = new RoleRepository();

        $dbRoles = $rRepo->getExistingRoles($roles);
        $user = $uRepo->findByEmail($email);

        if ($dbRoles->isEmpty()) {
            RtlHelpers::log(Logger::ERROR, 'You must specify at least one role name to run the command.', $this);

            return;
        }

        if ($user == null) {
            RtlHelpers::log(logger::ERROR, 'The specified user cannot be found on our databases.', $this);

            return;
        }

        // Try to login as runAs user.
        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = $uRepo->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        if (!$this->login($sysUser)) {
            RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            // Get the roles for the user.
            $userRoles = $user->roles()->get()->first();

            // If there is no role available, implies that the role could not be removed.
            if ($userRoles->pluck('name')->intersect($roles)->isEmpty()) {
                RtlHelpers::log(Logger::ERROR, 'You must have at least one role in order to run the command.', $this);

                return;
            }

            $uRepo->removeRole($user, $dbRoles);
            RtlHelpers::log(Logger::INFO, sprintf('The role(s) %s have been removed from the user %s', implode(',', $roles), $email), $this);
            $this->logout();

        } catch (\Exception $e) {

            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
