<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class AddPermission extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:addPermission
                                { permission : permission name}
                                { --runAs=   : User that run the command. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new permission';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $permission = $this->argument('permission') ? $this->argument('permission') : '';

        $pRepository = new PermissionRepository();
        $this->info('Validating the provided data...');
        if ($pRepository->validate([
            'name' => $permission])
        ) {
            foreach ($pRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            // Trying to login with
            if (!$this->login($sysUser)) {
                RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

                return;
            }

            $this->info('Saving the new permission...');
            $permissionResult = $pRepository->create($permission);
            $this->logout();
            $this->info('Done! The permission has been created sucessfully');

        } catch (\Exception $e) {
            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
