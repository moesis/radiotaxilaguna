<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Monolog\Logger;

class GivePermissionsToRole extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:givePermissionsToRole 
                                { role          : Role name},
                                { permissions*  : Permissions names }
                                { --runAs=      : User to be authenticated }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign one or more permissions to a role.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $role = $this->argument('role');
        $permissions = $this->argument('permissions');

        // Only use the Permissions that exist in database.
        $rRepo = new RoleRepository();
        $pRepo = new PermissionRepository();

        $dbPermissions = $pRepo->getExistingPermissions($permissions);
        $dbRole = $rRepo->findByName($role);

        if ($dbPermissions->isEmpty()) {
            RtlHelpers::log(Logger::ERROR, 'You must specify at least one existing permission name to run the command', $this);

            return;
        }

        if ($dbRole->isEmpty()) {
            RtlHelpers::log(Logger::ERROR, sprintf('The role [%s] cannot be found on our databases.', $role), $this);

            return;
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide new credentials.', $this);

            return;
        }

        try {
            // Trying to login with
            if (!$this->login($sysUser)) {
                RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

                return;
            }
            $rRepo->assignPermissionsTo($dbRole->first(), $dbPermissions);
            RtlHelpers::log(Logger::INFO, sprintf('The permission(s) [%s] have been assigned sucessfully to the role %s', implode(',', $dbPermissions->pluck('name')->toArray()), $role), $this);
            $this->logout();

        } catch (\Exception $e) {

            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
