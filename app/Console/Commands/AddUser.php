<?php

namespace App\Console\Commands;

use App\Facades\RtlHelpers;
use App\Mail\UserMailable;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Monolog\Logger;

class AddUser extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rtl:addUser 
                                { username   : User name }
                                { email      : Email that identifies the user }
                                { password?  : Password that will be used as user password. if it is empty the password will be generated and will be send to the user email.}
                                { --runAs=   : User that run the command. }
                                { --no-email : Flag to indicate that mail wont be sent. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user in the application and send an email to owner. If password is not provided the command generate ones';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('username') ? $this->argument('username') : '';
        $email = $this->argument('email') ? $this->argument('email') : '';
        $password = $this->argument('password') ? $this->argument('password') : '';

        $uRepository = new UserRepository();

        $this->info('Validating the provided data...');
        if ($uRepository->validate([
            'user'     => $user,
            'email'    => $email,
            'password' => $password])
        ) {

            foreach ($uRepository->getValidationMesasges() as $line) {
                $this->error($line);
            }

            return;
        }

        // Password is not given.
        if (empty($password)) {
            $this->info('Generating password for user...');
            $password = RtlHelpers::generatePassword(intval(Config::get('inmo.fields.password.max')));
        }

        if (($runAsUser = $this->option('runAs')) == null) {
            RtlHelpers::log(Logger::ERROR, 'You must authenticate in order to use the command.', $this);

            return;
        }

        if (($sysUser = (new UserRepository())->findByEmail($runAsUser)) == null) {
            RtlHelpers::log(Logger::ERROR, 'There is no user with this credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        // Trying to login with
        if (!$this->login($sysUser)) {
            RtlHelpers::log(Logger::ERROR, 'Wrong credentials. Contact with your supervisor in order to provide to you new credentials.', $this);

            return;
        }

        try {
            $this->info('Saving the new user...');
            $user = $uRepository->create($user, $email, $password);

            if (!$this->option('no-email')) {
                // Send an email to the user in order to comunicate the new password.
                $this->info('Sending the email to the user...');
                Mail::to($email)->send(new UserMailable(['title' => 'Nuevo usuario', 'view' => 'email.newuser', 'user' => $email, 'password' => $password]));
            }
            $this->logout();

            $this->info('Done! The user has been created sucessfully');
        } catch (\Exception $e) {
            RtlHelpers::log(Logger::ERROR, $e->getMessage(), $this);

            return;
        }
    }
}
