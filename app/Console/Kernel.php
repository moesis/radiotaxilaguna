<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\AddUser::class,
        \App\Console\Commands\UpdateUser::class,
        \App\Console\Commands\ActivateUser::class,
        \App\Console\Commands\AddRole::class,
        \App\Console\Commands\UpdateRole::class,
        \App\Console\Commands\AssignRolesToUser::class,
        \App\Console\Commands\RemoveRolesFromUser::class,
        \App\Console\Commands\RemoveRole::class,
        \App\Console\Commands\AddPermission::class,
        \App\Console\Commands\UpdatePermission::class,
        \App\Console\Commands\GivePermissionsToUser::class,
        \App\Console\Commands\GivePermissionsToRole::class,
        \App\Console\Commands\ImportPostalCodes::class,
        \App\Console\Commands\SyncTelegram::class,
        \App\Console\Commands\CheckExpiryDates::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
