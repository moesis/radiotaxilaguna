<?php
/**
 * Licence Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Models\Licence;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class LicenceRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Licence::class;
    }

    /**
     * Return the list of all Members
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return Collection
     */
    public function getList(): Collection
    {
        $licences = null;
        $licences = Licence::select('id', 'member_id', 'association_id', 'municipal_licence', 'deleted_at')->with([
                'member'      => function ($query) {
                    $query->select('id', 'name', 'lastname', 'mobile', 'email');
                },
                'association' => function ($query) {
                    $query->select('id', 'name');
                },
            ]
        )->get();

        if (!is_null($licences)) {
            foreach ($licences->toArray() as $id => $values) {
                array_walk_recursive($values, function (&$value) {
                    $value = $value === "" ? 'N/D' : $value;
                });
                $lic = "000" . $values['municipal_licence'];
                $values['municipal_licence'] = "LM-" .substr ($lic, strlen($lic)-3,3);
                $licences[$id] = array_dot($values);
            }
        }

        return $licences;
    }

    /**
     * Get all licence data with all relationships
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return array|NULL
     */
    public function getData(int $id): array
    {
        // Load Personal Data.
        $licenceModel = $this->model->with(['association', 'member'])->where('id', $id)->first();
        $licence = $licenceModel->toArray();
        $licence['association']['postal_code'] = $licenceModel->member->postalCode()->first()->toArray();
        $licence['member']['postal_code'] = $licenceModel->member->postalCode()->with('locality')->first()->toArray();
        $licence['card-info']['enabled'] = $licenceModel->member->transportCard()->where('disabled', false)->first()->toArray();
        $licence['card-info']['disabled'] = $licenceModel->member->transportCard()->where('disabled', true)->get()->toArray();
        $vehicle = $licenceModel->member->vehicles()->first();
        if ($vehicle) {
            $licence['member']['vehicle'] = collect($vehicle)->except('notes')->toArray();
            $carModel = $vehicle->carModel()->first();
            $licence['member']['vehicle']['notes'] = is_null($vehicle->notes);
            $licence['member']['vehicle']['info'] = $licenceModel->member->vehicles->first()->toArray();
            $licence['member']['vehicle']['brand'] = $carModel->brand()->first()->toArray();
            $licence['member']['vehicle']['model'] = $carModel->toArray();
            $licence['member']['vehicle']['preferences'] = $licenceModel->member->vehicles()->orderBy('created_at', 'DESC')->first()->preferences()->get();
        } else {
            $licence['member']['vehicle']['notes'] = null;
            $licence['member']['vehicle']['id'] = 0;
            $licence['member']['vehicle']['plate'] = '';
            $licence['member']['vehicle']['places'] = Config::get('radiotaxilaguna.company.vehicles.places.min');
            $licence['member']['vehicle']['disabled_places'] = Config::get('radiotaxilaguna.company.vehicles.disabled-places.min');
            $licence['member']['vehicle']['enrollment_date'] = '1970-01-01';
            $licence['member']['vehicle']['cancellation_date'] = null;
            $carModel = null;
            $licence['member']['vehicle']['info'] = null;
            $licence['member']['vehicle']['brand'] = null;
            $licence['member']['vehicle']['model'] = null;
            $licence['member']['vehicle']['preferences'] = [];
        }

        $licence['member']['languages'] = $licenceModel->member->languages()->get();
        $licence['member']['preferences'] = $licenceModel->member->preferences()->Driver()->get();
        $licence['staff'] = $licenceModel->member->staff()->get()->toArray();

        return $licence;
    }

    /**
     * Return the telegram_id for a given licence. Throws an exception if
     * the licence could not be found.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param int $licenceId
     *
     * @return int
     */
    public function getTelegramId(int $licenceId) : int
    {
        $licence = $this->model->where('id', $licenceId)->with(['member'])->first();
        if (!$licence) {
            throw new ModelNotFoundException('Could not found the given licence');
        }

        return $licence->member->telegram_id;
    }

    /**
     * Returns true if the given licence exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param int $municipalLicence
     *
     * @return bool
     */
    public function existsMunicipalLicence(int $municipalLicence): bool
    {
        return !is_null($this->model->where('municipal_licence', intval($municipalLicence))->first());
    }

    /**
     * Returns true if the given gps serial exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $gpsSerial
     *
     * @return bool
     */
    public function existsGps(string $gpsSerial): bool
    {
        return !is_null($this->model->where('gps_serial', $gpsSerial)->first());
    }

    /**
     * Persist the licence data and return the recent record id
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @param array $licenceData
     * @param $memberId
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addLicence(array $licenceData, $memberId): JsonResponse
    {
        $validate = $this->validate($licenceData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }

        $licence = new Licence();
        $this->writeFieldsIntoRecord($licenceData, $licence);
        $licence->member_id = $memberId;

        $licence->saveOrFail();

        return new JsonResponse(['id' => $licence->id]);
    }

    /**
     * Base function to update the data for a licence record.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array   $request
     * @param string    $section
     *
     * @return JsonResponse
     */
    public function update(array $request, string $section)
    {
        if (empty($section)){
            return new JsonResponse(['message' => 'Invalid method name'], Response::HTTP_BAD_REQUEST);
        }
        $functionName = camel_case(sprintf ('update_%s', $section));

        if (!method_exists($this, $functionName)) {
            return new JsonResponse(['message' => 'Method does not exist'], Response::HTTP_BAD_REQUEST);
        }

        return call_user_func_array(__NAMESPACE__ . '\\LicenceRepository::' . $functionName, [$request]);
    }

    /**
     * One of the function set in charge of update the different sections of licence data.
     *
     * This is in charge to update the Personal Data.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updatePersonal(array $request)
    {
        $record = $this->model->find($request['licence_id']);
        unset($request['licence_id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['record' => $record]);
    }

    /**
     * Update the vehicle related data into database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateVehicle(array $request)
    {
        $record = $this->model->find($request['licence_id']);
        unset($request['licence_id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['record' => $record]);
    }

    /**
     * Return the data to populate the DataTable.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Apr 2018
     *
     * @param array $stations
     * @param array $gps
     *
     * @return Array
     */
    public function getStationsAndGps(array $stations, array $gps)
    {
        $reportData = null;
        if (!empty($stations)) {
            $stationsData = Licence::select('id', 'member_id', 'municipal_licence', 'station_brand', 'gps_model')->with([
                    'member'      => function ($query) {
                        $query->select('id', 'name', 'lastname', 'mobile', 'email');
                    },
                ]
            )->whereIn('station_brand', $stations);
        }

        if (!empty($gps)) {
            $gpsData = Licence::select('id', 'member_id', 'municipal_licence', 'station_brand', 'gps_model')->with([
                    'member'      => function ($query) {
                        $query->select('id', 'name', 'lastname', 'mobile', 'email');
                    },
                ]
            )->whereIn('gps_model', $gps);
        }

        $reportData = $stationsData->union($gpsData)->get();

        if (!is_null($reportData)) {
            foreach ($reportData->toArray() as $id => &$values) {
                array_walk_recursive($values, function (&$value) {
                    $value = $value === "" ? 'N/D' : $value;
                });
                $reportData[$id] = array_dot($values);
            }
        }

        return $reportData->toArray();
    }
}