<?php
/**
 * Language Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

class LanguageRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Language::class;
    }

}