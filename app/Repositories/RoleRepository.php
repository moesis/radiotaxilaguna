<?php
/**
 * Role repository grouping all related functions.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Role
 * @package     Repositories
 * @version     1.0
 *
 * @todo
 */

namespace App\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Permission\Models\Role;

class RoleRepository extends BaseRepository
{
    /**
     * Return the validation rules for roles
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param array $data
     * @param bool  $update
     *
     * @return null
     * @todo
     */
    public function rules(array $data, $update = false)
    {
        $rules = null;

        if (!empty($data['name'])) {
            if (empty($update)) {
                $rules['name'] = 'required | string | min:3 | max: 200';
            } else {
                $rules['name'] = 'string | min:3 | max: 200 | exists:roles,name';
            }
        }

        return $rules;
    }

    /**
     * Create a new role in databse.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param $name
     *
     * @return Role
     * @todo
     */
    public function create($name)
    {
        // Create the user and persist it.
        $record = new Role();

        $record->fill([
            'name' => $name,
        ]);

        // Try to save
        $record->saveOrFail();

        // Return the record
        return $record;
    }

    /**
     * Update the role name in database
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param $name
     * @param $newName
     *
     * @return mixed
     * @todo
     */
    public function update($name, $newName)
    {
        // try to find a record name.
        $record = Role::where('name', $name)->first();
        if (!$record) {
            throw new ModelNotFoundException(sprintf('Role %s not found', $name));
        }

        $record->name = $newName;
        $record->saveOrFail();

        return $record;
    }

    /**
     * Remove the Role from database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param string $name
     *
     * @todo
     */
    public function delete($name)
    {
        $record = Role::where('name', $name)->first();
        if (!$record) {
            throw new ModelNotFoundException(sprintf('Role %s not found', $name));
        }

        $record->delete();
    }

    /**
     * Return only the roles that exists in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param array $roles
     *
     * @return mixed
     * @todo
     */
    public function getExistingRoles(array $roles)
    {
        return Role::whereIn('name', $roles)->get();
    }

    /**
     * Return the role referenced by $role parameter, otherwise return null.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version June 2017
     *
     * @param   string $rolename
     *
     * @return  mixed
     * @todo
     */
    public function findByName($rolename)
    {
        return Role::where('name', $rolename)->get();
    }

    /**
     * Assign the Permission(s) to user if the user has not assigned yet,
     * otherwise leave as is the current assignment.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jun 2017
     *
     * @param Role $role
     * @param      $permissions
     *
     * @return Role
     * @todo
     */
    public function assignPermissionsTo(Role $role, $permissions)
    {
        if (empty($role)) {
            throw new \InvalidArgumentException('The role must be non null');
        }

        if (empty($permissions)) {
            throw new \InvalidArgumentException('The permissions must be non null');
        }

        foreach ($permissions as $permission) {
            if (!$role->hasPermissionTo($permission)) {
                $role->givePermissionTo($permission);
            }
        }

        return $role;
    }

}