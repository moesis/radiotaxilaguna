<?php
/**
 * Base Repository with all functions.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Base Repository
 * @package     Repository
 * @version     1.0
 * @todo
 */

namespace App\Repositories;

use App\Http\Requests\FeedbackFormRequest;
use Illuminate\Support\Facades\Validator;

class BaseRepository
{
    const UPDATE_RULES = 'update';

    protected $validator;

    /**
     * Validation the $data array with
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param   $data
     *
     * @return  array|bool
     * @todo
     */
    public function validate($data, $update = null)
    {
        $rules = $this->rules($data, $update);
        if (empty($rules)) {
            throw new \InvalidArgumentException('There is no rules defined.');
        }

        $this->validator = Validator::make($data, $rules);

        return $this->validator->fails();
    }

    /**
     * Return the errors if validation fails
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     * @return mixed
     * @todo
     */
    public function getValidationMesasges()
    {
        return $this->validator->getMessageBag()->all();
    }

}
