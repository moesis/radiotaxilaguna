<?php
/**
 * Common function related with User Model.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Admin
 * @version     1.0
 *
 * @todo
 */

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Traits\HasRoles;

class UserRepository extends BaseRepository
{
    use HasRoles;

    // Active or inactive Users.
    const ACTIVE = 1;
    const INACTIVE = 0;

    public function rules(array $data, $update = null)
    {
        $rules = null;

        if (!empty($data['user'])) {
            if (empty($update)) {
                $rules['user'] = 'required | string | min:3 | max: 200';
            } else {
                $rules['user'] = 'string | min:3 | max: 200';
            }
        }

        if (!empty($data['email'])) {
            if (empty($update)) {
                $rules['email'] = 'required | email | unique:users';
            } else {
                $rules['email'] = 'required | email | exists:users,email';
            }
        }

        if (!empty($data['action'])) {
            $rules['action'] = 'string | in:activate,deactivate';
        }

        if (!empty($data['password'])) {
            $rules['password'] = sprintf('string | min:%s | max:%s',
                Config::get('radiotaxilaguna.setup.fields.password.min'),
                Config::get('radiotaxilaguna.setup.fields.password.max')
            );
        }

        return $rules;
    }

    /**
     * Create an user and return the result record.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param $username
     * @param $email
     * @param $password
     *
     * @return User
     * @todo
     */
    public function create($username, $email, $password)
    {
        // Create the user and persist it.
        $record = new User();

        $record->fill([
            'name'     => $username,
            'email'    => $email,
            'password' => $password,
        ]);

        // Try to save
        $record->saveOrFail();

        // Return the record
        return $record;
    }

    /**
     * Activate or deactivate the user referer by email.
     * Internally uses update method.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param $email
     * @param $activate
     *
     * @return mixed
     *
     * @todo
     */
    public function activate($email, $activate)
    {
        return $this->update($email, null, null, $activate);
    }

    /**
     * Update the user data.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param string $email
     * @param string $username
     * @param string $password
     *
     * @return mixed
     * @todo
     */
    public function update($email, $username = null, $password = null, $activate = 'activate')
    {
        // Try to find the user...
        $record = User::where(['email' => $email])->first();
        if (empty($record)) {
            throw new ModelNotFoundException('The user with email [' . $email . '] has not found.');
        }

        if (!empty($username)) {
            $record->name = $username;
        }

        if (!empty($password)) {
            $record->password = $password;
        }

        $record->activate = $activate != 'activate' ? self::INACTIVE : self::ACTIVE;

        // Try to save
        $record->saveOrFail();

        // Return the record
        return $record;
    }

    /**
     * Assign the role(s) to user.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param User $user
     * @param      $role
     *
     * @return User
     * @todo
     */
    public function assignRole(User $user, $role)
    {
        if (empty($user)) {
            throw new \InvalidArgumentException('The user must be non null');
        }

        if (empty($role)) {
            throw new \InvalidArgumentException('The role must be non null');
        }

        $user->assignRole($role);

        return $user;
    }

    /**
     * Remove all role(s) to user.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param User       $user
     * @param Collection $roles
     *
     * @return User
     * @todo
     */
    public function removeRole(User $user, Collection $roles)
    {
        if (empty($user)) {
            throw new \InvalidArgumentException('The user must be non null');
        }

        if ($roles->isEmpty()) {
            throw new \InvalidArgumentException('The role must be non null');
        }

        foreach ($roles as $role) {
            $user->removeRole($role);
        }

        return $user;
    }

    /**
     * Return an user found by email field. NULL otherwise.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param string $email
     * @param bool   $active Defaults true in order to find only the active users.
     *
     * @return mixed
     * @todo
     */
    public function findByEmail($email, $active = true)
    {
        return User::where('email', $email)->where('activate', $active)->first();
    }

    /**
     * Assign the Permission(s) to user if the user has not assigned yet,
     * otherwise leave as is the current assignment.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jun 2017
     *
     * @param User $user
     * @param      $permissions
     *
     * @return User
     * @todo
     */
    public function assignPermissionsTo(User $user, $permissions)
    {
        if (empty($user)) {
            throw new \InvalidArgumentException('The user must be non null');
        }

        if (empty($permissions)) {
            throw new \InvalidArgumentException('The permissions must be non null');
        }

        foreach ($permissions as $permission) {
            if (!$user->hasPermissionTo($permission)) {
                $user->givePermissionTo($permission);
            }
        }

        return $user;
    }

}