<?php
/**
 * Repository Abstract class.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Ago 2017
 * @catergory   Repostiories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Facades\RtlHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

abstract class Repository implements RepositoryInterface
{
    /**
     * App Instance
     *
     * @var App
     */
    private $app;

    /**
     * Model instance
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Rules to validate the model
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for the validation rules.
     *
     * @var array
     */
    protected $messages;

    /**
     * Attribute names for validation rules.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Repository constructor.
     *
     * @param App $app
     *
     * @throws RepositoryException
     */
    public function __construct(App $app = null)
    {
        $this->app = App::getFacadeApplication();
        $this->model = $this->makeModel();

        $info = $this->loadRules();
        $this->rules = $info['rules'];
        $this->messages = $info['messages'];
        $this->attributes = $info['attributes'];
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * Create a new instance of model
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws RepositoryException
     */
    public function makeModel(): \Illuminate\Database\Eloquent\Model
    {
        $localModel = null;

        $localModel = $this->app->make($this->model());
        if (!$localModel instanceof Model) {
            throw new RepositoryException(sprintf('Class %s must be an instance of Illuminate\\Database\\Eloquent\\Model', $this->model()));
        }

        return $localModel;
    }

    /**
     * Load rules and messages (if they were exists)
     * from config file with same name that model.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return  array
     */
    public function loadRules($data = null): array
    {
        return RtlHelpers::loadRules($this->Model(), $data);
    }

    /**
     * Validation the $data array with
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version May 2017
     *
     * @param   $data
     *
     * @return  \Illuminate\Validation\Validator
     */
    public function validate($data) : \Illuminate\Validation\Validator
    {
        $validatorInfo = [];
//        if (empty($this->rules)) {
            $validatorInfo = $this->loadRules($data);
            $this->rules = $validatorInfo['rules'];
            $this->messages = $validatorInfo['messages'];
            $this->attributes = $validatorInfo['attributes'];
//        }

        return Validator::make($data, $this->rules, $this->messages, $this->attributes);
    }

    /**
     * Return all elements from Model.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->all($columns);
    }

    /**
     * Get results paginated in groups of $perPage items.
     *
     * @param int $perPage
     * @param array $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $perPage = 15, array $columns = ['*']): LengthAwarePaginator
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * Return the Model element with relationships and selected fields.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id                       model ID
     * @param array $columns                columns returned with model
     *
     * @return Model
     */
    public function find(int $id, array $columns = ['*']): Model
    {
        return $this->model->find($id, $columns);
    }

    /**
     * Return the records that match $field with $value.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param string $field
     * @param string $value
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBy(string $field, string $value, array $columns = ['*']) : Model
    {
        return $this->model->select($columns)->where($field, $value);
    }

    /**
     * Write the request content into record.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version jan 2018
     *
     * @param array $fields Content to be write into record.
     * @param Model $record Record itself.
     */
    public function writeFieldsIntoRecord(array $fields, Model &$record)
    {
        foreach ($fields as $field => $data) {
            $record->$field = $data;
        }
    }

//    /**
//     * Persist the model data into database using the fill method
//     *
//     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
//     * @version Spet 2017
//     *
//     * @param array $data
//     *
//     * @return Model
//     */
//    public function create(array $data)
//    {
//        $model = new $this->model();
//        $model->fill($data);
//        $model->save();
//
//        return $model;
//    }
//
//
//    public function update(array $data, int $id)
//    {
//        // TODO: Implement update() method.
//    }
//
//    public function delete(int $id)
//    {
//        // TODO: Implement delete() method.
//    }
}