<?php
/**
 * Documents Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Feb 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Http\JsonResponse;
use Webpatser\Uuid\Uuid;
use App\Models\Document;

class DocumentsRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Document::class;
    }

    /**
     * Upload response for Ajax transmission
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param   array $formData
     *
     * @return JsonResponse
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function uploadDocuments(array $formData): JsonResponse
    {
        $documents = $formData['files'];
        $primaryKey = '';
        $repository = '';

        $allowedModels = (new $this->model())->getAllowedModels();
        foreach ($allowedModels as $allowedModel => $modelName) {
            if (array_key_exists($allowedModel, $formData)) {
                $primaryKey = $formData[$allowedModel];
                $repository = 'App\\Repositories\\' . ucwords($modelName) . 'Repository';
                break;
            }
        }

        if (empty($primaryKey) || empty($repository)) {
            throw new \Exception('Model not allowed.');
        }

        // Generic repository instantiation...
        $ownerRepository = new $repository();
        $owner = $ownerRepository->find($primaryKey);

        foreach ($documents as $document) {
            $originalName = $document->getClientOriginalName();
            $extension = $document->getClientOriginalExtension();
            $allowedFilename = $this->createUniqueFilename($extension);

            // Check if document already exists...
            if (!Document::where('title', $originalName)->first()) {
                // Save the documents using storage ...
                $path = Storage::disk('documents')->putFileAs($primaryKey, $document, $allowedFilename);

                // Save the record
                $record = new Document();
                $record->title = $originalName;
                $record->filename = $allowedFilename;
                $owner->documents()->save($record);
                $record->saveorFail();
            }
        }

        return new JsonResponse([
            'error' => '',
            'code'  => HttpFoundation::HTTP_OK,
        ], HttpFoundation::HTTP_OK);

    }

    /**
     * Return the array of documents with a specific structure.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param int $licenceId
     *
     * @return array
     * @throws RepositoryException
     * @throws \Exception
     */
    public function getDocuments(int $licenceId) : array
    {
        $licenceRepository = new LicenceRepository();
        $licence = $licenceRepository->find($licenceId);
        if (!$licence) {
            throw new \Exception('Licence not found.');
        }

        $documents = [];
        $licenceDocuments = $licence->documents()->get();
        $fileTypes = Config::get('radiotaxilaguna.file-types');

        foreach ($licenceDocuments as $document) {
            $doc['filename'] = $document->title;
            $extension = pathinfo($document->filename, PATHINFO_EXTENSION);
            $doc['icon'] = $fileTypes[$extension] ?? 'fa-file-o';
            $doc['link'] = Storage::disk('documents')->url($licenceId . DIRECTORY_SEPARATOR . $document->filename);

            array_push($documents, $doc);
        }

        return $documents;
    }

    /**
     * Return an unique filnema based on UUID v4 with the given file extension
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param string $extension
     *
     * @return string
     * @throws \Exception
     */
    public function createUniqueFilename(string $extension): string
    {
        $fullSizeDir = Config::get('images.full_size');
        do {
            $filename = Uuid::generate(4) . '.' . $extension;
            $fullImagePath = $fullSizeDir . $filename;
        } while (File::exists($fullImagePath));

        return $filename;
    }

    /**
     * Delete Image From Session folder, based on original filename
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param $originalFilename
     *
     * @return JsonResponse
     */
    public function delete($originalFilename)
    {
        $fullSizeDir = Config::get('images.full_size');
        $iconSizeDir = Config::get('images.icon_size');
        $document = $this->model->where('original_name', 'like', $originalFilename)->first();

        if (empty($document)) {
            return new JsonResponse([
                'error' => true,
                'code'  => Response::HTTP_BAD_REQUEST,
            ], Response::HTTP_BAD_REQUEST);
        }

        $fullPath1 = $fullSizeDir . $document->filename;
        $fullPath2 = $iconSizeDir . $document->filename;

        if (File::exists($fullPath1)) {
            File::delete($fullPath1);
        }

        if (File::exists($fullPath2)) {
            File::delete($fullPath2);
        }

        if (!empty($document)) {
            $document->delete();
        }

        return new JsonResponse([
            'error' => false,
            'code'  => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }

    /**
     * Sanitize the filename.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param $string
     * @param bool $force_lowercase
     * @param bool $anal
     *
     * @return mixed|null|string|string[]
     */
    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = ["~","`","!","@","#","$","%","^","&","*","(",")","_","=","+","[","{","]","}","\\","|",";",":","\"","'",
            "&#8216;","&#8217;","&#8220;","&#8221;","&#8211;","&#8212;","â€”","â€“",",","<",".",">", "/","?",
        ];
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
}