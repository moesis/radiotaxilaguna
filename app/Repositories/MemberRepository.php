<?php
/**
 * Licence Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Sept 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Models\Member;
use App\Models\Notification;
use App\Models\TransportCard;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class MemberRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Member::class;
    }

    /**
     * Returns true if the given personal id exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $memberPersonalId
     *
     * @return bool
     */
    public function exists(string $memberPersonalId): bool
    {
        return !is_null($this->model->where('pid', intval($memberPersonalId))->first());
    }

    /**
     * Returns true if the given email exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $email
     *
     * @return bool
     */
    public function emailExists(string $email): bool
    {
        return !is_null($this->model->where('email', $email)->first());
    }

    /**
     * Returns true if the given transport card exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $iban
     *
     * @return bool
     */
    public function existsIban(string $iban): bool
    {
        return !is_null($this->model->where('iban', $iban)->first());
    }

    /**
     * Returns true if the given transport card exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $transportCard
     *
     * @return bool
     */
    public function existsTransportCard(string $transportCard): bool
    {
        return (is_numeric($transportCard) && ($this->model->whereHas('transportCard',
                    function ($query) use ($transportCard) {
                        $query->where('transport_card', intval($transportCard));
                    })->get()->count() > 0));
    }

    /**
     * Persist a new member into database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @param array $memberData
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addMember(array $memberData): JsonResponse
    {
        $validate = $this->validate($memberData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }

        $member = new Member();
        $this->writeFieldsIntoRecord($memberData, $member);
        $member->saveOrFail();

        return new JsonResponse(['id' => $member->id]);
    }

    /**
     * Persist the transport card into database
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @param array $transportCardData Array with Record data.
     * @param integer $memberId        JSON string with encoded parent data.
     *
     * @return JsonResponse With following values:
     *                      HTTP_FAILED_DEPENDENCY      => The provided information about the parent record is invalid
     *                      HTTP_NOT_FOUND              => The parent record cannot be found
     *                      HTTP_INTERNAL_SERVER_ERROR  => The record and its relationships could not be persisted
     *                      HTTP_OK                     => The operation was successful
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addTransportCard(array $transportCardData, $memberId): JsonResponse
    {
        $tcr = new TransportCardRepository();
        $validate = $tcr->validate($transportCardData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }
        if (!empty($memberId) && !is_numeric($memberId)) {
            return new JsonResponse(['msg' => 'Cannot make the relationship. Invalid parent data'],
                Response::HTTP_FAILED_DEPENDENCY);
        }
        $transportCard = new TransportCard();
        $this->writeFieldsIntoRecord($transportCardData, $transportCard);
        $transportCard->member_id = $memberId;
        $transportCard->saveOrFail();

        return new JsonResponse(['id' => $transportCard->id]);
    }

    /**
     * Base function to update the data for a licence record.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     * @param string $area
     *
     * @return JsonResponse
     */
    public function update(array $request, string $area)
    {
        if (empty($area)) {
            return new JsonResponse(['message' => 'Invalid method name'], Response::HTTP_BAD_REQUEST);
        }
        $functionName = camel_case(sprintf('update_%s', $area));

        if (!method_exists($this, $functionName)) {
            return new JsonResponse(['message' => 'Method does not exist'], Response::HTTP_BAD_REQUEST);
        }

        return call_user_func_array(__NAMESPACE__ . '\\MemberRepository::' . $functionName, [$request]);
    }

    /**
     * One of the function set in charge of update the different sections of licence data.
     *
     * This is in charge to update the Personal Data.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updatePersonal(array $request): JsonResponse
    {
        $record = $this->model->find($request['id']);
        unset($request['id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['id' => $record->id]);
    }

    /**
     * Update the transport card for a member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     */
    public function updateTransportCard(array $request): JsonResponse
    {
        $record = $this->model->find($request['id'])->transportCard->get();
        unset($request['id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['id' => $record->id]);
    }


    /**
     * Update Vehicle and related models data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function updateVehicle(array $request): JsonResponse
    {
        $record = $this->model->find($request['member_id']);
        unset ($request['member_id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        };

        $vehicleId = $request['vehicle_id'];
        unset ($request['vehicle_id']);
        $vehicleRepository = new VehicleRepository();
        $vehicleUpdateResponse = $vehicleRepository->updateVehicleModel($vehicleId, $request['car_model_id']);
        if ($vehicleUpdateResponse->status() !== Response::HTTP_OK) {
            return new JsonResponse(['response' => $vehicleUpdateResponse->content()], Response::HTTP_BAD_REQUEST);
        }

        unset ($request['car_model_id']);
        $vehicleUpdateResponse = $vehicleRepository->update($vehicleId, $request);
        if ($vehicleUpdateResponse->status() !== Response::HTTP_OK) {
            return new JsonResponse(['response' => $vehicleUpdateResponse->content()], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['id' => $vehicleId]);
    }

    /**
     * Specific update function to persist the document related data.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateDocument(array $request): JsonResponse
    {
        $record = $this->find($request['member_id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        };

        unset($request['member_id']);

        $transportCardData = [
            'transport_card'     => $request['transport_card'],
            'tc_expiration_date' => $request['tc_expiration_date'],
        ];

        unset($request['transport_card']);
        unset($request['tc_expiration_date']);

        $transportCard = $record->transportCard->where('disabled', null)->first();
        if (!$transportCard) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        }
        $this->writeFieldsIntoRecord($transportCardData, $transportCard);
        $transportCard->saveOrFail();

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['id' => $record->id]);
    }

    /**
     * Uodate the specific economic information.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateEconomic(array $request)
    {
        $record = $this->find($request['member_id']);

        if (!$record) {
            return new JsonResponse(['message' => 'Registro no encontrado'], Response::HTTP_NOT_FOUND);
        };

        unset($request['member_id']);
        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['id' => $record->id]);
    }

    /**
     * upodate the member preferences as driver and spoken languages
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     */
    public function updatePreferences(array $request)
    {
        $record = $request['member'];

        $record->preferences()->sync(explode(',', $request['driverPreferences']));
        $record->languages()->sync(explode(',', $request['spokenLanguagesPreferences']));

        return new JsonResponse(['id' => $record->id]);
    }

    /**
     * Update the Notification Date for given Licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Apr 2018
     *
     * @param int $licenceId
     * @param Carbon $date
     *
     * @return int
     */
    public function updateNotificationDate(int $licenceId, Carbon $date)
    {
        $notificationModel = new Notification();
        $record = $notificationModel->where('licence_id', $licenceId)->first();

        $record->date = $date->toDateTimeString();
        if ($record->save()) {
            return Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return Response::HTTP_OK;
    }

    /**
     * Return the members with next expiration dates...
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return array
     */
    public function getExpirations()
    {
        $pidNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.pid_expiration_date'))->toDateTimeString();
        $driverLicenceNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.driver_licence_expiry_date'))->toDateTimeString();
        $municipalLicenceNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.municipal_licence_expiry_date'))->toDateTimeString();
        $transportCardNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.tc_expiration_date'))->toDateTimeString();

        $expirations = [
            'pid'              => $this->model
                ->where('pid_expiration_date', '<=', $pidNoticeDate)
                ->with(['licence', 'licence.notifications'])
                ->orderBy('pid_expiration_date', 'asc')
                ->get(),
            'driverLicence'    => $this->model
                ->where('driver_licence_expiry_date', '<=', $driverLicenceNoticeDate)
                ->with(['licence', 'licence.notifications'])
                ->orderBy('driver_licence_expiry_date', 'asc')
                ->get(),
            'municipalLicence' => $this->model
                ->where('municipal_licence_expiry_date', '<=', $municipalLicenceNoticeDate)
                ->with(['licence', 'licence.notifications'])
                ->orderBy('municipal_licence_expiry_date', 'asc')
                ->get(),
            'transportCard'    => TransportCard::where('tc_expiration_date', '<=', $transportCardNoticeDate)
                                               ->with(['member', 'member.licence', 'member.licence.notifications'])
                                               ->orderBy('tc_expiration_date', 'asc')
                                               ->get(),
        ];

        return $expirations;
    }
}