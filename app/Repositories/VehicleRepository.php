<?php
/**
 * Car Brand Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Models\CarModel;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;


class VehicleRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Vehicle::class;
    }

    /**
     * Return the list of all Members
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return array
     */
    public function getList(): array
    {
        $vehicles = $this->model()->all();
        if (is_null($vehicles)) {
            return $vehicles;
        }

        $list = $vehicles->toArray();

        return $list;
    }

    /**
     * Return true if plate give does exist in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $plate
     *
     * @return bool
     */
    public function existsPlate(string $plate): bool
    {
        return !is_null($this->model->select('id')->where('cancellation_date', null)->where('plate', $plate)->first());
    }

    /**
     * Persist the vehicle into database
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @param array $vehicleData
     * @param $memberId
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addVehicle(array $vehicleData, $memberId): JsonResponse
    {
        $validate = $this->validate($vehicleData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }

        $vehicle = new Vehicle();
        foreach ($vehicleData as $key => $value) {
            $vehicle->$key = $value;
        }

        $vehicle->saveOrFail();
        $vehicle->members()->attach($memberId);

        return new JsonResponse(['id' => $vehicle->id]);
    }

    /**
     * Update the vehicle model from variable.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param integer   $vehicleId Vechicle Id
     * @param integer   $model     Car Model Id
     *
     * @return JsonResponse
     */
    public function updateVehicleModel($vehicleId, $model)
    {
        $vehicleId = Vehicle::find($vehicleId);
        if (!$vehicleId) {
            return new JsonResponse(['message' => 'Vehículo no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $carModel = CarModel::find($model);
        if (!$carModel) {
            return new JsonResponse(['message' => 'Modelo de vehículo no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $vehicleId->carModel()->associate($carModel);
        $vehicleId->saveOrFail();

        return new JsonResponse(['id' => $vehicleId->id]);
    }

    /**
     * Update the vehicle data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param int $vehicleId
     * @param array $request
     *
     * @return JsonResponse
     */
    public function update(int $vehicleId, array $request)
    {
        $record = Vehicle::find($vehicleId);
        if (!$record) {
            return new JsonResponse(['message' => 'Vehículo no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $this->writeFieldsIntoRecord($request, $record);
        $record->saveOrFail();

        return new JsonResponse(['record' => $record]);
    }

    /**
     * Update the vehicle preferences
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param array $request
     *
     * @return JsonResponse
     */
    public function updatePreferences(array $request)
    {
        $record = $request['vehicle'];

        $record->preferences()->sync(explode(',', $request['vehiclePreferences']));

        return new JsonResponse(['record' => $record]);
    }

    /**
     * Update the notes for the vehicle $vehicleId
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param int $vehicleId
     * @param array $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateNotes(int $vehicleId, array $request)
    {
        $newNotes = [
            'date' => Carbon::now('Europe/London')->format('Y-m-d H:i'),
            'text' => $request['notes'],
        ];

        $record = $this->find($vehicleId);
        if (!$record) {
            return new JsonResponse(['message' => 'Vehículo no encontrado'], Response::HTTP_NOT_FOUND);
        }

        $notes[] = json_decode($record->notes, true);
        array_push($notes, $newNotes);
        $record->notes = json_encode($notes);

        $record->saveOrFail();

        return new JsonResponse(['record' => $record]);
    }

    /**
     * Get data from Licence, Member and vehicle from Plate.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @param string $plate
     *
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getPlateWithLicence(string $plate)
    {
        $plateData = $this->model->with('carModel', 'carModel.brand', 'members', 'members.licence')->where('cancellation_date', null)->where('plate', $plate)->first();

        if (!$plateData) {
            throw new ModelNotFoundException('La matrícula buscada no se encuentra en nuestros archivos');
        }

        return $plateData;
    }

}