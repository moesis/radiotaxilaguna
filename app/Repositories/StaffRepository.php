<?php
/**
 * Staff Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Nov 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Http\Controllers\BaseController;
use App\Models\Language;
use App\Models\Staff;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class StaffRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Staff::class;
    }

    /**
     * Return the list of all Staff
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return Collection
     */
    public function getList(): Collection
    {
        $staff = null;
        /*$staff = Staff::with([
            'members' => function ($query) {
                $query->select('name', 'lastname')->where('end_date', null)->orWhere('end_date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
            }
        ])->get();*/

        $staff = Staff::with([
            'members' => function ($query) {
                $query->select('name', 'lastname')
                    ->where('end_date', null)
                    ->orWhere('end_date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
            },
        ])->with(['members.licence'])->get();

        if (!is_null($staff)) {
            $tempStaff = $staff->toArray();

            foreach ($tempStaff as $id => $values) {
                if (empty($values['members'])) {
                    $values['members']['name'] = '';
                    $values['members']['lastname'] = '';
                    $values['members']['licence']['municipal_licence'] = '';
                } else {
                    // The staff can have only one boss, so take the first element.
                    $values['members'] = $values['members'][0];
                    if (empty($values['members']['licence'])) {
                        $values['members']['licence']['municipal_licence'] = '';
                    } else {
                        $lic = "000" . $values['members']['licence']['municipal_licence'];
                        $values['members']['licence']['municipal_licence'] = "LM-" .substr ($lic, strlen($lic)-3,3);
                    }
                }

                array_walk_recursive($values, function (&$value, $key) {
                    // Do not process the timestamps fields.
                    if (!in_array($key, BaseController::getTimestampsFields())) {
                        $value = empty($value) ? 'N/D' : $value;
                    }
                });
                $staff[$id] = array_dot($values);
            }
        }

        return $staff;
    }

    /**
     * Return the staff related data in dot format
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param int $id
     *
     * @return array
     *
     */
    public function getData(int $id) : array
    {
        if (!is_numeric($id)) {
            return null;
        }

        $staff = Staff::where('id', $id)->with(['members', 'members.licence', 'postalCode', 'contract', 'languages', 'preferences'])->first();
        $result = [];

        $result['staff'] = $staff->attributestoArray();
        $result['staff']['postal-code'] = $staff->postalCode->toArray();
        $result['staff']['locality'] = $staff->postalCode->locality()->first()->toArray();
        $result['staff']['member'] = [];
        $result['staff']['contract'] = [];
        if ($staff->members->count() > 0) {
            $result['staff']['member'] = $staff->members->first()->toArray();
            //$result['staff']['member']['licence'] = $staff->members->first()->licence()->first()->toArray();
            $result['staff']['contract'] = $staff->contract->first()->toArray();
        }

        $result['staff']['languages'] = $staff->languages;
        $result['staff']['preferences'] = $staff->preferences->isEmpty() ? [] : $staff->preferences->where('type', 'DRIVER');

        return $result['staff'];
    }

    /**
     * Returns true if the given personal id exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $staffPersonalId
     *
     * @return bool
     */
    public function exists(string $staffPersonalId): bool
    {
        return !is_null($this->model->where('pid', intval($staffPersonalId))->first());
    }

    /**
     * Returns true if the given email exists, otherwise return false
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param string $email
     *
     * @return bool
     */
    public function emailExists(string $email): bool
    {
        return !is_null($this->model->where('email', $email)->first());
    }

    /**
     * Persist a new member into database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Oct 2017
     *
     * @param array $staffData
     * @param array $contractData
     * @param array $languages
     * @param array $preferences
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function add(array $staffData, array $contractData, array $languages, array $preferences): JsonResponse
    {
        $validate = $this->validate($staffData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }

        $staff = new Staff();
        foreach ($staffData as $key => $value) {
            $staff->$key = $value;
        }

        try {
             $staff->saveOrFail();

             // Attach Languages.
            if (empty($languages)) {
                // Languages, by default always speak Spanish (id = 1)
                $staff->languages()->attach(Language::where('name', 'Español')->first());
            } else {
                foreach($languages as $language) {
                    $staff->languages()->attach($language);
                }
            }

            // Attach Preferences.
            if (!empty($preferences)) {
                foreach ($preferences as $preference) {
                    $staff->preferences()->attach($preference);
                }
            }

            // Contract relationship...
            if (!empty(array_filter($contractData, function($v, $k) {
                return !is_null($v);
            }, ARRAY_FILTER_USE_BOTH))) {
                $staff->members()->attach($staff->id, [
                    'member_id'        => $contractData['boss-name'],
                    'contract_type_id' => $contractData['contract-type'],
                    'start_date'       => $contractData['start_date'],
                    'end_date'         => $contractData['end_date'],
                ]);
            }
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], Response::HTTP_FAILED_DEPENDENCY);
        }

        return new JsonResponse(['id' => $staff->id]);
    }

    /**
     * Update the Staff record.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec 2017
     *
     * @param array $staffData
     * @param array $contractData
     * @param array $languagesData
     * @param array $preferencesData
     *
     * @return JsonResponse
     */
    public function update(array $staffData, array $contractData, array $languagesData, array $preferencesData): JsonResponse
    {
        $validate = $this->validate($staffData);
        if ($validate->fails()) {
            return new JsonResponse($validate->errors()->toArray(), Response::HTTP_BAD_REQUEST);
        }

        $staff = $this->find($staffData['id']);
        if (!$staff) {
            return new JsonResponse(['message' => 'No se encontró la ficha del asalariado'], Response::HTTP_NOT_FOUND);
        }

        foreach ($staffData as $key => $value) {
            $staff->$key = $value;
        }

        try {
            $staff->saveOrFail();
            $staff->languages()->sync($languagesData);
            $staff->preferences()->sync($preferencesData);

            if (array_filter($contractData)) {
                $staff->members()->sync([
                    $contractData['boss-name'] => [
                        'contract_type_id' => $contractData['contract-type'],
                        'start_date'       => $contractData['start_date'],
                        'end_date'         => $contractData['end_date'],
                    ]
                ]);
            }

        }catch (\Throwable $e) {
            return new JsonResponse(['message' => $e->getMessage()], Response::HTTP_FAILED_DEPENDENCY);
        }

        return new JsonResponse(['id' => $staff->id]);
    }

    /**
     * Return the members with next expiration dates...
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jun 2018
     *
     * @return array
     */
    public function getExpirations()
    {
        $pidNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.pid_expiration_date'))->toDateTimeString();
        $driverLicenceNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.driver_licence_expiry_date'))->toDateTimeString();
        $municipalLicenceNoticeDate = Carbon::now(new \DateTimeZone('Europe/London'))->addDays(Config::get('radiotaxilaguna.expirations.municipal_licence_expiry_date'))->toDateTimeString();

        $expirations = [
            'pid' => $this->model->where('pid_expiration_date', '<=', $pidNoticeDate)->get(),
            'driverLicence'    => $this->model->where('driver_licence_expiry_date', '<=', $driverLicenceNoticeDate)->with(['members', 'members.licence', 'members.licence.notifications'])->orderBy('driver_licence_expiry_date', 'asc')->get(),
            'municipalLicence' => $this->model->where('municipal_licence_expiry_date', '<=', $municipalLicenceNoticeDate)->with(['members', 'members.licence', 'members.licence.notifications'])->orderBy('municipal_licence_expiry_date', 'asc')->get(),
        ];

        return $expirations;
    }
}