<?php
/**
 * Transport Card Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Oct 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

class TransportCardRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\TransportCard::class;
    }

}