<?php
/**
 * Car Brand Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Models\CarBrand;


class CarBrandRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\CarBrand::class;
    }

    /**
     * Return the list of all Members
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return array
     */
    public function getList(): array
    {
        $member = CarBrand::all();
        if (is_null($member)) {
            return $member;
        }

        $list = $member->toArray();

        return $list;
    }
}