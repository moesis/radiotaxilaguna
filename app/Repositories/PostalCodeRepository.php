<?php
/**
 * Postal Code Repository
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Sept 2017
 * @catergory   Repositories
 * @package     Radio Taxi Laguna Core
 */

namespace App\Repositories;

use App\Models\Locality;
use App\Models\PostalCode;


class PostalCodeRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\PostalCode::class;
    }

    /**
     * Persist the model data into database using the fill method
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Spet 2017
     *
     * @param array $data
     *
     * @return PostalCode
     */
    public function create(array $data): PostalCode
    {
        $model = new $this->model();

        $model->postal_code = $data['postal_code'];
        $model->name = $data['place'];
        $locality = Locality::find($data['locality_id']);
        $model->locality()->associate($locality);
        $model->saveOrFail();

        return $model;
    }

}