<?php
/**
 * Interface for Repositories
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Repostiories
 * @package     Radio Taxi Laguna Core.
 */

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    public function all(array $columns = ['*']) : Collection;

    public function paginate(int $perPage = 15, array $columns = ['*']) : LengthAwarePaginator;

//    public function create(array $data);
//
//    public function update(array $data, int $id);
//
//    public function delete(int $id);

    public function find(int $id, array $columns = ['*']);

    public function findBy(string $field, string $value, array $columns = ['*']) : Model;

}