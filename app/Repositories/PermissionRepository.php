<?php
/**
 * Persmission Repostiry groupping all operations.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @date        May 2017
 * @catergory   Permissions
 * @package     Repostories
 * @version     1.0
 *
 * @todo
 */

namespace App\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Permission\Models\Permission;

class PermissionRepository extends BaseRepository
{
    /**
     * Return the rules based on parameters.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @date    May 2017
     * @version 1.0
     *
     * @param $update bool
     *
     * @return array
     *
     * @todo
     */
    public function rules($update = false)
    {
        $ifUpdate = ' | exists:permissions,name';

        $rules = [
            'name'     => 'required | string | min:3 | max: 40',
            'new-name' => 'required_with:name | string | min:3 | max: 40',
        ];

        // Only of update is selected in order to check
        // the permission exists in database.
        if (!$update) {
            $rules['name'] .= $ifUpdate;
        }

        return $rules;
    }

    /**
     * Create an user and persist into database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @date    May 2017
     * @version 1.0
     *
     * @param string $name
     *
     * @return Permission|array|bool
     *
     * @todo
     */
    public function create($name)
    {
        $data = ['name' => $name];
        $validation = $this->validate($data);

        if (is_array($validation)) {
            return $validation;
        }

        // Create the user and persist it.
        $record = new Permission();

        // Fill the data.
        $record->name = $name;

        // Try to save
        $record->saveOrFail();

        // Return the record
        return $record;
    }

    /**
     * Update the permission with $newName
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version June 2017
     *
     * @param $name
     * @param $newName
     *
     * @return array|bool
     * @todo
     */
    public function update($name, $newName)
    {
        $data = [
            'name'     => $name,
            'new-name' => $newName,
        ];
        $validation = $this->validate($data);

        if (is_array($validation)) {
            return $validation;
        }
        $record = Permission::where(['name' => $name])->first();

        if (!$record) {
            throw new ModelNotFoundException(sprintf('[%s] cannot be found.'));
        }

        $record->name = $newName;
        $record->saveOrFail();

        return $record;
    }

    /**
     * Return only the permissions that exists in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version June 2017
     *
     * @param array $permissions
     *
     * @return mixed
     * @todo
     */
    public function getExistingPermissions(array $permissions)
    {
        return Permission::whereIn('name', $permissions)->get();
    }

}