<?php

namespace App\Providers;

use App\Classes\RtlHelpers;
use Illuminate\Support\ServiceProvider;

class RtlHelpersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RtlHelpers::class,function(){
            return new RtlHelpers;
        });
    }
}
