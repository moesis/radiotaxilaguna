<?php

namespace App\Http\Requests;

use App\Facades\RtlHelpers;
use App\Models\PostalCode;
use Illuminate\Http\Request;

class PostalCodeFormRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) : array
    {
        return RtlHelpers::loadRules(PostalCode::class, $request);
    }

    public function messages()
    {
        return RtlHelpers::loadMessages(PostalCode::class);
    }
}
