<?php

namespace App\Http\Requests;

use App\Models\Language;
use Illuminate\Http\Request;

class LanguageFormRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return $this->loadRules(Language::class, $request);
    }
}
