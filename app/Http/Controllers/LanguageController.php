<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LanguageController extends BaseController
{

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Mantenimientos';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Idiomas y Lugares';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = 'Idiomas';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Language::all();

        return $this->composeView('intranet.cruds.languages.list', ['title' => 'Lenguajes', 'path' => ['Idiomas'], 'records' => $records->pluck('name', 'id')]);
    }

    public function getRecord(Request $request)
    {
        try {
            $resource = $request->get('resource', '');
            $id = intval($request->get('record', 0));

            if (!$request->ajax() || empty($resource) || !is_int($id) || $id === 0) {
                return new JsonResponse(['message' => 'Error en los parámetros enviados'], Response::HTTP_BAD_REQUEST);
            }

            if (($model = $this->getModelFromName($resource)) === null) {
                return new JsonResponse(['message' => 'Error en los parámetros enviados'], Response::HTTP_BAD_REQUEST);
            }

            $record = $model->findOrFail($id)->toArray();
            $fieldDefinitions = $model->getFieldDefinition($resource);

            foreach ($model->getFillable() as $field) {
                 if (array_key_exists($field, $record)) {
                     $fieldDefinitions[$field]['value'] = $record[$field];
                     $fieldDefinitions[$field]['id'] = $record['id'];
                 }
            }

        } catch (ModelNotFoundException $mnf) {
            return new JsonResponse(['message' => $mnf->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse (['record' => $fieldDefinitions, 'message' => 'OK']);
    }

    public function getModelFromName(string $resource)
    {
        $model = null;
        switch ($resource) {
            case 'language':
                $model = new Language();
            break;
        }

        if (!is_subclass_of($model, 'Illuminate\Database\Eloquent\Model')) {
            $model = null;
        }

        return $model;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Language $language
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Language $language
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Language $language
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language $language
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        //
    }
}
