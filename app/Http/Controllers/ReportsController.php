<?php

namespace App\Http\Controllers;

use App\Models\Licence;
use App\Repositories\LicenceRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Datatables;

class ReportsController extends BaseController
{
    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Informes';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Emisoras';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = '';

    /**
     *
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return \Illuminate\View\View
     */
    public function radiostation(): \Illuminate\View\View
    {
        $licence = new Licence();
        $radioStations = $licence->distinct()->select('station_brand')->groupBy('station_brand')->get()->pluck('station_brand', 'station_brand')->toArray();
        $gpsModel = $licence->distinct()->select('gps_model')->groupBy('gps_model')->get()->pluck('gps_model', 'gps_model')->toArray();

        return $this->composeView('intranet.reports.forms.radiostation', [
            'title'   => 'Informe de Estaciones de Radio y Modelos de GPS',
            'path'    => ['Informes', 'Emisoreas'],
            'station' => $radioStations,
            'gps'     => $gpsModel,
        ]);
    }

    /**
     * Return the records that match with form data given.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Apr 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postRadiostation(Request $request): JsonResponse
    {
        $stations = $request->has('allStations') ? [] : $request->get('station');
        $gps = $request->has('allGps') ? [] : $request->get('gps');

        try {
            $lr = new LicenceRepository();
            $data = DataTables::of($lr->getStationsAndGps($stations, $gps))
                              ->make();

            return $data;

        } catch (\Exception $e) {
            return new JsonResponse(['data' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
