<?php

namespace App\Http\Controllers;

use App\Facades\RtlHelpers;
use App\Repositories\MemberRepository;
use App\Repositories\StaffRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DashboardController extends BaseController
{
    /**
     * Action Name
     *
     * @var string
     */
    protected $action = 'dashboard';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Repositories\RepositoryException
     * @throws \Exception
     */
    public function index()
    {
        $this->loggedUser = Auth::user();

        $m = new MemberRepository();
        $s = new StaffRepository();

        $membersExpirationsDates = $this->calculateMembersExpiration($m->getExpirations());
        $staffExpirationsDates = $this->calculateStaffExpiration($s->getExpirations());

        return $this->composeView('intranet', [
            'expirations'      => $membersExpirationsDates,
            'staffExpirations' => $staffExpirationsDates,
            'days'             => Config::get('radiotaxilaguna.expirations'),
        ]);
    }

    /**
     * Collect the data for all users that have any document close to expire
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @param array $expirations
     *
     * @return array
     */
    public function calculateMembersExpiration(array $expirations)
    {
        $data = [];
        $today = Carbon::now(new \DateTimeZone('Europe/London'));
        foreach ($expirations as $key => $expiration) {
            $data[$key . '-data'] = [];
            $element = [];
            foreach ($expiration as $record) {
                $message = 'Radio Taxi Laguna, le informa que ';
                $lm = '';
                $date = '';
                if ($record->licence) {
                    $element['telegram_id'] = $record->telegram_id;
                    $element['id'] = $record->licence->id;
                    $element['lm'] = 'LM-' . str_pad($record->licence->municipal_licence, 3, '0', STR_PAD_LEFT);
                    $element['name'] = $record->name . ' ' . $record->lastname;
                    $element['type'] = $key;
                    switch ($key) {
                        case 'pid':
                            $element['date'] = Carbon::parse($record->pid_expiration_date);
                            if ($element['date']->lt($today)) {
                                $message .= 'su carnet de identidad ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                            } else {
                                $message .= 'su carnet de identidad caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                            }
                        break;
                        case 'driverLicence':
                            $element['date'] = Carbon::parse($record->driver_licence_expiry_date);
                            if ($element['date']->lt($today)) {
                                $message .= 'su carnet de conducir ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                            } else {
                                $message .= 'su carnet de conducir caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                            }
                        break;
                        case 'municipalLicence':
                            $element['date'] = Carbon::parse($record->municipal_licence_expiry_date);
                            if ($element['date']->lt($today)) {
                                $message .= 'su licencia municipal ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                            } else {
                                $message .= 'su licencia municipal caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                            }
                        break;
                        case 'transportCard':
                            if ($record->member->licence) {
                                $element['lm'] = 'LM-' . str_pad($record->member->licence->municipal_licence, 3, '0', STR_PAD_LEFT);
                            }
                            $name = $record->member->name . ' ' . $record->member->lastname;
                            $element['date'] = Carbon::parse($record->tc_expiration_date);
                            if ($element['date']->lt($today)) {
                                $message .= 'su tarjeta de transporte ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                            } else {
                                $message .= 'su tarjeta de transporte caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                            }
                        break;
                    }

                    $element['date'] = RtlHelpers::getShortDate($element['date']);
                    $element['message'] = $message;

                    // Look for the last notification record...
                    $lastNotification = $record->licence->notifications->sortBy('date')->last();
                    if ($lastNotification) {
                        $element['last-date'] = $lastNotification->date ? $lastNotification->date : null;
                        $element['status'] = $lastNotification->status;
                    } else {
                        $element['last-date'] = null;
                        $element['status'] = null;
                    }
                }

                array_push($data[$key . '-data'], $element);
            }
        }

        return $data;
    }

    public function calculateStaffExpiration(array $expirations)
    {
        $data = [];
        $today = Carbon::now(new \DateTimeZone('Europe/London'));
        foreach ($expirations as $key => $expiration) {
            $data[$key . '-data'] = [];
            $element = [];
            foreach ($expiration as $record) {

                if ($record->members->count() > 0) {
                    $message = 'Radio Taxi Laguna, le informa que ';
                    $element['lm'] = '';
                    $date = '';
                    $element['id'] = $record->id;
                    $element['pid'] = $record->pid;
                    $element['name'] = sprintf('%s %s', $record->name, $record->lastname);
                    $element['email'] = empty($record->email) ? '' : $record->email;
                    $element['type'] = 'staff-' . $key;
                    $element['licence'] = null;
                    $element['members'] = $record->members->first();
                    $lic = $record->members->first() ? "000" . $element['members']['licence']['municipal_licence'] : '';
                    $element['members']['licence']['municipal_licence'] = "LM-" . substr($lic, strlen($lic) - 3, 3);

                    switch ($key) {
                        case 'pid':
                            if ($record->members->count() > 0) {
                                $element['licence'][$record->members->first()->licence->id] = [
                                    'telegram_id' => $record->members->first()->telegram_id,
                                    'telegram_alias' => $record->members->first()->telegram_alias,
                                    'lm' => 'LM-' . str_pad($record->members->first()->licence->municipal_licence, 3,
                                            '0', STR_PAD_LEFT)
                                ];

                                $element['date'] = Carbon::parse($record->pid_expiration_date);
                                if ($element['date']->lt($today)) {
                                    $message .= 'su carnet de identidad ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                                } else {
                                    $message .= 'su carnet de identidad caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                                }

                                $element['last-date'] = null;
                                $element['status'] = null;

                                // Look for the last notification record...

                                $members = $record->members->first();
                                if (!is_null($members)) {
                                    $notifications = $members->licence->notifications;
                                } else {
                                    $notifications = new Collection();
                                }


                                foreach ($notifications as $notification) {
                                    if (($notification->type == 'staff-pid') && ($notification->staff_id == $record->id)) {
                                        $element['last-date'] = $notification->date ? $notification->date : null;
                                        $element['status'] = $notification->status;
                                    }
                                }
                            }
                            break;
                        case 'driverLicence':
                            if ($record->members->count() > 0) {
                                $element['licence'][$record->members->first()->licence->id] = [
                                    'telegram_id' => $record->members->first()->telegram_id,
                                    'telegram_alias' => $record->members->first()->telegram_alias,
                                    'lm' => 'LM-' . str_pad($record->members->first()->licence->municipal_licence, 3,
                                            '0', STR_PAD_LEFT)
                                ];
                                $element['date'] = Carbon::parse($record->driver_licence_expiry_date);
                                if ($element['date']->lt($today)) {
                                    $message .= 'su carnet de conducir ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                                } else {
                                    $message .= 'su carnet de conducir caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                                }

                                $element['last-date'] = null;
                                $element['status'] = null;

                                // Look for the last notification record...
                                $notifications = $record->members->first()->licence->notifications;

                                foreach ($notifications as $notification) {
                                    if (($notification->type == 'staff-driverLicence') && ($notification->staff_id == $record->id)) {
                                        $element['last-date'] = $notification->date ? $notification->date : null;
                                        $element['status'] = $notification->status;
                                    }
                                }
                            }

                            break;
                        case 'municipalLicence':
                            if ($record->members->count() > 0) {
                                $element['licence'][$record->members->first()->licence->id] = [
                                    'telegram_id' => $record->members->first()->telegram_id,
                                    'telegram_alias' => $record->members->first()->telegram_alias,
                                    'lm' => 'LM-' . str_pad($record->members->first()->licence->municipal_licence, 3,
                                            '0', STR_PAD_LEFT)
                                ];
                                $element['date'] = Carbon::parse($record->municipal_licence_expiry_date);
                                if ($element['date']->lt($today)) {
                                    $message .= 'su licencia municipal ha caducado el día ' . RtlHelpers::getShortDate($element['date']);
                                } else {
                                    $message .= 'su licencia municipal caducará el día ' . RtlHelpers::getShortDate($element['date']) . '. Proceda a su renovación.';
                                }

                                $element['last-date'] = null;
                                $element['status'] = null;

                                // Look for the last notification record...
                                $members = $record->members->first();
                                if (!is_null($members)) {
                                    $notifications = $members->licence->notifications;
                                } else {
                                    $notifications = new Collection();
                                }

                                foreach ($notifications as $notification) {
                                    if (($notification->type == 'staff-municipalLicence') && ($notification->staff_id == $record->id)) {
                                        $element['last-date'] = $notification->date ? $notification->date : null;
                                        $element['status'] = $notification->status;
                                    }
                                }
                            }
                            break;
                    }
                    $element['date'] = RtlHelpers::getShortDate($element['date']);
                    $element['message'] = $message;

                    array_push($data[$key . '-data'], $element);
                }
            }
        }

        return $data;
    }

    public function fakeFunction($expirations)
    {

    }
    

}
