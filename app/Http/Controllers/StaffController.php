<?php

namespace App\Http\Controllers;

use App\Models\ContractType;
use App\Models\Language;
use App\Models\Member;
use App\Models\Preference;
use App\Models\Staff;
use App\Repositories\StaffRepository;
use Illuminate\View\View;

class StaffController extends BaseController
{
    /**
     * Licence repository
     *
     * @var StaffRepository
     */
    protected $staffRepository;

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Sociedad';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Asalariados';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = '';

    /**
     * MemberController constructor.
     *
     * @param StaffRepository $staffRepository
     */
    public function __construct(StaffRepository $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    /**
     * Show the table with all members.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function showAll(): View
    {
        return $this->composeView('intranet.staff.list', ['title' => 'Lista de asalariados', 'path' => ['Sociedad', 'Asalariados']]);
    }

    /**
     * Show the wizard to add new member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     */
    public function new()
    {
        return $this->composeView('intranet.staff.new',
            [
                'title'              => 'Nuevo asalariado',
                'path'               => ['Sociedad', 'Asalariados'],
                'members'            => Member::doesntHave('staff')->get(['id', 'name', 'lastname']),
                'contracts'          => ContractType::all()->pluck('name', 'id'),
                'preferences-driver' => Preference::Driver()->get(['id', 'name']),
                'languages'          => Language::get(['id', 'name']),
            ]);
    }

    /**
     * Show member data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id): View
    {
        $staff = $this->staffRepository->getData($id);
        if (is_null($staff)) {
            return $this->composeView('intranet.errors.404');
        }

        // Figure out how to get the Menu option from Config...
        return $this->composeView('intranet.staff.show',
            [
                'staff'       => $staff,
                'members'     => Member::get(['id', 'name', 'lastname']),
                'contracts'   => ContractType::get(['id', 'name']),
                'languages'   => [
                    'ids'    => implode(',', $staff['languages']->pluck('id')->toArray()),
                    'values' => Language::whereNotIn('id', $staff['languages']->pluck('id'))->get(),
                ],
                'preferences' => [
                    'driver' => [
                        'ids'    => empty($staff['preferences']) ? [] : implode(',', $staff['preferences']->pluck('id')->toArray()),
                        'values' => Preference::Driver()->whereNotIn('id', (empty($staff['preferences']) ? [] : $staff['preferences']->pluck('id')))->get(),
                    ],
                ],
                'path'        => ['Sociedad', 'Asalariados'],
                'title'       => $staff['name'] . ' ' . $staff['lastname'],
            ]
        );
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $staff = $this->staffRepository->find($id);
        if (!is_null($staff)) {
            $staff->delete();
        }

        return redirect()->route('admin.staff.list');
    }
}
