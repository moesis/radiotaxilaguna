<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Preference;
use App\Repositories\MemberRepository;
use Illuminate\View\View;

class MemberController extends BaseController
{
    /**
     * Licence repository
     *
     * @var LicenceRepository
     */
    protected $memberRepository;

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Sociedad';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Socios';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = '';

    /**
     * MemberController constructor.
     *
     * @param LicenceRepository $licenceRepository
     */
    public function __construct(MemberRepository $licenceRepository)
    {
        $this->memberRepository = $licenceRepository;
    }

    /**
     * Show the table with all members.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function showAll(): View
    {
        return $this->composeView('intranet.members.list', ['title' => 'Lista de socios', 'path' => ['Sociedad', 'Socio']]);
    }

    /**
     * Show the wizard to add new member.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     */
    public function new()
    {
        return $this->composeView('intranet.licence.new', ['title' => 'Nuevo socio', 'path' => ['Sociedad', 'Socio']]);
    }

    /**
     * Show member data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id): View
    {
        $licence = $this->memberRepository->getData($id);
        if (is_null($licence)) {
            return $this->composeView('intranet.errors.404');
        }

        // Figure out how to get the Menu option from Config...
        return $this->composeView('intranet.members.show',
                                  [
                                      'data'  => [
                                          'licence'   => $licence,
                                          'languages' => Language::whereNotIn('id', $licence['member']['languages']->pluck('id')->all())->get(['id', 'name']),
                                          'preferences' => [
                                              'vehicle' => Preference::Vehicle()->whereNotIn('id', $licence['member']['preferences']->pluck('id')->all())->get(),
                                              'driver' => Preference::Driver()->whereNotIn('id', $licence['member']['preferences']->pluck('id')->all())->get(),
                                          ],
                                      ],
                                      'path'  => ['Sociedad', 'Socio'],
                                      'title' => 'Listado de Socios',
                                  ]
        );
    }

    public function persist(Request $request)
    {
        // TODO implement persist the member to database
    }
}
