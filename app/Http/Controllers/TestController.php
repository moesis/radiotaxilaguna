<?php

namespace App\Http\Controllers;

use App\Http\Requests\LanguageFormRequest;
use App\Models\Association;
use App\Models\Member;
use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;

class TestController extends BaseController
{

    public function prueba()
    {
//        $a = Member::with('association')->get()->toArray();
        $a = Member::with('association:id,name')->get();
        $b = [];
        foreach ($a as $item) {
            $b = $item->toArray();
            $b['associationName'] = $item->association->association_acronym;
            unset($b['association']);
        }

        dd($b);

        $m = $a->members()->get();

        dd($m);


        return 'Olé';

    }

}
