<?php
/**
 * Base controller to be extended by all controllers.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Jul 2017
 * @catergory   Controllers
 * @package     RadioTaxi Backend
 *
 * @todo
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class BaseController extends Controller
{
    /**
     * @var /App\Models\User
     */
    protected $loggedUser;

    /**
     * Option base for all views.
     *
     * @var array
     */
    protected $options;

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = '';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = '';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = '';

    /**
     * Compose and return the view identified by $name and attach all options in
     * $options array.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jul 2017
     *
     * @param       $name
     * @param array $options
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function composeView($name, $options = [])
    {
        $this->loggedUser = Auth::user();

        $localOptions = $this->getDefaultOptions();
        if (!empty($options)) {
            $localOptions['data'] = array_merge($localOptions['data'], $options);
        }

        return view($name, $localOptions);
    }

    /**
     * Return the field names for the CREATED_AT, UPDATED_AT and Softdeletes operations.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov, 2017
     *
     * @return array
     */
    public static function getTimestampsFields() : array
    {
        return Config::get('radiotaxilaguna.timestampfields');
    }

    /**
     * Return the default options for default View
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jul, 2017
     *
     * @return array
     */
    private function getDefaultOptions() : array
    {
        return [
            'data' => [
                'login' => [
                    'user'       => $this->loggedUser,
                    'roles'      => $this->loggedUser->roles()->get(),
                    'user-photo' => sprintf('/assets/img/users/%s.jpg', str_slug($this->loggedUser->name)),
                    'user-thumb' => sprintf('/assets/img/users/%s-small.jpg', str_slug($this->loggedUser->name)),
                ],
                'menu'  => [
                    'activeGroupTitle' => $this->groupTitle,
                    'activeOption'     => $this->option,
                    'activeSubOption'  => $this->subOption,
                ],
            ],
        ];

    }




}