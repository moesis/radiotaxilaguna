<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Requests\PostalCodeFormRequest;
use App\Repositories\LicenceRepository;
use App\Repositories\PostalCodeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundation;

class PostalCodeController extends BaseController
{
    /**
     * Member repository
     *
     * @var LicenceRepository
     */
    protected $memberRepository;

    /**
     * MemberController constructor.
     *
     * @param PostalCodeRepository $mr
     */
    public function __construct(PostalCodeRepository $mr)
    {
        $this->memberRepository = $mr;
    }

    public function save(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $result = $this->ajaxSave($request);
        } else {
            $result = $this->formSave(new PostalCodeFormRequest($request->all()));
        }

        return $result;
    }

    /**
     * Persist data thought ajax call
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Spet 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    private function ajaxSave(Request $request) : JsonResponse
    {
        $validation = $this->memberRepository->validate($request->except(['_token']));
        if (!$validation->errors()->isempty()) {
            return new JsonResponse($validation->errors()->getMessages(), HttpFoundation::HTTP_BAD_REQUEST);
        }

        try{
            if ($this->memberRepository->create($request->except(['_token']))) {
                return new JsonResponse('El registro se ha grabado satisfactoriamente.', HttpFoundation::HTTP_CREATED);
            }

        }catch (\Exception $e) {
            return new JsonResponse('Se ha producido algún error en la grabación, Intentelo más tarde.', HttpFoundation::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse('Se ha producido algún error en la grabación, Intentelo más tarde.', HttpFoundation::HTTP_BAD_REQUEST);
    }

    /**
     * Persist data throught form call
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param PostalCodeFormRequest $request
     *
     * @return JsonResponse
     */
    private function formSave(PostalCodeFormRequest $request) : JsonResponse
    {
        if ($this->memberRepository->create($request->except(['_token']))) {
            return new JsonResponse('El registro se ha grabado satisfactoriamente.', HttpFoundation::HTTP_CREATED);
        }

        return new JsonResponse('Se ha producido algún error en la grabación, Intentelo más tarde.', HttpFoundation::HTTP_BAD_REQUEST);
    }

}