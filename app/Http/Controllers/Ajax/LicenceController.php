<?php

namespace App\Http\Controllers\Ajax;

use App\Facades\RtlHelpers;
use App\Repositories\LicenceRepository;
use App\Repositories\MemberRepository;
use App\Repositories\VehicleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response as Response;
use Datatables;


class LicenceController extends BaseController
{
    const PERSONAL_DATA = 'personal';
    const VEHICLE_DATA = 'vehicle';
    const TRANSPORT_CARD_DATA = 'transport_card';
    const DOCUMENT_DATA = 'document';
    const ECONOMIC_DATA = 'economic';
    const PREFERENCES_DATA = 'preferences';

    /**
     * Member repository
     *
     * @var LicenceRepository
     */
    protected $licenceRepository;

    /**
     * MemberController constructor.
     *
     * @param LicenceRepository $licenceRepo
     */
    public function __construct(LicenceRepository $licenceRepo)
    {
        $this->licenceRepository = $licenceRepo;
    }

    /**
     * Return the Datatable with member list.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getLicencesList(Request $request)
    {
        return Datatables::of($this->licenceRepository->getList())
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->get('search')['value'] != '') {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return
                            Str::contains(Str::lower($row['member']['name']), Str::lower($request->get('search')['value']))
                            || Str::contains(Str::lower($row['member']['lastname']), Str::lower($request->get('search')['value']))
                            || Str::contains(Str::lower($row['municipal_licence']), Str::lower($request->get('search')['value']))
                            || Str::contains(Str::lower($row['association']['name']), Str::lower($request->get('search')['value']))
                            || Str::contains(Str::lower($row['member']['email']), Str::lower($request->get('search')['value']))
                            || Str::contains(Str::lower($row['member']['mobile']), Str::lower($request->get('search')['value']))
                                ? true : false;
                    });
                }
            })
            ->addColumn('action', function ($record) {
                return $this->getActionColumnContent('licence', $record);
            })
            ->make(true);
    }

    /**
     * Return 'Found' if Municipal Licence exists in database, otherwise return 'Not Found'
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkLm(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]],
                Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $licence = $request->get('municipal_licence');
        if (!is_null($licence) && $this->licenceRepository->existsMunicipalLicence($licence)) {
            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]]);
    }

    /**
     * Return a JsonResponse with 302 if the GPS serial number is found, otherwise return a 404
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkGps(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]],
                Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $gpsSerial = $request->get('gps_serial');
        if (!is_null($gpsSerial) && $this->licenceRepository->existsGps($gpsSerial)) {
            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]]);
    }

    /**
     * Entrypoint to update the personal data of a licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updatePersonalData(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]],
                Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $licenceData = $request->only([
                'licence_id',
                'municipal_licence',
                'association_id',
            ]);

            $licenceUpdateResponse = $this->licenceRepository->update($licenceData, self::PERSONAL_DATA);
            if ($licenceUpdateResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['response' => $licenceUpdateResponse->content()],
                    Response::HTTP_BAD_REQUEST);
            }

            $memberData = $request->only([
                'pid',
                'pid_expiration_date',
                'dob',
                'name',
                'lastname',
                'telegram_alias',
                'address',
                'address_1',
                'postal_code_id',
                'phone',
                'mobile',
                'email',
                'driver_licence_expiry_date',
                'rest_day',
                'list',
            ]);

            $memberData['id'] = json_decode($licenceUpdateResponse->content())->record->member_id;
            $memberData['dob'] = RtlHelpers::dateOrNull($memberData['dob'], 'Y-m-d');
            $memberData['pid_expiration_date'] = RtlHelpers::dateOrNull($memberData['pid_expiration_date'], 'Y-m-d');
            $memberData['driver_licence_expiry_date'] = RtlHelpers::dateOrNull($memberData['driver_licence_expiry_date'], 'Y-m-d');
            $memberData['municipal_licence_expiry_date'] = RtlHelpers::dateOrNull($request->get('tc_expiration_date'), 'Y-m-d');


            $memberRepository = new MemberRepository();
            $memberUpdateResponse = $memberRepository->update($memberData, self::PERSONAL_DATA);
            if ($memberUpdateResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['HttpResponse' => $memberUpdateResponse->content()],
                    Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);

        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update the vehicle's data into database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateVehicleData(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]],
                Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $licenceData = $request->only([
                'licence_id',
                'station_brand',
                'station_model',
                'station_serial',
                'gps_brand',
                'gps_model',
                'gps_serial',
                'gps_mac',
                'gps_iccid',
                'auriga_id',
                'alternative_auriga_id',
            ]);

            $licenceUpdateResponse = $this->licenceRepository->update($licenceData, self::VEHICLE_DATA);
            if ($licenceUpdateResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['response' => $licenceUpdateResponse->content()], Response::HTTP_BAD_REQUEST);
            }

            $vehicleData = $request->only([
                'vehicle_id',
                'car_model_id',
                'plate',
                'places',
                'disabled_places',
                'enrollment_date',
            ]);
            if ($request->has('cancellation_date')) {
                $vehicleData['cancellation_date'] = RtlHelpers::dateOrNull($request->request['cancellation_date'],
                    'Y-m-d');
            }
            $vehicleData['enrollment_date'] = RtlHelpers::dateOrNull($vehicleData['enrollment_date'], 'Y-m-d');
            $vehicleData['member_id'] = json_decode($licenceUpdateResponse->content())->record->member_id;

            $memberRepository = new MemberRepository();
            $memberRepository->update($vehicleData, self::VEHICLE_DATA);

            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upodate the Document regarding information.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateDocumentData(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $licenceId = $request->only(['licence_id']);
            $memberData = $request->only([
                'transport_card',
                'tc_expiration_date',
                'application_date',
                'council_date',
                'communication_date',
            ]);

            $licenceRecord = $this->licenceRepository->find($licenceId['licence_id'])->member()->first();
            if (!$licenceRecord) {
                return new JsonResponse(['message' => 'Licencia no encontrada'], Response::HTTP_NOT_FOUND);
            }
            $memberData['member_id'] = $licenceRecord->id;
            $memberData['tc_expiration_date'] = RtlHelpers::dateOrNull($memberData['tc_expiration_date'], 'Y-m-d');
            $memberData['application_date'] = RtlHelpers::dateOrNull($memberData['application_date'], 'Y-m-d');
            $memberData['council_date'] = RtlHelpers::dateOrNull($memberData['council_date'], 'Y-m-d');
            $memberData['communication_date'] = RtlHelpers::dateOrNull($memberData['communication_date'], 'Y-m-d');

            $memberRepository = new MemberRepository();
            $memberUpdateResponse = $memberRepository->update($memberData, self::DOCUMENT_DATA);


            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update the specific economic data of current licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateEconomicData(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $licenceId = $request->only(['licence_id']);
            $memberData = $request->only([
                'iban',
            ]);
            $licenceRecord = $this->licenceRepository->find($licenceId['licence_id'])->member()->first();
            if (!$licenceRecord) {
                return new JsonResponse(['message' => 'Licencia no encontrada'], Response::HTTP_NOT_FOUND);
            }
            $memberData['member_id'] = $licenceRecord->id;

            $memberRepository = new MemberRepository();
            $memberUpdateResponse = $memberRepository->update($memberData, self::ECONOMIC_DATA);

            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update the member preferences and languages.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jan 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updatePreferences(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $licenceId = $request->only(['licence_id']);
            $memberData = $request->only([
                'driverPreferences',
                'spokenLanguagesPreferences',
            ]);

            $licenceRecord = $this->licenceRepository->find($licenceId['licence_id'])->member()->first();
            if (!$licenceRecord) {
                return new JsonResponse(['message' => 'Licencia no encontrada'], Response::HTTP_NOT_FOUND);
            }
            $memberData['member'] = $licenceRecord;

            $vehiclePreferences = $request->only([
                'vehiclePreferences',
            ]);

            $vehiclePreferences['vehicle'] = $licenceRecord->vehicles()->first();

            $memberRepository = new MemberRepository();
            $memberUpdateResponse = $memberRepository->update($memberData, self::PREFERENCES_DATA);
            if ($memberUpdateResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['HttpResponse' => $memberUpdateResponse->content()], Response::HTTP_BAD_REQUEST);
            }

            $vehicleRepository = new VehicleRepository();
            $vechicleUpdateResponse = $vehicleRepository->updatePreferences($vehiclePreferences);
            if ($vechicleUpdateResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['HttpResponse' => $memberUpdateResponse->content()], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);

        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

}