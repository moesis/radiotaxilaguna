<?php

namespace App\Http\Controllers\Ajax;

use App\Facades\RtlHelpers;
use App\Models\Language;
use App\Repositories\LicenceRepository;
use App\Repositories\MemberRepository;
use App\Repositories\VehicleRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;
use Yajra\Datatables\Facades\Datatables;
use DateTimeZone;

class MemberController extends BaseController
{
    /**
     * Member repository
     *
     * @var MemberRepository
     */
    protected $memberRepository;

    /**
     * MemberController constructor.
     *
     * @param MemberRepository $memberRepo
     */
    public function __construct(MemberRepository $memberRepo)
    {
        $this->memberRepository = $memberRepo;
    }

    /**
     * Check if personal Id exists in database and return the appropriate JsonResponse
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkPersonalId(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_BAD_REQUEST]], HttpFoundation::HTTP_BAD_REQUEST);
        }

        $licence = strtoupper($request->get('pid'));
        if (is_null($licence)) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_BAD_REQUEST]], HttpFoundation::HTTP_BAD_REQUEST);
        }

        // Remove all . and -
        $licence = str_replace(['.', '-'], '', $licence);

        // Check the letter if it is wrong, change to right letter and check
        $letterToResponse = '';
        $letter = strtoupper(substr($licence, -1, 1));
        if (!ctype_alpha($letter)){
            $letter = '';
        }
        $personalId = substr($licence, 0, 8);

        // if it's a NIE we must change the first letter by 0, 1 ó 2 depending on X, Y or Z.
        $personalId = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $personalId);

        $letterFromCalc = strtoupper(substr("TRWAGMYFPDXBNJZSQVHLCKEO", intval($personalId) % 23, 1));
        $newLicence = $licence;
        if ($letter !== $letterFromCalc) {
            $newLicence = $personalId . $letterFromCalc;
            $letterToResponse = $letterFromCalc;
        }

        $status = HttpFoundation::$statusTexts[HttpFoundation::HTTP_NOT_FOUND];
        if ($this->memberRepository->exists($newLicence)) {
            $status = HttpFoundation::$statusTexts[HttpFoundation::HTTP_FOUND];
        }

        return new JsonResponse([
            'status' => $status,
            'letter' => $letterToResponse
        ]);
    }

    /**
     * Check and verify an IBAN account number. Returns 400 if it is not valid, 302 if found or 404 otherwise.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2015
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkIban(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]],
                HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        $ibanAccount = str_replace(' ', '', $request->get('iban'));
        if (empty($ibanAccount) || !verify_iban($ibanAccount) || !iban_verify_checksum($ibanAccount)) {
            return new JsonResponse(['status' => 'La cuenta tiene errores, verifiquela antes de continuar'],
                HttpFoundation::HTTP_BAD_REQUEST);
        }

        if ($this->memberRepository->existsIban(iban_to_machine_format($ibanAccount))) {
            return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_NOT_FOUND]]);
    }


    /**
     * Check if email is unique in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkEmail(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_BAD_REQUEST]], HttpFoundation::HTTP_BAD_REQUEST);
        }

        $email = $request->get('email');
        if (is_null($email) || $this->memberRepository->emailExists($email)) {
            return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_NOT_FOUND]]);
    }

    /**
     * Return 'Found' if Transport Card exists in database, otherwise return 'Not Found'
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkTransportCard(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]], HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        $transportCard = $request->get('transport_card');
        if (!is_null($transportCard) && $this->memberRepository->existsTransportCard($transportCard)) {
            return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_NOT_FOUND]]);

    }

    /**
     * Add a new member to the database. Bear in mind that this procedure involves
     * three repositories (licence, members and vehicles).
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2015
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addMember(Request $request): JsonREsponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]], HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        $licenceData = $request->only([
            'association_id', 'municipal_licence',  'station_brand', 'station_model', 'station_serial',
            'gps_brand', 'gps_model', 'gps_serial', 'gps_mac', 'auriga_id', 'alternative_auriga_id'
        ]);

        $memberData = $request->only([
            'pid', 'pid_expiration_date', 'name', 'lastname', 'dob', 'address', 'address_1',
            'postal_code_id', 'phone', 'mobile', 'email', 'telegram_alias', 'iban', 'list', 'rest_day',
            'driver_licence_expiry_date', 'municipal_licence_expiry_date',
            'application_date', 'council_date', 'communication_date'
        ]);

        $transportCardData = $request->only(['transport_card', 'tc_expiration_date']);
        $vehicleData = $request->only(['car_model_id', 'plate', 'places', 'disabled_places']);

        // Reformat the dates
        $memberData['dob'] = RtlHelpers::dateOrNull($memberData['dob'], 'Y-m-d');
        $memberData['pid_expiration_date'] = RtlHelpers::dateOrNull($memberData['pid_expiration_date'], 'Y-m-d');
        $memberData['application_date'] = RtlHelpers::dateOrNull($memberData['application_date'], 'Y-m-d');
        $memberData['council_date'] = RtlHelpers::dateOrNull($memberData['council_date'], 'Y-m-d');
        $memberData['communication_date'] = RtlHelpers::dateOrNull($memberData['communication_date'], 'Y-m-d');
        $memberData['driver_licence_expiry_date'] = RtlHelpers::dateOrNull($memberData['driver_licence_expiry_date'], 'Y-m-d');
        $memberData['municipal_licence_expiry_date'] = RtlHelpers::dateOrNull($memberData['municipal_licence_expiry_date'], 'Y-m-d');
        $transportCardData['tc_expiration_date'] = RtlHelpers::dateOrNull($transportCardData['tc_expiration_date'], 'Y-m-d');

        $vehicleData['enrollment_date'] = Carbon::now(new DateTimeZone('Europe/London'))->toDateString();

        $response = null;
        $status = HttpFoundation::HTTP_OK;

        try {
            // Member
            $memberResponse = $this->memberRepository->addMember($memberData);
            if ($memberResponse->status() !== HttpFoundation::HTTP_OK) {
                return new JsonResponse(['code' => HttpFoundation::HTTP_BAD_REQUEST, 'message' => $memberResponse->content()], HttpFoundation::HTTP_BAD_REQUEST);
            }

            $memberId = json_decode($memberResponse->content())->id;
            $memberObject = $this->memberRepository->find($memberId);

            // Languages, by default always speak Spanish (id = 1)
            $memberObject->languages()->attach(Language::where('name', 'Español')->first());

            // Licence
            $licenceRepository = new LicenceRepository();
            $licenceResponse = $licenceRepository->addLicence($licenceData, $memberId);
            if ($licenceResponse->status() !== HttpFoundation::HTTP_OK) {
                return new JsonResponse(['code' => HttpFoundation::HTTP_BAD_REQUEST, 'message' => $licenceResponse->content()], HttpFoundation::HTTP_BAD_REQUEST);
            }

            // Transport Card
            $transportCardResponse = $this->memberRepository->addTransportCard($transportCardData, $memberId);
            if ($transportCardResponse->status() !== HttpFoundation::HTTP_OK) {
                return new JsonResponse(['code' => HttpFoundation::HTTP_BAD_REQUEST, 'message' => $transportCardResponse->content()], HttpFoundation::HTTP_BAD_REQUEST);
            }

            // Vehicle
            $vehicleRepository = new VehicleRepository();
            $vehicledResponse = $vehicleRepository->addVehicle($vehicleData, $memberId);
            if ($transportCardResponse->status() !== HttpFoundation::HTTP_OK) {
                return new JsonResponse(['code' => HttpFoundation::HTTP_BAD_REQUEST, 'message' => $vehicledResponse->content()], HttpFoundation::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(['id' => $memberId], $status);

        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], HttpFoundation::HTTP_BAD_REQUEST);
        }
    }
}