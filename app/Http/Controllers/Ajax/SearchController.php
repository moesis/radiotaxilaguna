<?php

namespace App\Http\Controllers\Ajax;

use App\Repositories\VehicleRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundation;

class SearchController extends BaseController
{
    const SELECTOR_LENGHT = 3;
    const SEARCH_BY_PLATE = 'MAT';
    const SEARCH_BY_LICENCE = 'LIC';
    const SEARCH_BY_NAME = 'NOM';

    /**
     * Member repository
     *
     * @var vehicleRepository
     */
    protected $vehicleRepository;

    /**
     * SearchController constructor.
     *
     * @param VehicleRepository $vr
     */
    public function __construct(VehicleRepository $vr)
    {
        $this->vehicleRepository = $vr;
    }

    /**
     * Do a search by vehicle plate.
     *
     * The plate format to look for could be the following:
     * XX-9999-XX, 9999-XX, 9999 XX, XX9999XX
     * So we remove spaces and dashes from string.
     *
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Mar 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function search(Request $request) : JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]], HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        $status = HttpFoundation::HTTP_OK;
        $data = [];

        if ($request->has('q')) {
            $query = $request->get('q');
            if (empty($query)) {
                return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_BAD_REQUEST]], HttpFoundation::HTTP_BAD_REQUEST);
            }
        }

        // Analyze the query in order to know what do you want to search...
        switch (substr($query, 0, self::SELECTOR_LENGHT)) {
            case self::SEARCH_BY_PLATE:
                $data = $this->searchByPlate(ltrim($query, self::SEARCH_BY_PLATE));
            break;
            case self::SEARCH_BY_LICENCE:
                $data = $this->searchByLicence(ltrim($query, self::SEARCH_BY_LICENCE));
            break;
            case self::SEARCH_BY_NAME:
                $data = $this->searchByName(ltrim($query, self::SEARCH_BY_NAME));
            break;
            default:
                // There is no selector specified, so look for all categories.
        }

        return $data;
    }

    /**
     * Do a search by plate.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version mar 2018
     *
     * @param string $plate
     *
     * @return JsonResponse
     */
    protected function searchByPlate(string $plate) : JsonResponse
    {
        $data = null;

        try {
            $vr = new VehicleRepository();

            // Clean the plate. Remove dashes and spaces.
            if (preg_match("/[-|\s]/", $plate)) {
                $plate = str_replace(['-', ' '], '', $plate);
            }

            $plateModel = $vr->getPlateWithLicence($plate);

            // Transform the data as we need and return it
            $member = $plateModel->members->first();

            $data['licence_id'] = $member->licence->id;
            $data['plate'] = $plateModel->plate;
            $data['lm'] = $member->licence->municipal_licence;
            $data['car'] = sprintf ('%s %s', $plateModel->carModel->brand->name , $plateModel->carModel->name);
            $data['member'] = sprintf ('%s %s', $member->name, $member->lastname);
            $data['phone'] = empty($member->phone) ? '' : $member->phone;
            $data['mobile'] = empty($member->mobile) ? '' : $member->mobile;
            $data['email'] = empty($member->email) ? '' : $member->email;
            $data['telegram_alias'] = empty($member->telegram_alias) ? '' : $member->telegram_alias;
            $data['telegram_id'] = empty($member->telegram_id) ? '' : $member->telegram_id;

            return new JsonResponse(['data' => $data], HttpFoundation::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_NOT_FOUND]], HttpFoundation::HTTP_NOT_FOUND);
        }catch (\Exception $e) {
            return new JsonResponse(['msg' => $e->getMessage()], HttpFoundation::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function searchByLicence(string $plate)
    {

    }

    protected function searchByName(string $plate)
    {

    }
}