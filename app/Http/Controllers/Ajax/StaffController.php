<?php

namespace App\Http\Controllers\Ajax;

use App\Facades\RtlHelpers;
use App\Repositories\MemberRepository;
use App\Repositories\StaffRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Ajax\BaseController;

class StaffController extends BaseController
{
    /**
     * Member repository
     *
     * @var MemberRepository
     */
    protected $staffRepository;

    /**
     * MemberController constructor.
     *
     * @param StaffRepository $staffRepository
     */
    public function __construct(StaffRepository $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    /**
     * Return the Datatable with member list.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        return DataTables::of($this->staffRepository->getList())
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->get('search')['value'] != '') {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return
                            Str::contains(Str::lower($row['name']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['lastname']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['pid']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['mobile']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['email']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['members']['name']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['members']['lastname']), Str::lower($request->get('search')['value'])) ||
                            Str::contains(Str::lower($row['members']['licence']['municipal_licence']), Str::lower($request->get('search')['value']))
                            ? true : false;
                    });
                }
            })
            ->addColumn('action', function ($record) {
            return $this->getActionColumnContent('staff', $record);
        })->make(true);
    }

    /**
     * Check if personal Id exists in database and return the appropriate JsonResponse
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkPersonalId(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]], Response::HTTP_BAD_REQUEST);
        }

        $personalId = strtoupper($request->get('pid'));
        if (is_null($personalId)) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]], Response::HTTP_BAD_REQUEST);
        }

        // Check the personal licence.
        $pid = rtlHelpers::checkPersonalId($personalId);

        $status = Response::$statusTexts[Response::HTTP_NOT_FOUND];
        if ($this->staffRepository->exists($pid['personal-licence'] . $pid['letter'])) {
            $status = Response::$statusTexts[Response::HTTP_FOUND];
        }

        return new JsonResponse([
                                    'status' => $status,
                                    'letter' => $pid['letter'],
                                ]);
    }

    /**
     * Check if email is unique in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkEmail(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]], Response::HTTP_BAD_REQUEST);
        }

        $email = $request->get('email');
        if (is_null($email) || $this->staffRepository->emailExists($email)) {
            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]]);
    }

    /**
     * Add a new staff to the database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addStaff(Request $request): JsonREsponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $response = null;

        try {
            $staffData = $request->only(['pid', 'pid_expiration_date', 'dob', 'name', 'lastname',
                                         'address', 'address_1', 'postal_code_id', 'phone', 'mobile',
                                         'email', 'driver_licence_expiry_date', 'municipal_licence_expiry_date']);
            $contractData = $request->only(['boss-name', 'contract-type', 'start_date', 'end_date']);

            $preferences = explode(',', rtrim($request->get('preferences', ''), ','));
            $languages = explode(',', rtrim($request->get('languages', ''), ','));

            $staffData['dob'] = RtlHelpers::dateOrNull($staffData['dob'], 'Y-m-d');
            $staffData['pid_expiration_date'] = RtlHelpers::dateOrNull($staffData['pid_expiration_date'], 'Y-m-d');
            $staffData['driver_licence_expiry_date'] = RtlHelpers::dateOrNull($staffData['driver_licence_expiry_date'], 'Y-m-d');
            $staffData['municipal_licence_expiry_date'] = RtlHelpers::dateOrNull($staffData['municipal_licence_expiry_date'], 'Y-m-d');
            $contractData['start_date'] = RtlHelpers::dateOrNull($contractData['start_date'], 'Y-m-d');
            $contractData['end_date'] = RtlHelpers::dateOrNull($contractData['end_date'], 'Y-m-d');
            if (is_null($contractData['end_date'])) {
                $contractData['end_date'] = null;
            }

            return $this->staffRepository->add($staffData, $contractData, $languages, $preferences);

        }
        catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update data for selected Staff
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateStaff(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $staffData = $request->only(['pid', 'pid_expiration_date', 'dob', 'name', 'lastname',
                                         'address', 'address_1', 'postal_code_id', 'phone', 'mobile',
                                         'email', 'driver_licence_expiry_date', 'municipal_licence_expiry_date']);
            $staffData['id'] = $request->get('staff-pk');
            $contractData = $request->only(['boss-name', 'contract-type', 'start_date', 'end_date']);

            $preferencesData = explode(',', rtrim($request->get('driverPreferences', ''), ','));
            $languagesData = explode(',', rtrim($request->get('spokenLanguagesPreferences', ''), ','));

            $staffData['dob'] = RtlHelpers::dateOrNull($staffData['dob']);
            $staffData['pid_expiration_date'] = RtlHelpers::dateOrNull($staffData['pid_expiration_date']);
            $staffData['driver_licence_expiry_date'] = RtlHelpers::dateOrNull($staffData['driver_licence_expiry_date']);
            $staffData['municipal_licence_expiry_date'] = RtlHelpers::dateOrNull($staffData['municipal_licence_expiry_date']);
            $contractData['start_date'] = RtlHelpers::dateOrNull($contractData['start_date']);
            $contractData['end_date'] = RtlHelpers::dateOrNull($contractData['end_date']);

            $result = $this->staffRepository->update($staffData, $contractData, $languagesData, $preferencesData);

            $end_date = Carbon::createFromFormat('Y-m-d', $contractData['end_date'], 'Europe/London');
            $today = Carbon::now(new \DateTimeZone('Europe/London'));
            if ($end_date->lt($today) ) {
                $staff = $this->staffRepository->find($staffData['id']);
                if (!is_null($staff)) {
                    $staff->delete();
                }
            }

            return $result;
        }
        catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteStaff(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]], Response::HTTP_BAD_REQUEST);
        }

        $id = $request->get('id');
        try {
            $this->staffRepository->find($id)->delete();
            $status = Response::$statusTexts[Response::HTTP_OK];
            return new JsonResponse([
               'status' => $status
            ]);
        }
        catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}