<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class BaseController extends Controller
{

    /**
     * Return the string to include as action cell for each row in datatable.
     * Actions icons for a $modelname given and using the $record id
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @date    June 2016
     * @version 1.0
     *
     * @param string $modelname Model name to build the route names
     * @param Model $record     Record with data to process.
     *
     * @return string
     *
     * @throws \Exception
     *
     * @todo
     */
    protected function getActionColumnContent($modelname, $record)
    {
        $primaryKey = null;
        $primaryKeyCandidates = ['id', 'key_id'];

        if (($record instanceof Model) && !empty($record->id)) {
            $primaryKey = $record->id;
        } else {
            if (is_array($record)) {
                foreach ($primaryKeyCandidates as $candidateKey) {
                    if (array_has($record, $candidateKey)) {
                        $primaryKey = $record[$candidateKey];
                        break;
                    }
                }
            }
        }

        $routes = [
            'show'   => sprintf('admin.%s.show', $modelname),
            'edit'   => sprintf('admin.%s.edit', $modelname),
            'delete' => sprintf('admin.%s.delete', $modelname),
            'recover' => sprintf('admin.%s.recover', $modelname)
        ];


        $response = sprintf('%s %s',
                            sprintf('<a id="show-row-%d" href="%s" data-toggle="tooltip" data-placement="bottom" class="rtl-show-icon" title="%s"><i class="fa fa-eye"></i></a>',
                                    $primaryKey,
                                    Route::has($routes['show']) ? route(sprintf('admin.%s.show', $modelname), ['record_id' => $primaryKey]) : '#',
                                    'Mostrar'
                            ),
                            sprintf('<a id="edit-row-%d" href="%s" data-toggle="tooltip" data-placement="bottom" class="rtl-edit-icon" title="%s"><i class="fa fa-pencil"></i></a>',
                                    $primaryKey,
                                    Route::has($routes['edit']) ? route(sprintf('admin.%s.show', $modelname), ['record_id' => $primaryKey]) : '#',
                                    'Editar'
                            )
        );

        // Check privileges
        if (true) {
            $response = sprintf('%s %s',
                                $response,
                                sprintf('<form id="form-id-%s" class="rtl-delete-grid-form" method="POST" action="%s"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="%s"><a href="#" data-toggle="tooltip" data-placement="bottom" title="%s" onclick="document.getElementById(\'form-id-%s\').submit();"><i class="fa fa-close alert-danger"></i></a></form>',
                                        $primaryKey,
                                        Route::has($routes['delete']) ? route(sprintf('admin.%s.delete', $modelname), ['record_id' => $primaryKey]) : '#',
                                        csrf_token(),
                                        'Eliminar',
                                        $primaryKey
                                )
            );
        }

        if ($record['deleted_at']) {
            $response = sprintf('%s',
                                sprintf('<a href="%s" data-toggle="tooltip" data-placement="bottom" title="%s"><i class="fa fa-recycle"></i></a>',
                                        Route::has($routes['edit']) ? route(sprintf('admin.%s.show', $modelname), ['record_id' => $primaryKey]) : '#',
                                        'Recuperar'
                                )
            );
        }

        return $response;
    }
}
