<?php
/**
 * Controller to manage the image uploads.
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Feb 2018
 * @category    Controllers
 * @package     Radio Taxi Laguna Application
 * @subpackage  Controllers
 */

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\DocumentsRepository;
use App\Repositories\LicenceRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Repository for Documents
     *
     * @var DocumentsRepository
     */
    protected $document;

    /**
     * DocumentController constructor.
     *
     * @param DocumentsRepository $imageRepository
     */
    public function __construct(DocumentsRepository $imageRepository)
    {
        $this->document = $imageRepository;
    }

    /**
     * Upload the files.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return JsonResponse
     */
    public function upload()
    {
        try {
            $request = Input::except(['_token', 'ajax']);
            if (!array_key_exists('files', $request)) {
                return new JsonResponse([
                    'code'    => HttpFoundation::HTTP_BAD_REQUEST,
                    'message' => 'No se han seleccionado ficheros para subir.',
                ], HttpFoundation::HTTP_BAD_REQUEST);
            }
            $this->document->uploadDocuments($request);

            return new JsonResponse(['response' => HttpFoundation::HTTP_OK], HttpFoundation::HTTP_OK);

        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], HttpFoundation::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get all licence's documents
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version feb 2018
     *
     * @param Request $request
     *
     * @return bool|JsonResponse
     * @throws \Exception
     */
    public function getDocuments(Request $request)
    {
        if (!$request->has('licenceId')) {
            return new JsonResponse([
                'code'    => HttpFoundation::HTTP_BAD_REQUEST,
                'message' => 'No se ha encontrado licencia.',
            ], HttpFoundation::HTTP_BAD_REQUEST);
        }

        try {
            $licenceId = $request->get('licenceId');
            $documents = $this->document->getDocuments($licenceId);

            return new JsonResponse(['response' => $documents], HttpFoundation::HTTP_OK);

        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], HttpFoundation::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove a file associated to a licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @return \Illuminate\Http\JsonResponse|int
     *
     */
    public function delete()
    {
        $filename = Input::get('id');
        if (!$filename) {
            return 0;
        }

        $response = $this->document->delete($filename);

        return $response;
    }

}