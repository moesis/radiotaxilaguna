<?php

namespace App\Http\Controllers\Ajax;

use App\Models\CarBrand;
use App\Models\CarModel;
use App\Repositories\CarBrandRepository;
use App\Repositories\VehicleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Datatables;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundation;

class CarController extends BaseController
{
    /**
     * Member repository
     *
     * @var CarBrandRepository
     */
    protected $carBrandRepository;

    /**
     * MemberController constructor.
     *
     * @param CarBrandRepository $mr
     */
    public function __construct(CarBrandRepository $mr)
    {
        $this->carBrandRepository = $mr;
    }

    /**
     * Return the Datatable with member list.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList() : JsonResponse
    {
        return Datatables::of($this->memberRepository->getList())
            ->addColumn('action', function ($record) {
                return $this->getActionColumnContent('carbrand', $record);
            })
            ->make(true);
    }

    /**
     * Return all car brands from database
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getBrands(Request $request) : JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]], HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        $carBrands = CarBrand::select('name', 'id')->get()->pluck('name', 'id');
        if ($carBrands) {
            return new JsonResponse($carBrands, HttpFoundation::HTTP_OK);
        }

        return new JsonResponse(['name' => 'No encontrado'], HttpFoundation::HTTP_NOT_FOUND);
    }

    /**
     * Return all model for the brand given on request
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getModels(Request $request) : JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => HttpFoundation::$statusTexts[HttpFoundation::HTTP_METHOD_NOT_ALLOWED]], HttpFoundation::HTTP_METHOD_NOT_ALLOWED);
        }

        if ($request->has('brand_id')) {
            $carModels = CarModel::select('name', 'id')->where('brand_id', $request->get('brand_id'))->get()->pluck('name', 'id');
            if ($carModels) {
                return new JsonResponse($carModels, HttpFoundation::HTTP_OK);
            }
        }

        return new JsonResponse(['name' => 'No encontrado'], HttpFoundation::HTTP_NOT_FOUND);
    }

}