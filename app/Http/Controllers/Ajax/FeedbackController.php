<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackFormRequest;
use App\Models\Association;
use App\Models\CarBrand;
use App\Models\Locality;
use App\Models\PostalCode;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundation;

class FeedbackController extends Controller
{
    const SUGGESTION = 'SUGGESTION';
    const TICKET = 'TICKET';

    /**
     * Suggestion action to save the suggestion.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Jul 2017
     *
     * @param FeedbackFormRequest $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function newFeedback(FeedbackFormRequest $request)
    {
        if ($request->isXmlHttpRequest()) {

            try {
                // Look for user by email
                $userId = User::where('email', $request->get('email'))->first();
                $record = new Ticket();
                $record->user()->associate($userId);
                $record->type = $request->get('type');
                $record->text = trim($request->get('suggestion'));
                $record->saveOrFail();
            }
            catch (\Exception $e) {
                return new JsonResponse('La grabación de su sugerencia no ha podido ser realizada. Intetelo más tarde.', HttpFoundation::HTTP_INTERNAL_SERVER_ERROR);
            }

            return new JsonResponse('Su sugerencia ha sido registrada con éxito.', HttpFoundation::HTTP_OK);
        }

        return new JsonResponse('Access Denied', HttpFoundation::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Return the location name and id for the postal code given
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postalCode(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has('postalCode')) {
                $postalCodeValue = $request->get('postalCode');

                if (is_numeric($postalCodeValue)) {
                    $postalCode = PostalCode::with('locality')->where('postal_code', $postalCodeValue)->first();

                    if (!is_null($postalCode)) {
                        return new JsonResponse(
                            [
                                'id'   => $postalCode->id,
                                'name' => $postalCode->locality->name,
                            ], HttpFoundation::HTTP_OK
                        );
                    }

                    return new JsonResponse(['name' => 'No encontrado'], HttpFoundation::HTTP_NOT_FOUND);
                }
            }

            return new JsonResponse('Access Denied', HttpFoundation::HTTP_BAD_REQUEST);

        }

        return new JsonResponse('Access Denied', HttpFoundation::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Return all associations from database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function associations(Request $request)
    {
        if ($request->ajax()) {
            $assopciations = Association::all();
            if ($assopciations) {
                return new JsonResponse($assopciations->sortBy('id')->pluck('name', 'id'), HttpFoundation::HTTP_OK);
            }

            return new JsonResponse(['name' => 'No encontrado'], HttpFoundation::HTTP_NOT_FOUND);
        }

        return new JsonResponse('Access Denied', HttpFoundation::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Return all localities
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function localities(Request $request) : JsonResponse
    {
        if ($request->ajax()) {
            $localities = Locality::select('name', 'id')->get()->pluck('name', 'id');
            if ($localities) {
                return new JsonResponse($localities, HttpFoundation::HTTP_OK);
            }

            return new JsonResponse(['name' => 'No encontrado'], HttpFoundation::HTTP_NOT_FOUND);
        }

        return new JsonResponse('Access Denied', HttpFoundation::HTTP_NOT_ACCEPTABLE);
    }

}
