<?php

namespace App\Http\Controllers\Ajax;

use App\Repositories\LicenceRepository;
use App\Repositories\VehicleRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Datatables;
use Symfony\Component\HttpFoundation\Response as Response;


class VehicleController extends BaseController
{
    /**
     * Member repository
     *
     * @var LicenceRepository
     */
    protected $vehicleRepository;

    /**
     * MemberController constructor.
     *
     * @param VehicleRepository $repo
     */
    public function __construct(VehicleRepository $repo)
    {
        $this->vehicleRepository = $repo;
    }

    /**
     * Return the Datatable with member list.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(): JsonResponse
    {
        return Datatables::of($this->vehicleRepository->getList())
                         ->addColumn('action', function ($record) {
                             return $this->getActionColumnContent('vehicle', $record);
                         })
                         ->make(true);
    }

//    /**
//     * Return a 302 if given plate exists on database, otherwise return a 404
//     *
//     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
//     * @version Sept 2017
//     *
//     * @param Request $request
//     *
//     * @return JsonResponse
//     */
//    public function checkPlate(Request $request) : JsonResponse
//    {
//        if ($request->isXmlHttpRequest()) {
//
//            if ($request->has('carPlate')) {
//                $exists = $this->vehicleRepository->exists($request->get('carPlate'));
//
//                return new JsonResponse(['status' => $exists ? Response::HTTP_FOUND : Response::HTTP_NOT_FOUND], Response::HTTP_OK);
//            }
//
//            return new JsonResponse('Invalid parameter', Response::HTTP_BAD_REQUEST);
//        }
//
//        return new JsonResponse('Access Denied', Response::HTTP_NOT_ACCEPTABLE);
//    }

    /**
     * Return a JsonResponse with 302 if the car plate is found, otherwise return a 404
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Sept 2017
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkPlate(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }
        $carPlate = $request->get('plate');

        // Clean the plate. Remove dashes and spaces.
        if (preg_match("/[-|\s]/", $carPlate)) {
            $carPlate = str_replace(['-', ' '], '', $carPlate);
        }

        if (!is_null($carPlate) && $this->vehicleRepository->existsPlate($carPlate)) {
            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_FOUND]]);
        }

        return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]]);
    }

    /**
     * Save the notes for the current vehicle.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addNotes(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $vehicleResponse = $this->vehicleRepository->updateNotes($request->get('vehicle_id'), $request->only(['notes']));
            if ($vehicleResponse->status() !== Response::HTTP_OK) {
                return new JsonResponse(['HttpResponse' => $vehicleResponse->content()], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_OK]]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Recover the notes for the specified vehicle.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Feb 2018
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getNotes(Request $request) : JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['msg' => Response::$statusTexts[Response::HTTP_METHOD_NOT_ALLOWED]], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        try {
            $vehicle = $this->vehicleRepository->find($request->get('vehicle_id'));
            if (!$vehicle) {
                return new JsonResponse(['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]], Response::HTTP_NOT_FOUND);
            }

            return new JsonResponse([
                'status' => Response::$statusTexts[Response::HTTP_OK],
                'notes' => json_decode($vehicle->notes, true)
            ], Response::HTTP_OK);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

    }

}