<?php

namespace App\Http\Controllers;

use App\Repositories\CarBrandRepository;
use App\Repositories\LicenceRepository;
use Illuminate\View\View;

class CarController extends BaseController
{
    /**
     * Member repository
     *
     * @var LicenceRepository
     */
    protected $memberRepository;

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Sociedad';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Vehículos';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = 'Marcas';

    /**
     * MemberController constructor.
     *
     * @param CarBrandRepository $mr
     */
    public function __construct(CarBrandRepository $mr)
    {
        $this->memberRepository = $mr;
    }

    /**
     * Show the table with all members.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function list() : View
    {
        return $this->composeView('intranet.cruds.vehicles.brands.list', ['title' => 'Marcas de vehículos', 'path' => ['Sociedad', 'Vehiculos', 'Marcas']]);
    }

    /**
     * Show member data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMember(int $id) : View
    {
        $member = $this->memberRepository->getAllMemberData($id);
        if (is_null($member)) {
            return $this->composeView('intranet.errors.404');
        }

        // Figure out how to get the Menu option from Config...
        return $this->composeView('intranet.members.show', ['member' => $member, 'path' => ['Sociedad', 'Socio']]);
    }

}
