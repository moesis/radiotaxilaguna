<?php

namespace App\Http\Controllers;

use App\Models\CarBrand;
use App\Models\CarModel;
use App\Models\Language;
use App\Models\Preference;
use App\Repositories\LicenceRepository;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;

class LicenceController extends BaseController
{
    /**
     * Licence repository
     *
     * @var LicenceRepository
     */
    protected $licenceRepository;

    /**
     * String to match group in menu
     *
     * @var string
     */
    protected $groupTitle = 'Sociedad';

    /**
     * String to match option in menu.
     *
     * @var string
     */
    protected $option = 'Licencias';

    /**
     * Strint to match with suboption in menu.
     *
     * @var string
     */
    protected $subOption = '';

    /**
     * Controller constructor.
     *
     * @param LicenceRepository $licenceRepository
     */
    public function __construct(LicenceRepository $licenceRepository)
    {
        $this->licenceRepository = $licenceRepository;
    }

    /**
     * Show the table with all licences.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Dec 2017
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function showAll(): View
    {
        return $this->composeView('intranet.licences.list', ['title' => 'Lista de licencias', 'path' => ['Sociedad', 'Licencias']]);
    }

    /**
     * Show the wizard to add new licence.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     */
    public function new()
    {
        return $this->composeView('intranet.licences.new', ['title' => 'Nueva Licencia', 'path' => ['Sociedad', 'Licencias']]);
    }

    /**
     * Show licence data
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Aug 2017
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id): View
    {
        $licence = $this->licenceRepository->getData($id);
        if (is_null($licence)) {
            return $this->composeView('intranet.errors.404');
        }

        // Figure out how to get the Menu option from Config...
        return $this->composeView('intranet.licences.show', [
            'data'  => [
                'licence'     => $licence,
                'vehicles'    => [
                    'brands' => CarBrand::get(['id', 'name'])->pluck('name', 'id')->toArray(),
                    'models' => CarModel::get(['id', 'name'])->pluck('name', 'id')->toArray(),
                ],
                'languages'   => [
                    'ids'    => implode(',', $licence['member']['languages']->pluck('id')->toArray()),
                    'values' => Language::whereNotIn('id', $licence['member']['languages']->pluck('id')->all())->get(['id', 'name']),
                ],
                'preferences' => [
                    'vehicle' => [
                        'ids'    => implode(',', empty($licence['member']['vehicle']['preferences']) ? [] : $licence['member']['vehicle']['preferences']->pluck('id')->toArray()),
                        'values' => Preference::Vehicle()->whereNotIn('id', empty($licence['member']['vehicle']['preferences']) ? [] : $licence['member']['vehicle']['preferences']->pluck('id')->all())->get(),
                    ],
                    'driver'  => [
                        'ids'    => implode(',', $licence['member']['preferences']->pluck('id')->toArray()),
                        'values' => Preference::Driver()->whereNotIn('id', $licence['member']['preferences']->pluck('id')->all())->get(),
                    ],
                ],
            ],
            'path'  => ['Sociedad', 'Socio'],
            'title' => 'Listado de Socios',
        ]);
    }

    public function persist(Request $request)
    {
        // TODO implement persist the member to database
    }
}
