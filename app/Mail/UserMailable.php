<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class UserMailable extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = Config::get('radiotaxilaguna.company.email');
        if (array_key_exists('from-email', $this->data)) {
            $from = Config::get('radiotaxilaguna.company.app-email');
        }

        return $this->from($from, 'Administración ' . Config::get('radiotaxilaguna.company.name¡') . ' ' . Config::get('radiotaxilaguna.company.surname') . ' S. Coop.')
            ->subject($this->data['title'])
            ->view($this->data['view'])
            ->with(['data' => $this->data]);
    }
}
