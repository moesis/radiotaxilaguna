<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/send/{id}/{message}', function ($id, $message) {

    $l = \App\Models\Licence::where('id', $id)->with(['member'])->first();
    \App\Facades\Telegram::sendMessage($l->member->telegram_id, $message);
});


Route::group(['prefix' => 'admin'], function () {

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('post-login');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', 'DashboardController@index')->name('admin.dashboard');

        require_once('routes/ajax.routes.php');
        require_once('routes/registration.routes.php');
        require_once('routes/licences.routes.php');
        require_once('routes/staff.routes.php');
        require_once('routes/vehicles.routes.php');
        require_once('routes/reports.routes.php');
        require_once('routes/crud.routes.php');

        Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    });
});

Route::get('/', 'HomeController@index')->name('public.home');
