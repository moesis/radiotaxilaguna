<?php
/**
 * Ajax Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Jul 2017
 * @catergory   Routes
 * @package     Radio Taxi Laguna
 */
// Licences
Route::post('/ajax/update/personalData', 'Ajax\LicenceController@updatePersonalData')->name('ajax.licences.update.personaldata');
Route::post('/ajax/update/vehicleData', 'Ajax\LicenceController@updateVehicleData')->name('ajax.licences.update.vehicledata');
Route::post('/ajax/update/documentData', 'Ajax\LicenceController@updateDocumentData')->name('ajax.licences.update.documentdata');
Route::post('/ajax/update/economicData', 'Ajax\LicenceController@updateEconomicData')->name('ajax.licences.update.economicdata');
Route::post('/ajax/update/preferences', 'Ajax\LicenceController@updatePreferences')->name('ajax.licences.update.preferences');

// Member
Route::get('/ajax/members/check/lm', 'Ajax\LicenceController@checkLm')->name('ajax.check.lm');
Route::get('/ajax/members/check/gps', 'Ajax\LicenceController@checkGps')->name('ajax.check.gps.serial');
Route::get('/ajax/members', 'Ajax\LicenceController@getLicencesList')->name('ajax.licences.list');
Route::get('/ajax/members/check/iban', 'Ajax\MemberController@checkIban')->name('ajax.check.iban');
Route::get('/ajax/members/check/transportCard', 'Ajax\MemberController@checkTransportCard')->name('ajax.check.transport.card');
Route::get('/ajax/members/check/personalId', 'Ajax\MemberController@checkPersonalId')->name('ajax.check.personalId');
Route::get('/ajax/members/check/email', 'Ajax\MemberController@checkEmail')->name('ajax.check.email');
Route::post('/ajax/members/add', 'Ajax\MemberController@addMember')->name('ajax.add.member');

// Staff 
Route::get('/ajax/staff', 'Ajax\StaffController@getList')->name('ajax.staff');
Route::get('/ajax/staff/check/personalId', 'Ajax\StaffController@checkPersonalId')->name('ajax.check.staff.personalId');
Route::get('/ajax/staff/check/email', 'Ajax\StaffController@checkEmail')->name('ajax.check.staff.email');
Route::post('/ajax/staff/add', 'Ajax\StaffController@addStaff')->name('ajax.add.staff');
Route::post('/ajax/staff/update', 'Ajax\StaffController@updateStaff')->name('ajax.update.staff');
Route::post('/ajax/staff/delete', 'Ajax\StaffController@deleteStaff')->name('ajax.delete.staff');

// Vehicles
Route::get('/ajax/carbrands', 'Ajax\CarController@getBrands')->name('ajax.carbrands');
Route::get('/ajax/carmodels', 'Ajax\CarController@getModels')->name('ajax.carmodels');
Route::get('/ajax/vehicle/check/plate', 'Ajax\VehicleController@checkPlate')->name('ajax.check.vehicle.plate');
Route::post('/ajax/vehicle/notes', 'Ajax\VehicleController@addNotes')->name('ajax.add.notes');
Route::get('/ajax/vehicle/notes', 'Ajax\VehicleController@getNotes')->name('ajax.get.notes');

// Reports
Route::get('/ajax/report/stationandgps', 'ReportsController@postRadiostation')->name('ajax.reports.stationandgps');


// Other
Route::post('/ajax/suggestions', 'Ajax\FeedbackController@newFeedback')->name('ajax.feedback');
Route::get('/ajax/postalcode', 'Ajax\FeedbackController@postalcode')->name('ajax.postalcode');
Route::get('/ajax/associations', 'Ajax\FeedbackController@associations')->name('ajax.associations');
Route::get('/ajax/localities', 'Ajax\FeedbackController@localities')->name('ajax.localities');
Route::post('/ajax/postalcode/save', 'Ajax\PostalCodeController@save')->name('ajax.postalcode.save');
Route::post('/ajax/doc/upload', 'Ajax\DocumentController@upload')->name('ajax-upload-post');
Route::get('/ajax/doc/documents', 'Ajax\DocumentController@getDocuments')->name('ajax-get-documents');
Route::post('/ajax/doc/delete', 'Ajax\DocumentController@delete')->name('ajax-upload-delete');
Route::get('/ajax/search', 'Ajax\SearchController@search')->name('ajax.search');


