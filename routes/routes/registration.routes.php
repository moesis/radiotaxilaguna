<?php
/**
 * Registration Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Jul 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 *
 * @todo
 */

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
