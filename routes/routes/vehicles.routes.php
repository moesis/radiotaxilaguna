<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Member Routes...
use Illuminate\Support\Facades\Route;

Route::get('/carbrand', 'CarController@list')->name('admin.carbrand.list');
//Route::get('/carbrand/show/{id}', 'CarBrandController@show')->name('admin.carbrand.show');
//Route::get('/carbrand/edit/{id}', 'CarBrandController@edit')->name('admin.carbrand.edit');
//Route::post('/carbrand/save', 'CarBrandController@save')->name('admin.carbrand.save');
//Route::post('/carbrand/update/{id}', 'CarBrandController@update')->name('admin.carbrand.update');
//Route::delete('/carbrand/delete/{id}', 'CarBrandController@delete')->name('admin.carbrand.delete');

Route::get('/carmodel', 'CarController@list')->name('admin.carmodel.list');
//Route::get('/carmodel/show/{id}', 'carmodelController@show')->name('admin.carmodel.show');
//Route::get('/carmodel/edit/{id}', 'carmodelController@edit')->name('admin.carmodel.edit');
//Route::post('/carmodel/save', 'carmodelController@save')->name('admin.carmodel.save');
//Route::post('/carmodel/update/{id}', 'carmodelController@update')->name('admin.carmodel.update');
//Route::delete('/carmodel/delete/{id}', 'carmodelController@delete')->name('admin.carmodel.delete');
