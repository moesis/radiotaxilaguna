<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Licence Routes...
use Illuminate\Support\Facades\Route;

Route::get('/report/radiostation', 'ReportsController@radiostation')->name('admin.reports.stationandgps');
