<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Language Routes...
use Illuminate\Support\Facades\Route;

Route::get('/crud/languages', 'LanguageController@index')->name('admin.crud.language.list');
Route::get('/crud/get', 'LanguageController@getRecord')->name('admin.crud.get.record');