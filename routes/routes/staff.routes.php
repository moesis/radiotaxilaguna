<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Member Routes...
use Illuminate\Support\Facades\Route;


Route::get('/staff/list', 'StaffController@showAll')->name('admin.staff.list');
Route::get('/staff/show/{id}', 'StaffController@show')->name('admin.staff.show');
Route::get('/staff/new', 'StaffController@new')->name('admin.staff.new');
//Route::get('/staff/delete', 'StaffController@delete')->name('admin.staff.delete');
// POST -> Ajax Call

// Route::get('edit\{id}', 'MemberController@edit')->name('member.edit');
// Route::post('/members/put', 'MemberController@save')->name('member.update');
Route::delete('/staff/delete/{id}', 'StaffController@delete')->name('admin.staff.delete');