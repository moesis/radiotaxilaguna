<?php
/**
 * Password Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Jul 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 *
 * @todo
 */

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');