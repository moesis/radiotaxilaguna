<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Member Routes...
use Illuminate\Support\Facades\Route;


Route::get('/members', 'MemberController@showAll')->name('admin.member.list');
Route::get('/members/show/{id}', 'MemberController@show')->name('admin.member.show');
Route::get('/members/new', 'MemberController@new')->name('admin.member.new');
// POST -> Ajax Call

// Route::get('edit\{id}', 'MemberController@edit')->name('member.edit');
// Route::post('/members/put', 'MemberController@save')->name('member.update');
// Route::delete('delete\{id}', 'MemberController@delete')->name('member.delete');