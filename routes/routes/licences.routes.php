<?php
/**
 * Members Routes
 *
 * @author      Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version     Aug 2017
 * @catergory   Routes
 * @package     Backend Radio Taxi Laguna.
 */

// Licence Routes...
use Illuminate\Support\Facades\Route;

Route::get('/licences/list', 'LicenceController@showAll')->name('admin.licences.list');
Route::get('/licence/show/{id}', 'LicenceController@show')->name('admin.licence.show');
Route::get('/licence/new', 'LicenceController@new')->name('admin.licence.new');